

  function deleteForm(id){
    $("#item"+id).remove();
  }

  function editForm(id){

    var desc          = $('#desc'+id).val();
    var category      = $('#cat'+id).val();
    var subCategory   = $('#subCat'+id).val();
    var price         = numeral($("#price"+id).text()).value();
    var discount      = numeral($("#disc"+id).text()).value();
    var count         = numeral($("#count"+id).text()).value();
    var amount        = numeral($("#amount"+id).text()).value();

    var form =
        "<tr id='editForm"+id+"'>"+
        "<td><select id='categoryForm'  style='width:100px' class='form-control select2' data-placeholder='Choose Optic' tabindex='1'>"+
        "</select></td>"+
        "<td><select id='subCategoryForm'  style='width:100px' class='form-control select2' data-placeholder='Choose Optic' tabindex='1'>"+
        "</select></td>"+
        "<td><select id='descriptionForm'  style='width:100px' class='form-control select2' data-placeholder='Choose Optic' tabindex='1'>"+

        "</select></td>"+
        "<td><input id='countForm' type='text' style='width:50px' class='form-control' onkeyup='calculateAmount()' value='"+count+"'></td>"+
        "<td><input id='priceForm' readonly type='text' style='width:100px' class='form-control' onkeyup='calculateAmount()' value='"+price+"'></td>"+
        "<td><input id='discountForm' readonly type='text' style='width:50px' class='form-control' onkeyup='calculateAmount()' value='"+discount+"'></td>"+
        "<td><input id='amountForm' readonly type='text' style='width:100px' class='form-control' value='"+amount+"' readonly></td>"+
        "<td><input type='button' class='btn blue btn-outline' onclick='checkItem("+id+");' value='Update'></td>"+
        "<td><input type='button' class='btn red btn-outline' onclick='cancelUpdate("+id+")' value='Cancel'>"+
        "</td>"+
    "</tr>";
    $("#item"+id).after(form);
    $("#item"+id).hide();
    $(".editButton").hide();
    $(".deleteButton").hide();
    $("#addForm").remove();



        $("#categoryForm").select2({
          ajax: {
            url: '/Category/jsonSelect',
            dataType: 'json',
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
              };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }


        });
        $('#categoryForm').on('select2:select', function (e) {
            var data = e.params.data;
            $("#subCategoryForm").attr("disabled", false);
            $("#subCategoryForm").select2({
              ajax: {
                url: '/SubCategory/jsonSelect',
                dataType: 'json',
                data: function (params) {
                  return {
                    q: params.term, // search term
                    idCat : data.id,
                    page: params.page
                  };
                },
                processResults: function (data) {
                  return {
                    results: $.map(data, function (item) {
                            return {
                                text: item.nama,
                                id: item.id
                            }
                        })
                  };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              }


            });
        });

        $('#subCategoryForm').on('select2:select', function (e) {
            var data = e.params.data;
            $("#descriptionForm").attr("disabled", false);
            $("#descriptionForm").select2({
              ajax: {
                url: '/Item/jsonSelect',
                dataType: 'json',
                data: function (params) {
                  return {
                    q: params.term, // search term
                    idCat : data.id,
                    page: params.page
                  };
                },
                processResults: function (data) {
                  return {
                    results: $.map(data, function (item) {
                            return {
                                text: item.nama,
                                id: item.id
                            }
                        })
                  };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              }


            });
        });

    $('#descriptionForm').on('select2:select', function (e) {
        var data = e.params.data;
        $.ajax({
            url: '/Item/json',
            type: 'PATCH',
            data: { id : data.id},
            success: function(result) {
              //$('#priceForm').val(result.harga_jual_default);
              //$("#alamatOptic").val(result.alamat);
            }
        });
    });


    $("#descriptionForm").select2({
      ajax: {
        url: '/Item/jsonSelect',
        dataType: 'json',
        processResults: function (data) {

          return {
            results: $.map(data, function (item) {
                    var category    = item.subcategory.category;
                    var subCategory = item.subcategory;

                    return {
                        text: item.nama + " - " + category.nama + "( "+subCategory.nama+" )" ,
                        id: item.id
                    }
                })
          };
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }


    });
  }

  function updateForm(id){
    var valueCat      = $("#categoryForm :selected").val();
    var category      = $("#categoryForm :selected").text();
    var valueSubCat   = $("#subCategoryForm :selected").val();
    var subCategory   = $("#subCategoryForm :selected").text();
    var value         = $("#descriptionForm :selected").val();
    var description   = $("#descriptionForm :selected").text();
    var price         = numeral($("#priceForm").val()).format();
    var discount      = numeral($("#discountForm").val()).format();
    var count         = numeral($("#countForm").val()).format();
    var amount        = numeral($("#amountForm").val()).format();

    $("#item"+id).remove();

    var item =
    "<tr id='item"+id+"'>"+
        "<td id='cat"+idCount+"' name='cat' style='display:none;'>"+valueCat+"</td>"+
        "<td >"+category+"</td>"+
        "<td id='subCat"+idCount+"' name='subCat' style='display:none;'>"+valueSubCat+"</td>"+
        "<td >"+subCategory+"</td>"+
        "<td id='desc"+id+"' name='desc' style='display:none;'>"+value+"</td>"+
        "<td >"+description+"</td>"+
        "<td id='count"+id+"' name='count'>"+count+"</td>"+
        "<td id='price"+id+"' readonly name='price'>"+price+"</td>"+
        "<td id='disc"+id+"' readonly name='disc'>"+discount+"</td>"+
        "<td id='amount"+id+"' readonly name='amount'>"+amount+"</td>"+
        "<td class='editButton' style='float:right'><input type='button' class='btn green btn-outline' onclick='editForm("+id+")' value='Edit'></td>"+
        "<td class='deleteButton'><input type='button' class='btn red btn-outline' onclick='deleteForm("+id+")' value='Delete'>"+
        "</td>"+
    "</tr>";
    $("#editForm"+id).before(item);
    $("#editForm"+id).remove();
    calculateSummary();
    cancelUpdate(id);

    checkOpticPlafon();
  }

  function cancelUpdate(id){
    $("#item"+id).show();
    $(".editButton").show();
    $(".deleteButton").show();
    $("#editForm"+id).remove();
    generateAddItem();
  }

  function cancel(){
    $("#formItem").remove();
    generateAddItem();
  }

  function saveForm(){
    var valueCat          = $("#categoryForm :selected").val();
    var valueSubCat       = $("#subCategoryForm :selected").val();
    var valueItem         = $("#descriptionForm :selected").val();
    var category          = $("#categoryForm :selected").text();
    var subCategory       = $("#subCategoryForm :selected").text();
    var description       = $("#descriptionForm :selected").text();
    var price             = numeral($("#priceForm").val()).format();
    var discount          = numeral($("#discountForm").val()).format();
    var count             = numeral($("#countForm").val()).format();
    var amount            = numeral($("#amountForm").val()).format();

    $("#formItem").remove();
    var item =
        "<tr id='item"+idCount+"'>"+
        "<td id='cat"+idCount+"' name='cat' style='display:none;'>"+valueCat+"</td>"+
        "<td >"+category+"</td>"+
        "<td id='subCat"+idCount+"' name='subCat' style='display:none;'>"+valueSubCat+"</td>"+
        "<td >"+subCategory+"</td>"+
        "<td id='desc"+idCount+"' name='desc' style='display:none;'>"+valueItem+"</td>"+
        "<td >"+description+"</td>"+
        "<td id='count"+idCount+"' name='count'>"+count+"</td>"+
        "<td id='price"+idCount+"' readonly name='price'>"+price+"</td>"+
        "<td id='disc"+idCount+"' readonly name='disc'>"+discount+"</td>"+
        "<td id='amount"+idCount+"' readonly name='amount'>"+amount+"</td>"+
        "<td class='editButton' style='float:right'><input type='button' class='btn green btn-outline' onclick='editForm("+idCount+")' value='Edit'></td>"+
        "<td class='deleteButton'><input type='button' class='btn red btn-outline' onclick='deleteForm("+idCount+")' value='Delete'>"+
        "</td>"+
    "</tr>";
    $("#itemSales tbody").append(item);
    calculateSummary();
    generateAddItem();
    idCount++;

    checkOpticPlafon();
  }

  function calculateSummary(){
    var total = 0;
    for(var i=0;i<=idCount;i++){
      total += numeral($("#amount"+i).html()).value();
    }

    $("#totalAmount").html("Rp "+numeral(total).format());
  }

  function calculateAmount(){
    var count     = $("#countForm").val();
    var price     = $("#priceForm").val();
    var discount  = $("#discountForm").val();
    var disCalc   = (count * price) * (discount / 100);
    var amount    = (count * price) - disCalc;

    $("#amountForm").val(amount);
  }

  function checkItem(state){
      var item      = $("#descriptionForm :selected").val();
      var buyedItem = $("#countForm").val();
      var count     = 0;
      $.ajax({
          url: '/Item/jsonitemWithStock',
          type: 'PATCH',
          data: {id : item},
          success: function(result) {
            count = result.items_stock.length == 0 ? 0 : result.items_stock[result.items_stock.length-1].jumlah;
            if(buyedItem > count){
              alert("Item tidak mencukupi, jumlah item tinggal "+count);
            }
            else{
              if(state == -999)
                saveForm();
              else {
                updateForm(state);
              }
            }
          },
          error: function (xhr, status, errorThrown) {
            $("#alamatOptic").val("");
            $("#plafonOptic").val("");
            $("#lastDateOptic").val("");
            $("#submitButton").prop('disabled', true);
          }
      });


  }

  function generateAddItem(){
    var generateForm = "<tr id='addForm'><td colspan='7'><a href='#' onclick='insertForm();return false;'>Add Item</a></td></tr>";
    $("#itemSales tbody").append(generateForm);
  }

  function insertForm(){
    $("#addForm").remove();
    var form =
        "<tr id='formItem'>"+
        "<td ><select id='categoryForm' style='width:100%' class='form-control select2' data-placeholder='Choose Category' tabindex='1'>"+
        "</select></td>"+
        "<td ><select id='subCategoryForm' disabled style='width:100%'  class='form-control select2' data-placeholder='Choose SubCategory' tabindex='1'>"+
        "</select></td>"+
        "<td ><select id='descriptionForm' disabled style='width:100%' class='form-control select2' data-placeholder='Choose Item' tabindex='1'>"+
        "</select></td>"+
        "<td><input id='countForm' type='text' style='width:100%' class='form-control' onkeyup='calculateAmount()' value='0'></td>"+
        "<td><input id='priceForm' type='text' style='width:100%' class='form-control' onkeyup='calculateAmount()' readonly value='0'></td>"+
        "<td><input id='discountForm' type='text' style='width:100%' class='form-control' onkeyup='calculateAmount()' readonly  value='0'></td>"+
        "<td><input id='amountForm' type='text' style='width:100%' class='form-control' value='0' readonly></td>"+
        "<td><input type='button' class='btn blue btn-outline' onclick='checkItem(-999);' value='Submit'></td>"+
        "<td><input type='button' class='btn red btn-outline' onclick='cancel()' value='Cancel'>"+
        "</td>"+
    "</tr>";
    $("#itemSales tbody").append(form);


    $('#categoryForm').on('select2:select', function (e) {
        var data = e.params.data;
        $("#subCategoryForm").attr("disabled", false);
        $("#subCategoryForm").select2({
          ajax: {
            url: '/SubCategory/jsonSelect',
            dataType: 'json',
            data: function (params) {
              return {
                q: params.term, // search term
                idCat : data.id,
                page: params.page
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
              };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }


        });
    });

    $('#subCategoryForm').on('select2:select', function (e) {
        var data = e.params.data;
        $("#descriptionForm").attr("disabled", false);
        $("#descriptionForm").select2({
          ajax: {
            url: '/Item/jsonSelect',
            dataType: 'json',
            data: function (params) {
              return {
                q: params.term, // search term
                idCat : data.id,
                page: params.page
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
              };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }


        });
    });

    $('#descriptionForm').on('select2:select', function (e) {
        var data = e.params.data;
        $.ajax({
            url: '/Item/json',
            type: 'PATCH',
            data: { id : data.id},
            success: function(result) {
              //$('#priceForm').val(result.harga_jual_default);
              //$("#alamatOptic").val(result.alamat);

            }
        });
    });


    $("#categoryForm").select2({
      ajax: {
        url: '/Category/jsonSelect',
        dataType: 'json',
        processResults: function (data) {
          return {
            results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                })
          };
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }


    });


  }

  $( document ).ready(function() {

    $(".select2").select2();
    generateAddItem();
  });
