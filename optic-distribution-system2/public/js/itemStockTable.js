  var idCount = 0;

  function deleteForm(id){
    $("#item"+id).remove();
  }

  function editForm(id){

    var desc          = $('#desc'+id).val();
    var category      = $('#cat'+id).val();
    var subCategory   = $('#subCat'+id).val();
    var stock         = numeral($("#stock"+id).text()).value();
    var price         = numeral($("#price"+id).text()).value();
    var discount      = numeral($("#disc"+id).text()).value();
    var count         = numeral($("#count"+id).text()).value();
    var amount        = numeral($("#amount"+id).text()).value();
    var expiry        = $('#expiry'+id).text();
    
    var form =
        "<tr id='editForm"+id+"'>"+
        "<td><select id='categoryForm' class='form-control select2' data-placeholder='Choose Optic' tabindex='1'>"+

        "</select></td>"+
        "<td><select id='subCategoryForm' class='form-control select2' data-placeholder='Choose Optic' tabindex='1'>"+

        "</select></td>"+
        "<td><select id='descriptionForm' class='form-control select2' data-placeholder='Choose Optic' tabindex='1'>"+

        "</select></td>"+
        "<td><input id='stockForm' type='text' class='form-control' value='"+stock+"' readonly></td>"+
        "<td><input id='countForm' type='text' style='width:80px' class='form-control' onkeyup='calculateAmount()' value='"+count+"'></td>"+
        "<td><input id='priceForm' type='text' class='form-control' onkeyup='calculateAmount()' value='"+price+"'></td>"+
        "<td hidden><input id='discountForm' type='text' class='form-control' onkeyup='calculateAmount()' value='"+discount+"'></td>"+
        "<td><input class='form-control form-control-inline date-picker' style='width:100px;' data-provide='datepicker' id='expiry' name='expiry' type='text' value='"+expiry+"' /></td>"+
        "<td><input id='amountForm' type='text' class='form-control' value='"+amount+"' readonly></td>"+
        "<td><input type='button' class='btn blue btn-outline' onclick='updateForm("+id+")' value='Update'></td>"+
        "<td><input type='button' class='btn red btn-outline' onclick='cancelUpdate("+id+")' value='Cancel'>"+
        "</td>"+
    "</tr>";
    $("#item"+id).after(form);
    $("#item"+id).hide();
    $(".editButton").hide();
    $(".deleteButton").hide();
    $("#addForm").remove();

    $("#categoryForm").select2({
          ajax: {
            url: '/Category/jsonSelect',
            dataType: 'json',
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
              };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }


        });
        $('#categoryForm').on('select2:select', function (e) {
            var data = e.params.data;
            $("#subCategoryForm").attr("disabled", false);
            $("#subCategoryForm").select2({
              ajax: {
                url: '/SubCategory/jsonSelect',
                dataType: 'json',
                data: function (params) {
                  return {
                    q: params.term, // search term
                    idCat : data.id,
                    page: params.page
                  };
                },
                processResults: function (data) {
                  return {
                    results: $.map(data, function (item) {
                            return {
                                text: item.nama,
                                id: item.id
                            }
                        })
                  };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              }


            });
        });

        $('#subCategoryForm').on('select2:select', function (e) {
            var data = e.params.data;
            $("#descriptionForm").attr("disabled", false);
            $("#descriptionForm").select2({
              ajax: {
                url: '/Item/jsonSelect',
                dataType: 'json',
                data: function (params) {
                  return {
                    q: params.term, // search term
                    idCat : data.id,
                    page: params.page
                  };
                },
                processResults: function (data) {
                  return {
                    results: $.map(data, function (item) {
                            return {
                                text: item.nama,
                                id: item.id
                            }
                        })
                  };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              }


            });
        });

    $('#descriptionForm').on('select2:select', function (e) {
        var data = e.params.data;
        $.ajax({
            url: '/Item/json',
            type: 'PATCH',
            data: { id : data.id},
            success: function(result) {
              $('#priceForm').val(result.harga_jual_default);
              //$("#alamatOptic").val(result.alamat);
            }
        });
    });

    $('#descriptionForm').on('select2:select', function (e) {
        var data = e.params.data;
        $.ajax({
            url: '/Item/json',
            type: 'PATCH',
            data: { id : data.id},
            success: function(result) {
              $('#priceForm').val(result.harga_jual_default);
              //$("#alamatOptic").val(result.alamat);
            }
        });
    });

    $("#descriptionForm").select2({
      ajax: {
        url: '/Item/jsonSelect',
        dataType: 'json',
        processResults: function (data) {

          return {
            results: $.map(data, function (item) {
                    var category    = item.subcategory.category;
                    var subCategory = item.subcategory;

                    return {
                        text: item.nama + " - " + category.nama + "( "+subCategory.nama+" )" ,
                        id: item.id
                    }
                })
          };
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }


    });
  }

  function updateForm(id){
    var valueCat      = $("#categoryForm :selected").val();
    var category      = $("#categoryForm :selected").text();
    var valueSubCat   = $("#subCategoryForm :selected").val();
    var subCategory   = $("#subCategoryForm :selected").text();
    var description   = $("#descriptionForm :selected").text();
    var price         = numeral($("#priceForm").val()).format();
    var discount      = numeral($("#discountForm").val()).format();
    var stock         = numeral($("#stockForm").val()).format();
    var count         = numeral($("#countForm").val()).format();
    var amount        = numeral($("#amountForm").val()).format();
    var expiry        = $("#expiry").val();

    $("#item"+id).remove();

    var item =
    "<tr id='item"+id+"'>"+
        "<td id='cat"+idCount+"' name='cat' style='display:none;'>"+valueCat+"</td>"+
        "<td >"+category+"</td>"+
        "<td id='subCat"+idCount+"' name='subCat' style='display:none;'>"+valueSubCat+"</td>"+
        "<td >"+subCategory+"</td>"+
        "<td >"+description+"</td>"+
        "<td id='stock"+id+"'>"+stock+"</td>"+
        "<td id='count"+id+"'>"+count+"</td>"+
        "<td id='price"+id+"'>"+price+"</td>"+
        "<td id='expiry"+id+"'>"+expiry+"</td>"+
        "<td hidden id='disc"+id+"'>"+discount+"</td>"+
        "<td id='amount"+id+"'>"+amount+"</td>"+
        "<td class='editButton' style='float:right'><input type='button' class='btn green btn-outline' onclick='editForm("+id+")' value='Edit'></td>"+
        "<td class='deleteButton'><input type='button' class='btn red btn-outline' onclick='deleteForm("+id+")' value='Delete'>"+
        "</td>"+
    "</tr>";
    $("#editForm"+id).before(item);
    $("#editForm"+id).remove();
    cancelUpdate(id);
  }

  function cancel(){
    $("#formItem").remove();
    generateAddItem();
  }

  function cancelUpdate(id){
    $("#item"+id).show();
    $(".editButton").show();
    $(".deleteButton").show();
    $("#editForm"+id).remove();
    generateAddItem();
  }

  function saveForm(){
    var valueCat          = $("#categoryForm :selected").val();
    var valueSubCat       = $("#subCategoryForm :selected").val();
    var valueItem         = $("#descriptionForm :selected").val();
    var category          = $("#categoryForm :selected").text();
    var subCategory       = $("#subCategoryForm :selected").text();
    var description       = $("#descriptionForm :selected").text();
    var inStock           = numeral($("#stockForm").val()).format();
    var price             = numeral($("#priceForm").val()).format();
    var discount          = numeral($("#discountForm").val()).format();
    var count             = numeral($("#countForm").val()).format();
    var amount            = numeral($("#amountForm").val()).format();
    var expiry            = $("#expiry").val();

    $("#formItem").remove();
    var item =
        "<tr id='item"+idCount+"'>"+
        "<td id='cat"+idCount+"' name='cat' style='display:none;'>"+valueCat+"</td>"+
        "<td >"+category+"</td>"+
        "<td id='subCat"+idCount+"' name='subCat' style='display:none;'>"+valueSubCat+"</td>"+
        "<td >"+subCategory+"</td>"+
        "<td id='desc"+idCount+"' name='desc' style='display:none;'>"+valueItem+"</td>"+
        "<td >"+description+"</td>"+
        "<td id='stock"+idCount+"' name='count'>"+inStock+"</td>"+
        "<td id='count"+idCount+"' name='count'>"+count+"</td>"+
        "<td id='price"+idCount+"' name='price'>"+price+"</td>"+
        "<td id='expiry"+idCount+"' name='expiry'>"+expiry+"</td>"+
        "<td hidden id='disc"+idCount+"' name='disc'>"+discount+"</td>"+
        "<td id='amount"+idCount+"' name='amount'>"+amount+"</td>"+
        "<td class='editButton' style='float:right'><input type='button' class='btn green btn-outline' onclick='editForm("+idCount+")' value='Edit'></td>"+
        "<td class='deleteButton'><input type='button' class='btn red btn-outline' onclick='deleteForm("+idCount+")' value='Delete'>"+
        "</td>"+
    "</tr>";
    $("#itemSales tbody").append(item);
    calculateSummary();
    generateAddItem();
    idCount++;
  }

  function calculateSummary(){
    var total = 0;
    for(var i=0;i<=idCount;i++){
      total += numeral($("#amount"+i).html()).value();
    }

    $("#totalAmount").html("Rp "+numeral(total).format());
  }

  function calculateAmount(){
    var count     = $("#countForm").val();
    var price     = $("#priceForm").val();
    var discount  = $("#discountForm").val();
    var disCalc   = (count * price) * (discount / 100);
    var amount    = (Math.floor(count) * price) - disCalc;

    $("#amountForm").val(amount);
  }

  function generateAddItem(){
    var generateForm = "<tr id='addForm'><td colspan='9'><a href='#' onclick='insertForm();return false;'>Add Item</a></td></tr>";
    $("#itemSales tbody").append(generateForm);
  }

  function insertForm(){
    $("#addForm").remove();
    var form =
        "<tr id='formItem'>"+
        "<td ><select id='categoryForm' style='width: 100%' style='width:150px' class='form-control select2' data-placeholder='Choose Category' tabindex='1'>"+
        "</select></td>"+
        "<td ><select id='subCategoryForm' disabled style='width: 100%' style='width:150px' class='form-control select2' data-placeholder='Choose SubCategory' tabindex='1'>"+
        "</select></td>"+
        "<td ><select id='descriptionForm' disabled style='width: 100%' style='width:150px' class='form-control select2' data-placeholder='Choose Item' tabindex='1'>"+
        "</select></td>"+
        "<td><input id='stockForm' type='text' class='form-control' value='0' readonly></td>"+
        "<td><input id='countForm' type='number' style='width:80px' class='form-control' onChange='calculateAmount()' onkeyup='calculateAmount()' value='0'></td>"+
        "<td><input id='priceForm' type='text' class='form-control' onkeyup='calculateAmount()' value='0'></td>"+
        "<td hidden><input id='discountForm' type='text' class='form-control' onkeyup='calculateAmount()' value='0'></td>"+
        "<td><input class='form-control form-control-inline date-picker' style='width:100px;' data-provide='datepicker' id='expiry' name='expiry' type='text' value='' disabled /></td>"+
        "<td><input id='amountForm' type='text' style='width:100px' class='form-control' value='0' readonly></td>"+
        "<td><input type='button' class='btn blue btn-outline' onclick='saveForm()' value='Submit'></td>"+
        "<td><input type='button' class='btn red btn-outline' onclick='cancel()' value='Cancel'>"+
        "</td>"+
    "</tr>";
    $("#itemSales tbody").append(form);


    $('#categoryForm').on('select2:select', function (e) {
        var data = e.params.data;
        $("#subCategoryForm").attr("disabled", false);
        $("#subCategoryForm").select2({
          ajax: {
            url: '/SubCategory/jsonSelect',
            dataType: 'json',
            data: function (params) {
              return {
                q: params.term, // search term
                idCat : data.id,
                page: params.page
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
              };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }


        });
    });

    $('#subCategoryForm').on('select2:select', function (e) {
        var data = e.params.data;
        $("#descriptionForm").attr("disabled", false);
        $("#descriptionForm").select2({
          ajax: {
            url: '/Item/jsonSelect',
            dataType: 'json',
            data: function (params) {
              return {
                q: params.term, // search term
                idCat : data.id,
                page: params.page
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
              };
            }
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }


        });
    });

    $('#descriptionForm').on('select2:select', function (e) {
        var data = e.params.data;
        $.ajax({
            url: '/Item/jsonitemWithStock',
            type: 'PATCH',
            data: { id : data.id},
            success: function(result) {
              var currentStock=0;
              var stock = result.items_stock;
              if (stock.length>0) {
                currentStock = result.items_stock[0].jumlah;
              }

              $('#priceForm').val(result.harga_jual_default);
              $('#stockForm').val(currentStock);

              if(result.sub_category.category.expiryStat == 0){
                $('#expiry').val('');
                $('#expiry').prop( "disabled", true );
              }
              if(result.sub_category.category.expiryStat == 1){
                $('#expiry').val('');
                $('#expiry').prop( "disabled", false );
              }



            }
        });
    });


    $("#categoryForm").select2({
      ajax: {
        url: '/Category/jsonSelect',
        dataType: 'json',
        processResults: function (data) {
          return {
            results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                })
          };
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }


    });


  }

  $( document ).ready(function() {

    $(".select2").select2();
    generateAddItem();
  });
