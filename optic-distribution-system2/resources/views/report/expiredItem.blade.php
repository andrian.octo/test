
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> Laporan Produk
            <small>Expired Item</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


              <div class="portlet light ">

                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        item
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Kategori
                        </th>
                        <th>
                          Sub Kategori
                        </th>
                        <th>
                          Nama
                        </th>
                        <th>
                          Expiry Date
                        </th>
                        <th>
                          Jumlah Item
                        </th>

                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    var table = $('#item').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("Report") }}/expiryDatatable',
        columns: [
            { data: 'category', name: 'category' },
            { data: 'subcategory', name: 'subcategory' },
            { data: 'nama', name: 'nama' },
            { data: 'expiry', name: 'expiry' },
            { data: 'sisa', name: 'jumlah' },
        ],
        columnDefs: [

        ],
        fnDrawCallback: function (oSettings) {

              $("[data-toggle='confirmation']").confirmation({
                singleton: true,
                onConfirm:function(){
                  var myElement = $(this).closest('.popover').prev();
                  var id        = myElement.context.getAttribute('data-id');

                  $.ajax({
                      url: '{{url("Item")}}/'+id,
                      type: 'DELETE',
                      data: {_token: '{{csrf_token()}}'},
                      success: function(result) {
                        table.ajax.reload();

                        toastr[success]("Product Berhasil dihapus", "Success")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                      }
                  });

                }
              });
            }
    });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
