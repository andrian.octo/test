
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@component('components.echart')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> Laporan Produk
            <small></small>
        </h2>
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Bar Chart</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" onclick="reset()">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body" >
                    <div id="echarts_bar" style="height:500px;"></div>
                </div>
            </div>
        </div>
        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


              <div class="portlet light ">

                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        item
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Kategori
                        </th>
                        <th>
                          Sub Kategori
                        </th>
                        <th>
                          Nama
                        </th>
                        <th>
                          Harga
                        </th>
                        <th>
                          Jumlah
                        </th>

                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
                <div id="main" style="width: 600px;height:400px;"></div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    var table = $('#item').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("Report") }}/inventoryDatatable',
        columns: [
            { data: 'category', name: 'category' },
            { data: 'subcategory', name: 'subcategory' },
            { data: 'nama', name: 'nama' },
            { data: 'harga_jual_default', name: 'harga_jual_default' },
            { data: 'jumlah', name: 'jumlah' },
        ],
        columnDefs: [

        ],
        fnDrawCallback: function (oSettings) {

              $("[data-toggle='confirmation']").confirmation({
                singleton: true,
                onConfirm:function(){
                  var myElement = $(this).closest('.popover').prev();
                  var id        = myElement.context.getAttribute('data-id');

                  $.ajax({
                      url: '{{url("Item")}}/'+id,
                      type: 'DELETE',
                      data: {_token: '{{csrf_token()}}'},
                      success: function(result) {
                        table.ajax.reload();

                        toastr[success]("Product Berhasil dihapus", "Success")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                      }
                  });

                }
              });
            }
    });
});


</script>

<script type="text/javascript">
        var stateChart = 0 ;
        // based on prepared DOM, initialize echarts instance
        var myChart = echarts.init(document.getElementById('echarts_bar'));

        // specify chart configuration item and data
        var option = {
            tooltip: {},
            legend: {
                data:['Jumlah']
            },
            xAxis: {
                data: []
            },
            yAxis: {},
            series: [{
                name: 'Inventory',
                type: 'bar',
                data: []
            }]
        };

        function reset(){
          // Asynchronous data loading
          $.get('{{ url("Report") }}/stokChartJson').done(function (data) {
              // fill in data
              myChart.setOption({
                  xAxis: {
                      data: data.description
                  },
                  series: [{
                      // find series by name
                      name: 'Category',
                      type: 'bar',
                      data: data.data
                  }]
              });
          });
          stateChart = 0;
        }

        reset();

        myChart.on('click', function (params) {
           if(stateChart == 0) //state Category
           {
             // Asynchronous data loading
             $.get('{{ url("Report") }}/stokChartJson?state=1&category='+params.name).done(function (data) {
                 // fill in data
                 myChart.setOption({
                     xAxis: {
                         data: data.description
                     },
                     series: [{
                         // find series by name
                         name: 'Sub Category',
                         type: 'bar',
                         data: data.data
                     }]
                 });

                 stateChart = 1;
             });
           }
           else if(stateChart == 1) //state Category
           {
             // Asynchronous data loading
             $.get('{{ url("Report") }}/stokChartJson?state=2&subCategory='+params.name).done(function (data) {
                 // fill in data
                 myChart.setOption({
                     xAxis: {
                         data: data.description
                     },
                     series: [{
                         // find series by name
                         name: 'Produk',
                         type: 'bar',
                         data: data.data
                     }]
                 });

                 stateChart = 1;
             });
           }
        });

        // use configuration item and data specified to show chart
        myChart.setOption(option);
    </script>
<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
