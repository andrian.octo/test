
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent


@component('components.echart')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
      <div class="btn-group btn-theme-panel" style="float:right">
                                <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="icon-settings">Setting</i>
                                </a>
                                <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click" style="min-width:200px">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h3>Cabang</h3>
                                            <ul class="theme-colors">
                                                <li class="theme-color theme-color-default" data-theme="default">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Dark Header</span>
                                                </li>
                                                <li class="theme-color theme-color-light active" data-theme="light">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Light Header</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <h2 class="page-title"> Laporan Penjualan
            <small></small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                  <div class="number">
                                      <span data-counter="counterup" data-value="{{$omset}}">{{$omset}} </span>Juta </div>
                                  <div class="desc"> Total Omset <?php echo Date('Y') ?></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">

                                    <div class="number">
                                      <span data-counter="counterup" data-value="{{$omset-$payed}}">{{$omset-$payed}} </span>Juta </div>
                                    <div class="desc"> Pending Payment <?php echo Date('Y') ?></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="549">549</span>
                                    </div>
                                    <div class="desc"> New Orders </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> +
                                        <span data-counter="counterup" data-value="89">89</span>% </div>
                                    <div class="desc"> Brand Popularity </div>
                                </div>
                            </a>
                        </div>
                    </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Jumlah Barang Yang Terjual</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <label class="btn red btn-outline btn-circle btn-sm active">
                                    <input type="radio" name="options" class="toggle" id="option1">New</label>
                                <label class="btn red btn-outline btn-circle btn-sm">
                                    <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="jumlah_item_chart" style="width: 100%; min-height: 400px"></div>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Jumlah Barang Yang Terjual</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <label class="btn red btn-outline btn-circle btn-sm active">
                                    <input type="radio" name="options" class="toggle" id="option1">New</label>
                                <label class="btn red btn-outline btn-circle btn-sm">
                                    <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="jumlah_revenue_chart" style="width: 100%; min-height: 400px"></div>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
          </div>
        <!-- END BODY CONTENT -->
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Bar Chart</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" onclick="reset()">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body" >
                    <div id="echarts_bar" style="width: 100%; min-height: 400px"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')

<script type="text/javascript">
        var stateChart = 0 ;
        // based on prepared DOM, initialize echarts instance
        var countItemChart = echarts.init(document.getElementById('jumlah_revenue_chart'));

        // specify chart configuration item and data
        var option = {
            tooltip: {},
            legend: {
                data:['Jumlah']
            },
            xAxis: {
                data: []
            },
            yAxis: {},
            series: [{
                name: 'Inventory',
                type: 'bar',
                data: []
            }]
        };

        function reset(){
          // Asynchronous data loading
          $.get('{{ url("Report") }}/salesChartByRevenueJSON').done(function (data) {
              // fill in data
              countItemChart.setOption({
                              xAxis: {
                                  type: 'category',
                                  data: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sept', 'Okt', 'Nov', 'Des']
                              },
                              yAxis: {
                                  type: 'value'
                              },
                              series: [{
                                  data: data.data,
                                  type: 'line',
                                  symbolSize: 5,
                                  lineStyle: {
                                      normal: {
                                          color: 'green',
                                          width: 4,
                                          type: 'dashed'
                                      }
                                  },
                                  itemStyle: {
                                      normal: {
                                          borderWidth: 3,
                                          borderColor: 'yellow',
                                          color: 'blue'
                                      }
                                  }
                              }]
                          });
          });
          stateChart = 0;
        }

        reset();

        countItemChart.on('click', function (params) {
           if(stateChart == 0) //state Category
           {
             // Asynchronous data loading
             $.get('{{ url("Report") }}/stokChartJson?state=1&category='+params.name).done(function (data) {
                 // fill in data
                 countItemChart.setOption({
                     xAxis: {
                         data: data.description
                     },
                     series: [{
                         // find series by name
                         name: 'Sub Category',
                         type: 'bar',
                         data: data.data
                     }]
                 });

                 stateChart = 1;
             });
           }
           else if(stateChart == 1) //state Category
           {
             // Asynchronous data loading
             $.get('{{ url("Report") }}/stokChartJson?state=2&subCategory='+params.name).done(function (data) {
                 // fill in data
                 countItemChart.setOption({
                     xAxis: {
                         data: data.description
                     },
                     series: [{
                         // find series by name
                         name: 'Produk',
                         type: 'bar',
                         data: data.data
                     }]
                 });

                 stateChart = 1;
             });
           }
        });

        // use configuration item and data specified to show chart
        countItemChart.setOption(option);

        $(window).on('resize', function(){
        if(countItemChart != null && countItemChart != undefined){
            countItemChart.resize();
        }
    });
    </script>
    <script type="text/javascript">
            var stateChart = 0 ;
            // based on prepared DOM, initialize echarts instance
            var revenueChart = echarts.init(document.getElementById('jumlah_item_chart'));

            // specify chart configuration item and data
            var option = {
                tooltip: {},
                legend: {
                    data:['Jumlah']
                },
                xAxis: {
                    data: []
                },
                yAxis: {},
                series: [{
                    name: 'Inventory',
                    type: 'bar',
                    data: []
                }]
            };

            function reset(){
              // Asynchronous data loading
              $.get('{{ url("Report") }}/salesChartByItemJSON').done(function (data) {
                  // fill in data
                  revenueChart.setOption({
                                  xAxis: {
                                      type: 'category',
                                      data: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sept', 'Okt', 'Nov', 'Des']
                                  },
                                  yAxis: {
                                      type: 'value'
                                  },
                                  series: [{
                                      data: data.data,
                                      type: 'line',
                                      symbolSize: 5,
                                      lineStyle: {
                                          normal: {
                                              color: 'green',
                                              width: 4,
                                              type: 'dashed'
                                          }
                                      },
                                      itemStyle: {
                                          normal: {
                                              borderWidth: 3,
                                              borderColor: 'yellow',
                                              color: 'blue'
                                          }
                                      }
                                  }]
                              });
              });
              stateChart = 0;
            }

            reset();

            revenueChart.on('click', function (params) {
               if(stateChart == 0) //state Category
               {
                 // Asynchronous data loading
                 $.get('{{ url("Report") }}/stokChartJson?state=1&category='+params.name).done(function (data) {
                     // fill in data
                     revenueChart.setOption({
                         xAxis: {
                             data: data.description
                         },
                         series: [{
                             // find series by name
                             name: 'Sub Category',
                             type: 'bar',
                             data: data.data
                         }]
                     });

                     stateChart = 1;
                 });
               }
               else if(stateChart == 1) //state Category
               {
                 // Asynchronous data loading
                 $.get('{{ url("Report") }}/stokChartJson?state=2&subCategory='+params.name).done(function (data) {
                     // fill in data
                     revenueChart.setOption({
                         xAxis: {
                             data: data.description
                         },
                         series: [{
                             // find series by name
                             name: 'Produk',
                             type: 'bar',
                             data: data.data
                         }]
                     });

                     stateChart = 1;
                 });
               }
            });

            // use configuration item and data specified to show chart
            revenueChart.setOption(option);

            $(window).on('resize', function(){
            if(countItemChart != null && countItemChart != undefined){
                revenueChart.resize();
            }
        });
        </script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
