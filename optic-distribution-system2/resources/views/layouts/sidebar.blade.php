<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="heading">
                <h3 class="uppercase">Features</h3>
            </li>

            @component('components.menuItem',[
              'icon'=>'fa fa-shopping-cart'
              ,'name'=>'Order'
            ])
            @endcomponent
            <li class="nav-item  ">
                <a href="{{url('Retur')}}/Client" class="nav-link ">
                    <i class="fa fa-tags"></i>
                    <span class="title">Manage Retur Client</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{url('Retur')}}" class="nav-link ">
                    <i class="fa fa-tags"></i>
                    <span class="title">Manage Retur Distributor</span>
                </a>
            </li>
            @component('components.menuItem',[
              'icon'=>'fa fa-tags'
              ,'name'=>'Stock'
            ])

            <li class="nav-item  ">
                <a href="{{url('Category')}}/create" class="nav-link ">
                    <i class="fa fa-tags"></i>
                    <span class="title">Tambah Kategori</span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="{{url('Item')}}/" class="nav-link ">
                    <i class="fa fa-tags"></i>
                    <span class="title">Kelola Produk</span>
                </a>
            </li>



            @endcomponent

            @component('components.menuItem',[
              'icon'=>'fa fa-shopping-cart'
              ,'name'=>'Optic'
            ])

            @endcomponent


            <li class="heading">
                <h3 class="uppercase">Administrator</h3>
            </li>

            @component('components.menuItem',[
              'icon'=>'fa fa-user-circle-o'
              ,'name'=>'User'
            ])
            @endcomponent

            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart"></i>
                    <span class="title">Laporan</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{url('Report/inventory')}}" class="nav-link ">
                            <i class="fa fa fa-book"></i>
                            <span class="title">Laporan Produk</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{url('Report/inventoryLow')}}" class="nav-link ">
                            <i class="fa fa fa-book"></i>
                            <span class="title">Laporan Produk Habis</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{url('Report/sales')}}" class="nav-link ">
                            <i class="fa fa fa-book"></i>
                            <span class="title">Laporan Penjualan</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{url('Report/expiry')}}" class="nav-link ">
                            <i class="fa fa fa-book"></i>
                            <span class="title">Laporan Expired Items</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item  ">
                        <a href="{{url('Report/receivings')}}" class="nav-link ">
                            <i class="fa fa fa-book"></i>
                            <span class="title">Laporan Pembelian</span>
                        </a>
                    </li> -->
                </ul>
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
