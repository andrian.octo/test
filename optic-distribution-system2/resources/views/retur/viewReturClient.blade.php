
@extends('layouts.app')

@component('components.select2Plugin')
@endcomponent

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Order") }}">Manage Order</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>Detail Order</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption">
                      <i class="icon-equalizer font-red-sunglo"></i>
                      <span class="caption-subject font-red-sunglo bold uppercase">Order</span>
                      <span class="caption-helper">Detail invoice</span>
                  </div>
                  <div class="tools">
                      <a href="" class="collapse"> </a>
                      <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                      <a href="" class="reload"> </a>
                      <a href="" class="remove"> </a>
                  </div>
              </div>
              <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                    <form id="order">
                      <div class="form-body" style="padding-top:0px">
                          <h3 class="form-section">Retur Information</h3>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Nama pegawai</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$sale->staff_name->username}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Tanggal Retur</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$sale->created_at}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <!--/span-->
                          </div>
                          <!--/row-->

                          <!--/row-->
                          <div class="row" style = "margin-top:10px">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Cabang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{$sale->staff_name->cabang->nama}}" readonly> </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Retur ID</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{$sale->id_string}}" readonly> </div>
                                </div>
                            </div>
                          </div>
                          <h3 class="form-section">Receiver Information</h3>
                          <!--/row-->
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Optic</label>
                                      <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{$optic->nama}}" readonly>
                                      </div>
                                  </div>
                              </div>

                          </div>

                          <div class="row" style="margin-top:10px">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Alamat Optic</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$optic->alamat}}" readonly>
                                      </div>
                                  </div>
                              </div>

                          </div>

                          <h3 class="form-section">Item</h3>
                          <div class="row table-scrollable">
                            <table class="table table-hover" id="itemSales">
                                <thead>
                                    <tr>
                                        <th> Categeory </th>
                                        <th> Sub Category </th>
                                        <th> Produk </th>
                                        <th> Pcs </th>
                                        <th> Unit Price </th>
                                        <th> Discount </th>
                                        <th> Amount </th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach ($items as $item)
                                  <tr>
                                    <td>{{$item->item->subCategory->category->nama}}</td>
                                    <td>{{$item->item->subCategory->nama}}</td>
                                    <td>{{$item->item->nama}}</td>
                                    <td>{{$item->jumlah}}</td>
                                    <td class="textRight">0</td>
                                    <td class="textRight">0
                                    </td>


                                    <td class="textRight">0</td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-md-8">
                                <h3 class="form-section">Keterangan</h3>
                                <textarea class="form-control" placeholer="Message" id="keterangan" name="keterangan" value="{{$sale->jatuh_tempo}}" readonly></textarea>
                            </div>

                          </div>

                          <!--/row-->
                      </div>
                    </form>

                      
                  <!-- END FORM-->
              </div>
          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@push('scripts')
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
@endpush

@endsection
