
@extends('layouts.app')

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Item") }}">Manage Produk</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>Edit Produk</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Edit Produk</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::model($item, ['method' => 'PATCH','route' => ['Item.update', $item->id],'class'=>"form-horizontal"]) !!}

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-4">
                          <select id="category" name="category" class="form-control">
                              <option value="0">-Pilih Kategori Produk-</option>
                               @foreach($category as $category)
                              <option value="{{$category->id}}" {{($item->subCategory->category->id==$category->id) ? 'selected' : '' }} >{{$category->nama}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Sub Kategori</label>
                        <div class="col-sm-4">
                          <select id="subcategory" name="subcategory" class="form-control">
                              <option value="{{$item->sub_category_id}}">{{$item->subCategory->nama}}</option>
                          </select>
                        </div>
                      </div>

                    <?php

                       $item = array(
                                array("text", "nama", "Nama" ,"col-sm-3","col-sm-4",$item->nama),
                                array("text", "harga_beli","Harga Beli", "col-sm-3","col-sm-4",$item->harga_beli),
                                array("text", "harga_jual_default","Harga Jual", "col-sm-3","col-sm-4",$item->harga_jual_default)
                              );

                    ?>

                    {!!NPMForm::FormGenerate($errors,$item)!!}
                     
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>

                    {{ Form::close() }}
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection

@push('scripts')
<script>

$('#category').on('change', function(e){
  var cat_id = e.target.value;
  $.get('/Category/SubCategory/'+cat_id, function(data){
    $('#subcategory').empty();
    $('#subcategory').append('<option>-Pilih Sub Kategori Produk-</option>');    
    $.each(JSON.parse(data), function(index, subcatObj){
          $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.nama+'</option>');
    });
  });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
