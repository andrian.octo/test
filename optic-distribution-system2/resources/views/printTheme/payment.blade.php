<style>
  div{
    margin:0px;
    padding:0px;
  }
  h1{
    margin:0px;
    padding:0px;
  }
  h6{
    margin:0px;
    padding:0px;
  }
  @page {
            margin-top: 15px;
            margin-left: 25px;
        }
    .viewPO{
        width:100%;
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .copPO{
        line-height: 1;
        text-align: center;
        margin-bottom: 20px;
    }
    .copPO p{
        text-align: center;
        line-height: 0.5;
        font-size: 10px;
    }
    .textCenter{
        text-align: center;
    }
    .textRight{
        text-align: right;
    }
    .ketPO{
        border: none;
        line-height: 2;
        margin-bottom: 10px;
        font-size: 12px;
    }
    .itemPO{
        width: 100%;
        font-size: 11px;
        margin-bottom: 10px;
    }
    table.itemPO, table.itemPO th, table.itemPO td{
        border:1px solid black;
        border-collapse: collapse;
    }
    table.itemPO th, table.itemPO td{
        padding: 5px;
    }
    .note{
        font-size: 8px;
        width: 60%;
        margin-bottom: 20px;
    }
    table.footer{
        width: 100%;
        border: 1px solid black;
        border-collapse: collapse;
    }
    .auth{
        width: 90%;
        margin-left: 10%;
        font-size: 12px;
    }
</style>

<div class="viewPO">


    <div style="height:70px;width:100%">
      <div style="float:left">
        <h1 >PAYMENT</h1>
      </div>
      <div style="float:right">
        <h6 style="margin-bottom:0px;">Optic Gajah Mada</h6>
        <p style="font-size:8px;margin:0px">Jalan Mugas Barat VII / 17A, Semarang 50243</p>
        <p style="font-size:8px;margin:0px">Phone:(024)844 1611, Fax:(024)831 9499</p>
        <p style="font-size:8px;margin:0px">sp-semarang@hoyalens.co.id</p>
      </div>
    </div>


    <div style="height:18px">
      <div style="width:120px;font-size:11px;margin:0px;margin-right:5px;float:left">
        Invoice Number
      </div>
      <div style="width:120px;font-size:11px;margin:0px;float:left">
        : {{$sale->id_string}}
      </div>

      <div style="width:120px;font-size:11px;margin:0px;margin-right;margin-left:50px;5px;float:left">
        Client
      </div>
      <div style="width:120px;font-size:11px;margin:0px;float:left">
        :{{$sale->optic->nama}}
      </div>

    </div>
    <div style="height:18px">
      <div style="width:120px;font-size:11px;margin:0px;margin-right:5px;float:left">
        Date
      </div>
      <div style="width:120px;font-size:11px;margin:0px;float:left">
        : {{$payment->created_at}}
      </div>
      <div style="width:120px;font-size:11px;margin:0px;margin-right;margin-left:50px;5px;float:left">
        Address
      </div>
      <div style="width:500px;font-size:11px;margin:0px;float:left">
        :{{$sale->optic->alamat}}
      </div>
    </div>

    <div style="height:18px">
      <div style="width:120px;font-size:11px;margin:0px;margin-right:5px;float:left">
        Trans Type
      </div>
      <div style="width:120px;font-size:11px;margin:0px;float:left">
        : Payment
      </div>
    </div>

    <div style="height:18px">
      <div style="width:120px;font-size:11px;margin:0px;margin-right:5px;float:left">
        Credit Term
      </div>
      <div style="width:120px;font-size:11px;margin:0px;float:left">
        <?php
        ?>
        : {{$sale->jatuh_tempo}}
      </div>
    </div>

    <table class="itemPO">
        <tr>
            <th style="width:10px">No</th>
            <th>Method of payment</th>
            <th>Amount</th>
        </tr>

        <tr>
          <td>1</td>
          <td>{{$payment->metode_pembayaran}}</td>
          <td>{{$payment->jumlah}}</td>
        </tr>
        <tr>
            <td colspan="2" class="textRight">GRAND TOTAL</td>
            <td class="textRight">{{$payment->jumlah}}</td>
        </tr>

    </table>
    <table style="margin-bottom:0px;float:right" class="ketPO">
        <tr>
            <td style="width:500px"> </td>
            <td style="width:120px">Receive By</td>
        </tr>
        <tr>
            <td style="width:500px"> </td>
            <td style="width:120px"><br>
              (
              @for ($i = 0; $i < 20; $i++)
                  &nbsp;
              @endfor
              )</td>
        </tr>

    </table>


</div>
