<style>
  div{
    margin:0px;
    padding:0px;
  }
  h1{
    margin:0px;
    padding:0px;
  }
  h6{
    margin:0px;
    padding:0px;
  }
  @page {
            margin-top: 15px;
            margin-left: 25px;
        }
    .viewPO{
        width:100%;
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .copPO{
        line-height: 1;
        text-align: center;
        margin-bottom: 20px;
    }
    .copPO p{
        text-align: center;
        line-height: 0.5;
        font-size: 10px;
    }
    .textCenter{
        text-align: center;
    }
    .textRight{
        text-align: right;
    }
    .ketPO{
        border: none;
        line-height: 2;
        margin-bottom: 10px;
        font-size: 12px;
    }
    .itemPO{
        width: 100%;
        font-size: 11px;
        margin-bottom: 10px;
    }
    table.itemPO, table.itemPO th, table.itemPO td{
         border-collapse:collapse;
      border-bottom: 1pt solid black;
    }
    table.itemPO th, table.itemPO td{
        padding: 5px;

    }

    .itemBilling{
        width: 100%;
        font-size: 11px;
        margin-bottom: 10px;
    }

    table.itemBilling, table.itemBilling th, table.itemBilling td{
      border-collapse:collapse;
      border-bottom: 1pt solid black;
    }
    table.itemBilling th, table.itemBilling td{
        padding: 5px;
    }

    .note{
        font-size: 8px;
        width: 60%;
        margin-bottom: 20px;
    }
    table.footer{
        width: 100%;
        border: 1px solid black;
        border-collapse: collapse;
    }
    .auth{
        width: 90%;
        margin-left: 10%;
        font-size: 12px;
    }
</style>

<div class="viewPO">


    <div style="height:70px;width:100%">
      <div style="float:left">
        <h1 >TANDA TERIMA TAGIHAN</h1>
        <p style="font-size:8px;margin:0px">PERIODE : <?php echo date("F Y") ?></p>
      </div>
      <div style="float:right">
        <h6 style="margin-bottom:0px;">SOG</h6>
        <p style="font-size:8px;margin:0px">Jalan Mugas Barat VII / 17A, Semarang 50243</p>
        <p style="font-size:8px;margin:0px">Phone:(024)844 1611, Fax:(024)831 9499</p>
        <p style="font-size:8px;margin:0px">sp-semarang@hoyalens.co.id</p>
      </div>
    </div>


    

    <table class="itemBilling" frame="border">
      <tr>
        <td>CUSTOMER</td>
        <td>:</td>
        <td><b>{{$optic->nama}}</b></td>
        
        <td>&nbsp;</td>

        <td>CT</td>
        <td>:</td>
        <td>{{$ct}}</td>
      </tr>

      <tr>
        <td>ADDRESS</td>
        <td>:</td>
        <td>{{$optic->alamat}}</td>
        
        <td>&nbsp;</td>

        <td>Jatuh Tempo</td>
        <td>:</td>
        <td>{{$jatuh_tempo}}</td>
      </tr>
    </table>

    

    <table class="itemPO" frame="border">
        <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">NO. INVOICE</th>
            <th style="text-align: center;">TGL. INVOICE</th>
            <th style="text-align: center;" colspan="2" class="text-center">JUMLAH</th>
        </tr>
        
        @foreach($billingList as $billing)  
        <tr>
            <td style="text-align: center;">{{$billing['no']}}</td>
            <td style="text-align: center;">{{$billing['invoiceID']}}</td>
            <td style="text-align: center;">{{$billing['tanggal']}}</td>
            <td style="text-align: center;">Rp.</td>
            <td style="text-align: right;">{{number_format($billing['total'],2,".",".")}}</td>
        </tr>
        @endforeach

        <!-- footer -->

        <tr>
          <td style="text-align: center;"><b>TOTAL</b></td>
          <td colspan="2"></td>
          <td style="text-align: center;">Rp.</td>
          <td style="text-align: right;">{{number_format($total_billing,2,".",".")}}</td>
        </tr>

        <tr>
          <td>BIAYA FACET</td>
          <td colspan="2"></td>
          <td style="text-align: center;">Rp.</td>
          <td style="text-align: right;">{{number_format($facet,2,".",".")}}</td>
        </tr>

        <tr>
          <td>BIAYA TINTING</td>
          <td colspan="2"></td>
          <td style="text-align: center;">Rp.</td>
          <td style="text-align: right;">{{number_format($tinting,2,".",".")}}</td>
        </tr>

        <tr>
          <td>BIAYA KIRIM</td>
          <td colspan="2"></td>
          <td style="text-align: center;">Rp.</td>
          <td style="text-align: right;">{{number_format($kirim,2,".",".")}}</td>
        </tr>

        <tr>
          <td>BIAYA LAIN-LAIN</td>
          <td colspan="2"></td>
          <td style="text-align: center;">Rp.</td>
          <td style="text-align: right;">{{number_format($lain,2,".",".")}}</td>
        </tr>

        <tr>
          <td><b>JUMLAH YANG HARUS DIBAYAR</b></td>
          <td colspan="2"></td>
          <td style="text-align: center;">Rp.</td>
          <td style="text-align: right;"><b>{{number_format($total_billing+$facet+$tinting+$kirim+$lain,2,".",".")}}</b></td>
        </tr>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Mohon Pembayaran ditransfer ke : </p>
            <p><b>BCA<br/> KCU KUDUS<br/> A/C. 031 9650 671 <br /> A/N. NUR ELIZA RAHMANIA </b>
          </p>

          </td>
        </tr>

    </table>
    


</div>
