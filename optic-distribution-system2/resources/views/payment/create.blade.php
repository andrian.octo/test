
@extends('layouts.app')

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Payment") }}">Manage Payment</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>New Payment</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Create New Payment Invoice </span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    {{ Form::model($model, ['action' => 'PaymentController@store','class'=>"form-horizontal"]) }}

                    <div class="form-group ">
                      <label for="nama" class="col-sm-3 control-label">Metode Pembayaran</label>
                      <div class="col-sm-8">
                        <select class="form-control select2" data-placeholder="Choose Optic" id="metode" name="metode" tabindex="1">
                          <option value="1">Cash</option>
                          <option value="2">Transfer</option>
                        </select>
                      </div>
                    </div>
                    <input type="hidden" name="jenis" id="jenis" value="{{$sale->jenis_pembayaran}}">
                    <input placeholder="Input Jumlah" name="sale_id" type="text" value="{{$sale_id}}" id="sale_id" hidden />

                    <?php
                       $item = array(
                                array("text", "jumlah", "Jumlah" ,"col-sm-3","col-sm-8",'0')
                              );
                    ?>

                    {!!NPMForm::FormGenerate($errors,$item)!!}

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>

                    {{ Form::close() }}
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection
