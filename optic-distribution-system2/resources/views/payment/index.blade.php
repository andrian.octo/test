
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> List Payment
            <small>detail & manage payment</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>

              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-equalizer font-green-haze"></i>
                          <span class="caption-subject font-green-haze bold uppercase">Invoice Info</span>
                          @if($sale->total!=$sale->total_terbayar)
                            <span class="label label-danger"> Pending </span>
                          @endif
                          @if($sale->total==$sale->total_terbayar)
                            <span class="label label-success"> Lunas </span>
                          @endif

                      </div>
                      <div class="tools">
                          <a href="" class="collapse" data-original-title="" title=""> </a>
                          <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                          <a href="" class="reload" data-original-title="" title=""> </a>
                          <a href="" class="remove" data-original-title="" title=""> </a>
                      </div>
                  </div>
                  <div class="portlet-body form">
                      <!-- BEGIN FORM-->
                      <form class="form-horizontal" role="form">
                          <div class="form-body">
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Invoice Id:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static"> {{$id_string}} </p>
                                          </div>
                                      </div>
                                  </div>
                                  <!--/span-->
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Optik:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static"> {{$sale->optic->nama}} </p>
                                          </div>
                                      </div>
                                  </div>
                                  <!--/span-->
                              </div>
                              <!--/row-->
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Sisa Tagihan:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static">Rp {{number_format($sale->total,0)}} </p>
                                          </div>
                                      </div>
                                  </div>
                                  <!--/span-->
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Alamat Optik:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static"> {{$sale->optic->alamat}} </p>
                                          </div>
                                      </div>
                                  </div>
                                  <!--/span-->
                              </div>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Terbayar:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static">Rp {{number_format($sale->total_terbayar,0)}} </p>
                                          </div>
                                      </div>
                                  </div>
                                  <!--/span-->
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Jatuh Tempo:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static"> {{$sale->jatuh_tempo}} </p>
                                          </div>
                                      </div>
                                  </div>
                                  <!--/span-->
                              </div>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Metode:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static">{{$sale->jenis_string}} </p>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </form>
                      <!-- END FORM-->
                  </div>
              </div>

              <div class="portlet light ">
                  <div class="portlet-title">
                      <a href="{{url('/Payment')}}/{{$id}}/create" type="button" class="btn blue btn-outline">New Payment</a>
                  </div>
                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        payments
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Tanggal
                        </th>
                        <th>
                          Metode Pembayaran
                        </th>
                        <th>
                          Jumlah
                        </th>
                        <th>
                          PIC
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
      var win = window.open('{{url("Print")}}/{{$id}}/{{$maxPayment}}', '_blank');
    @endif

    var table = $('#payments').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("Payment") }}/{{$id}}/datatable',
        columns: [
            { data: 'created_at', name: 'created_at' },
            { data: 'metode', name: 'metode' },
            { data: 'jumlah', name: 'jumlah' },
            { data: 'user_id', name: 'user_id' },
            { data: 'action', name: 'action',Sortable: "false"}
        ],
        columnDefs: [
          { className: "dt-body-center", "targets": [ 2,3] },
          { width: "70px", targets: [2] },
          { width: "150px",orderable: false, targets: [3] }
        ],
        fnDrawCallback: function (oSettings) {

              $("[data-toggle='confirmation']").confirmation({
                singleton: true,
                onConfirm:function(){
                  var myElement = $(this).closest('.popover').prev();
                  var id        = myElement.context.getAttribute('data-id');

                  $.ajax({
                      url: '{{url("Optic")}}/'+id,
                      type: 'DELETE',
                      data: {_token: '{{csrf_token()}}'},
                      success: function(result) {
                        table.ajax.reload();

                        toastr[success]("Payment Berhasil dihapus", "Success")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                      }
                  });

                }
              });
            }
    });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
