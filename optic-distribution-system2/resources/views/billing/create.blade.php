
@extends('layouts.app')

@component('components.select2Plugin')
@endcomponent

@component('components.toastrNotif')
@endcomponent

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Order") }}">Manage Order</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>Create Billing</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption">
                      <i class="icon-equalizer font-red-sunglo"></i>
                      <span class="caption-subject font-red-sunglo bold uppercase">Billing</span>
                      <span class="caption-helper">Create new Billing</span>
                  </div>
                  <div class="tools">
                      <a href="" class="collapse"> </a>
                      <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                      <a href="" class="reload"> </a>
                      <a href="" class="remove"> </a>
                  </div>
              </div>
              <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                    {{ Form::model($model, ['action' => 'BillingController@store','class'=>"form-horizontal"]) }}
                      <div class="form-body" style="padding-top:0px">
                          <h3 class="form-section">Billing Information</h3>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Nama pegawai</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$user->name}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Tanggal Invoice</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$date}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <!--/span-->
                          </div>
                          <!--/row-->

                          <!--/row-->
                          <div class="row" style = "margin-top:10px">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Cabang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{$user->cabang->nama}}" readonly> </div>
                                </div>
                            </div>
                          </div>
                          <h3 class="form-section">Receiver Information</h3>
                          <!--/row-->
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Optic</label>
                                      <div class="col-md-9">
                                        <select class="form-control select2" data-placeholder="Choose Optic" id="opticSelect" name="opticSelect" tabindex="1">
                                          <option value="-99">Choose Optic</option>
                                          @foreach ($listOptic as $optic)
                                            <option value="{{$optic->id}}">{{$optic->nama}} - {{$optic->kota}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">CT</label>
                                      <div class="col-md-9">
                                          <input id="plafonOptic" name="ct" type="textarea" class="form-control" value="">
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row" style="margin-top:10px">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Alamat Optic</label>
                                      <div class="col-md-9">
                                          <input id="alamatOptic" type="textarea" class="form-control" value="" readonly>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Jatuh Tempo</label>
                                      <div class="col-md-9">
                                          <input id="jatuh_tempo" type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" name="jatuh_tempo" value="">
                                      </div>
                                  </div>
                              </div>
                              
                          </div>
                          

                          <h3 class="form-section">Invoice</h3>
                          <div class="row table-scrollable">
                            <table class="table table-hover" id="itemSales">
                                <thead>
                                    <tr>
                                        <th> NO </th>
                                        <th> NO. INVOICE </th>
                                        <th> TGL. INVOICE </th>
                                        <th colspan="2"> JUMLAH </th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                          </div>
                          

                          <!--/row-->
                      </div>
                    

                      <div class="form-actions right">
                          <button id="submitButton" type="submit" onClick="submit()" class="btn blue">
                              <i class="fa fa-check"></i> Submit</button>
                      </div>
                  <!-- END FORM-->
                  </form>
              </div>
          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@push('scripts')
  <script>

    $( document ).ready(function() {

    $(".select2").select2();
  });


    var idCount = 0;

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    

    $('#opticSelect').on('select2:select', function (e) {
      var data = e.params.data;
      var id = data.id;
      $.ajax({
          url: '{{url("billing")}}/json',
          type: 'PATCH',
          data: {id : id},
          success: function(result) {
            $("#itemSales tbody").empty();
            $("#alamatOptic").val(result.optic.alamat);
            
            var sales = result.sales;
            if (sales.length<1) {
              toastr.error("Optik Tidak Memiliki Pembayaran Pending", "Alert");
            }else{
              var x = 1;
              $.each(result.sales, function (i) {
                var invoiceID = result.sales[i].id;
                var tanggal = result.sales[i].created_at;                  
                var total = result.sales[i].total;

                 var item =
                  "<tr>"+
                    "<td> "+x +"</td>"+
                    "<td> "+invoiceID +"</td>"+
                    "<td> "+tanggal +"</td>"+
                    "<td>Rp.</td>"+
                    "<td> "+total.toLocaleString() +"</td>"+
                    "<td> <input type='checkbox' name='invoiceNumber[]' value='"+invoiceID+"' class='form-control' checked> </td>"+
                  "</tr>";
                $("#itemSales tbody").append(item);
                x++;
              });

              var itemFooter =
                  "<tr>"+
                    "<td colspan='3'> <strong>BIAYA FACET</strong> </td>"+"<td>Rp.</td>"+"<td colspan='2'> <input type='text' name='facet' class='form-control'> </td>"+
                  "</tr>"+
                  "<tr>"+
                    "<td colspan='3'> <strong>BIAYA TINTING</strong> </td>"+"<td>Rp.</td>"+"<td colspan='2'> <input type='text' name='tinting' class='form-control'> </td>"+
                  "</tr>"+
                  "<tr>"+
                    "<td colspan='3'> <strong>BIAYA KIRIM</strong> </td>"+"<td>Rp.</td>"+"<td colspan='2'> <input type='text' name='kirim' class='form-control'> </td>"+
                  "</tr>"+
                  "<tr>"+
                    "<td colspan='3'> <strong>BIAYA LAIN LAIN</strong> </td>"+"<td>Rp.</td>"+"<td colspan='2'> <input type='text' name='lain' class='form-control'> </td>"+
                  "</tr>";
                $("#itemSales tbody").append(itemFooter);

             
            }

            
            
          },
          error: function (xhr, status, errorThrown) {
            
          }
      });
    });

    




  </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
@endpush

@endsection
