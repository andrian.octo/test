
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> List Produk
            <small>detail & manage Produk</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


              <div class="portlet light ">
                  <div class="portlet-title">
                      <a href="{{url('/Item/create')}}" type="button" class="btn blue btn-outline">New Data Produk</a>
                  </div>
                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        item
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Kategori
                        </th>
                        <th>
                          Sub Kategori
                        </th>
                        <th>
                          Nama
                        </th>
                        <th>
                          Harga Beli
                        </th>
                        <th>
                          Harga Jual
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    var table = $('#item').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("Item") }}/datatable',
        columns: [
            { data: 'category', name: 'category' },
            { data: 'subcategory', name: 'subcategory' },
            { data: 'nama', name: 'nama' },
            { data: 'harga_beli', name: 'harga_beli' },
            { data: 'harga_jual_default', name: 'harga_jual_default' },
            { data: 'action', name: 'action',Sortable: "false"}
        ],
        columnDefs: [

        ],
        fnDrawCallback: function (oSettings) {

              $("[data-toggle='confirmation']").confirmation({
                singleton: true,
                onConfirm:function(){
                  var myElement = $(this).closest('.popover').prev();
                  var id        = myElement.context.getAttribute('data-id');

                  $.ajax({
                      url: '{{url("Item")}}/'+id,
                      type: 'DELETE',
                      data: {_token: '{{csrf_token()}}'},
                      success: function(result) {
                        if (result.status=="OK") {
                          table.ajax.reload();
                          toastr.success("Item Berhasil dihapus", "Success");
                        }else{
                          toastr.success(result.message, "Warning");
                        }


                      }
                  });

                }
              });
            }
    });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
