
@extends('layouts.app')

@component('components.select2Plugin')
@endcomponent

@component('components.toastrNotif')
@endcomponent

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Order") }}">Manage Order</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>Detail Order</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption">
                      <i class="icon-equalizer font-red-sunglo"></i>
                      <span class="caption-subject font-red-sunglo bold uppercase">Order</span>
                      <span class="caption-helper">Detail invoice</span>
                  </div>
                  <div class="tools">
                      <a href="" class="collapse"> </a>
                      <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                      <a href="" class="reload"> </a>
                      <a href="" class="remove"> </a>
                  </div>
              </div>
              <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                    <form id="order">
                      <div class="form-body" style="padding-top:0px">
                          <h3 class="form-section">Invoice Information</h3>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Nama pegawai</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$sale->staff_name->username}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Tanggal Invoice</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$sale->created_at}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <!--/span-->
                          </div>
                          <!--/row-->

                          <!--/row-->
                          <div class="row" style = "margin-top:10px">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Cabang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{$sale->staff_name->cabang->nama}}" readonly> </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">INVOICE ID</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{$sale->id_string}}" readonly> </div>
                                </div>
                            </div>
                          </div>
                          <h3 class="form-section">Receiver Information</h3>
                          <!--/row-->
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Optic</label>
                                      <div class="col-md-9">
                                        <select class="form-control select2" data-placeholder="Choose Optic" id="opticSelect" name="opticSelect" tabindex="1">
                                          @foreach ($listOptic as $optic)
                                            <option value="{{$optic->id}}">{{$optic->nama}} - {{$optic->kota}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Pembayaran</label>
                                      <div class="col-md-9">
                                        <select class="form-control" id="jenis_pembayaran" name="jenis_pembayaran" data-placeholder="" tabindex="1">
                                            <option value="1" {{($sale->jenis_pembayaran == 1) ? "selected" : ""}}>Sekali Bayar</option>
                                            <option value="2" {{ ($sale->jenis_pembayaran == 2) ? 'selected' : '' }}>Bertahap</option>
                                        </select>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row" style="margin-top:10px">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Alamat Optic</label>
                                      <div class="col-md-9">
                                          <input type="text" id="alamatOptic" class="form-control" value="{{$sale->optic->alamat}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Batas Tempo</label>
                                      <div class="col-md-9">
                                          <input type="text" class="form-control" value="{{$tempo+1}}" id="tempo" name="tempo" >
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="row" style="margin-top:10px">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Plafon</label>
                                      <div class="col-md-9">
                                          <input id="plafonOptic" type="textarea" class="form-control" value="{{number_format($sale->optic->sisa_plafon + $sale->total,0)}}" readonly>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-3">Last Term Credit</label>
                                      <div class="col-md-9">
                                          <input id="lastDateOptic" type="textarea" class="form-control" value="{{$sale->optic->last_order}}" readonly>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <h3 class="form-section">Item</h3>
                          <div class="row table-scrollable">
                            <table class="table table-hover" id="itemSales">
                                <thead>
                                    <tr>
                                        <th> Categeory </th>
                                        <th> Sub Category </th>
                                        <th> Produk </th>
                                        <th> Pcs </th>
                                        <th> Unit Price </th>
                                        <th> Discount </th>
                                        <th> Amount </th>
                                        <th colspan="2"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $index = 0;
                                  ?>
                                  @foreach ($items as $item)

                                  <?php
                                    $diskon = $item->pivot->jumlah * $item->pivot->harga_jual * $item->pivot->diskon / 100;
                                    $total  = $item->pivot->jumlah * $item->pivot->harga_jual;
                                    $total  = $total - $diskon;
                                  ?>

                                  <tr id="item{{$index}}">
                                    <td id="cat{{$index}}" name="cat" style="display:none;">{{$item->subCategory->category->id}}</td>
                                    <td>{{$item->subCategory->category->nama}}</td>
                                    <td id="subCat{{$index}}" name="subCat" style="display:none;">{{$item->subCategory->id}}</td>
                                    <td>{{$item->subCategory->nama}}</td>
                                    <td id="desc{{$index}}" name="desc" style="display:none;">{{$item->id}}</td>
                                    <td>{{$item->nama}}</td>
                                    <td id="count{{$index}}" name="count">{{$item->pivot->jumlah}}</td>
                                    <td id="price{{$index}}" name="price" class="textRight">{{number_format($item->pivot->harga_jual,0)}}</td>
                                    <td id="disc{{$index}}" name="disc" class="textRight">{{$item->pivot->diskon}}</td>
                                    <td id="amount{{$index}}" name="amount" class="textRight">{{number_format($total,0)}}</td>
                                    <td class="editButton" style="float:right">
                                      <input type="button" class="btn green btn-outline" onclick="editForm({{$index}})" value="Edit">
                                    </td>
                                    <td class="deleteButton">
                                      <input type="button" class="btn red btn-outline" onclick="deleteForm({{$index}})" value="Delete">
                                    </td>
                                  </tr>
                                  <?php $index++; ?>
                                  @endforeach
                                </tbody>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-md-8">
                                <h3 class="form-section">Keterangan</h3>
                                <textarea class="form-control" placeholer="Message" id="keterangan" name="keterangan" value="{{$sale->jatuh_tempo}}"></textarea>
                            </div>
                            <div class="col-md-4">
                                <h3 class="form-section">Total</h3>
                                <h4 id="totalAmount">Rp {{$sale->total}}</h4>
                            </div>
                          </div>

                          <!--/row-->
                      </div>
                    </form>

                    <div class="form-actions right">
                        <button id="submitButton" type="submit" onClick="submit()" class="btn blue">
                            <i class="fa fa-check"></i> Submit</button>
                    </div>
                  <!-- END FORM-->
              </div>
          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@push('scripts')
  <script>

    var idCount = {{$index}};

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function checkOpticPlafon(){
      var total = numeral($("#totalAmount").text().replace("Rp ","")).value();
      if(numeral($("#plafonOptic").val()).value() > total){
          $("#submitButton").prop('disabled', false);
      }
      else{
        $("#submitButton").prop('disabled', true);
        toastr.error("Plafon Tidak Mencukupi", "Alert")
      }
    }

    $('#opticSelect').on('select2:select', function (e) {
      var data = e.params.data;
      var id = data.id;
      $.ajax({
          url: '{{url("Optic")}}/json',
          type: 'PATCH',
          data: {id : id},
          success: function(result) {
            $("#alamatOptic").val(result.alamat);
            $("#plafonOptic").val(numeral(result.sisa_plafon + {{$sale->total}}).format());
            $("#lastDateOptic").val(result.last_order);

            checkOpticPlafon();

            if(result.checkDate == 0){
              $("#submitButton").prop('disabled', true);
              toastr.error("Optik tidak diperbolehkan.", "Alert")
            }

          },
          error: function (xhr, status, errorThrown) {
            $("#alamatOptic").val("");
            $("#plafonOptic").val("");
            $("#lastDateOptic").val("");
            $("#submitButton").prop('disabled', true);
          }
      });
    });

    function submit(){
      var dataPass  = $('#order').serializeArray();

      var arrCat    = $("td[id*='cat']");
      var arrSubCat = $("td[id*='subCat']");
      var arrItem   = $("td[id*='desc']");
      var arrCount  = $("td[id*='count']");
      var arrPrice  = $("td[id*='price']");
      var arrDisc   = $("td[id*='disc']");
      var arrAmount = $("td[id*='amount']");
      var itemCounts = 0;
      var totalAmount = $("#totalAmount").text();


      for(var i = 0; i<arrCat.length;i++){
        dataPass.push({name: arrCat[i].attributes.name.value+''+i, value: arrCat[i].textContent});
        itemCounts++;
      }
      for(var i = 0; i<arrSubCat.length;i++){
        dataPass.push({name: arrSubCat[i].attributes.name.value+''+i, value: arrSubCat[i].textContent});
      }
      for(var i = 0; i<arrItem.length;i++){
        dataPass.push({name: arrItem[i].attributes.name.value+''+i, value: arrItem[i].textContent});
      }
      for(var i = 0; i<arrCount.length;i++){
        dataPass.push({name: arrCount[i].attributes.name.value+''+i, value: arrCount[i].textContent});
      }
      for(var i = 0; i<arrPrice.length;i++){
        dataPass.push({name: arrPrice[i].attributes.name.value+''+i, value: numeral(arrPrice[i].textContent).value()});
      }
      for(var i = 0; i<arrDisc.length;i++){
        dataPass.push({name: arrDisc[i].attributes.name.value+''+i, value: arrDisc[i].textContent});
      }
      for(var i = 0; i<arrAmount.length;i++){
        dataPass.push({name: arrAmount[i].attributes.name.value+''+i, value: numeral(arrAmount[i].textContent).value()});
      }
      dataPass.push({name: 'itemCounts', value: itemCounts});

      totalAmount = totalAmount.replace("Rp","");
      totalAmount = numeral(totalAmount.trim()).value();

      dataPass.push({name: 'totalAmount', value: totalAmount});

      $.ajax({
          url: '{{url("Order")}}/editSales/{{$sale->id}}',
          type: 'POST',
          data: dataPass,
          success: function(result) {
            if(result.status == 'OK'){
              var win = window.open('{{url("Print")}}/'+result.id, '_blank');
              window.location.href = '{{url("Order")}}';
            }
            else{
                toastr.error(result.message, "Alert")
            }
          }
      });
    }

    $("#opticSelect").select2().select2("val", '{{$sale->optic->id}}');;


  </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="{{asset("js/itemTable.js")}}" type="text/javascript"></script>
@endpush

@endsection
