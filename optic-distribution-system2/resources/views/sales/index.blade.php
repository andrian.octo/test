
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> List Order
            <small>detail & manage order</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


              <div class="portlet light ">
                  <div class="portlet-title">
                      <a href="{{url('/Order/create')}}" type="button" class="btn blue btn-outline">New Data Order</a>
                       <a href="{{url('/Billing/create')}}" target="_blank" type="button" class="btn red btn-outline pull-right">Billing</a>
                  </div>
                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        sales
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Tanggal
                        </th>
                        <th>
                          Invoice ID
                        </th>
                        <th>
                          Optic
                        </th>
                        <th>
                          Total
                        </th>
                        <th>
                          Jenis Pembayaran
                        </th>
                        <th>
                          Status Pembayaran
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    var table = $('#sales').DataTable({
        processing: true,
        serverSide: true,
        order: [[ 1, "desc" ]],
        ajax: '{{ url("Order") }}/datatable',
        columns: [
            { data: 'created_at', name: 'created_at' },
            { data: 'id', name: 'id' },
            { data: 'optic_name', name: 'optic_name' },
            { data: 'total', name: 'total' },
            { data: 'jenis_pembayaran', name: 'jenis_pembayaran' },
            { data: 'status_pembayaran', name: 'status_pembayaran' },
            { data: 'action', name: 'action',Sortable: "false"}
        ],
        columnDefs: [
          { className: "dt-body-center", "targets": [ 2,3] },
          { width: "70px", targets: [2] },
          { width: "270px", targets: [6] },
          { width: "150px",orderable: false, targets: [3] },
          {
            render: function ( data, type, row ) {
                    return "Rp "+numeral(data).format();
                },
                targets: 3
          }
        ],
        fnDrawCallback: function (oSettings) {

              $("[data-toggle='confirmation']").confirmation({
                singleton: true,
                onConfirm:function(){
                  var myElement = $(this).closest('.popover').prev();
                  var id        = myElement.context.getAttribute('data-id');

                  $.ajax({
                      url: '{{url("Order")}}/'+id,
                      type: 'DELETE',
                      data: {_token: '{{csrf_token()}}'},
                      success: function(result) {
                        table.ajax.reload();

                        toastr.success("Order Berhasil dihapus", "Success");

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                      }
                  });

                }
              });
            }
    });
});


</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
