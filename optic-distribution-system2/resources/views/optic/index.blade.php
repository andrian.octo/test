
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> List Optic
            <small>detail & manage optic</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


              <div class="portlet light ">
                  <div class="portlet-title">
                      <a href="{{url('/Optic/create')}}" type="button" class="btn blue btn-outline">New Data Optic</a>
                  </div>
                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        optics
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Nama
                        </th>
                        <th>
                          Kota
                        </th>
                        <th>
                          Alamat
                        </th>
                        <th>
                          Telepon
                        </th>
                        <th>
                          Plafon
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    var table = $('#optics').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("Optic") }}/datatable',
        columns: [
            { data: 'nama', name: 'nama' },
            { data: 'kota', name: 'kota' },
            { data: 'alamat', name: 'alamat' },
            { data: 'telepon', name: 'telepon' },
            { data: 'plafon', name: 'plafon' },
            { data: 'action', name: 'action',Sortable: "false"}
        ],
        columnDefs: [
          { className: "dt-body-center", "targets": [ 2,3] },
          { width: "70px", targets: [2] },
          { width: "150px",orderable: false, targets: [3] }
        ],
        fnDrawCallback: function (oSettings) {

              $("[data-toggle='confirmation']").confirmation({
                singleton: true,
                onConfirm:function(){
                  var myElement = $(this).closest('.popover').prev();
                  var id        = myElement.context.getAttribute('data-id');

                  $.ajax({
                      url: '{{url("Optic")}}/'+id,
                      type: 'DELETE',
                      data: {_token: '{{csrf_token()}}'},
                      success: function(result) {
                        if (result.status=="OK") {
                          table.ajax.reload();
                          toastr.success("Optik Berhasil dihapus", "Success");
                        }else{
                          toastr.success(result.message, "Warning");
                        }
                      }
                  });

                }
              });
            }
    });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
