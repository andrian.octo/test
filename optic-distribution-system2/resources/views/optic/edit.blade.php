
@extends('layouts.app')

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">

      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Optic") }}">Manage Optic</a>
                      <i class="fa fa-angle-right"></i>
                  </li>

                  <li>
                      <span>Edit Optic</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Create New Optic</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::model($optic, ['method' => 'PATCH','route' => ['Optic.update', $optic->id],'class'=>"form-horizontal"]) !!}

                    <div class="form-group">
                        <label class="control-label col-md-3">Kota</label>
                        <div class="col-md-8">
                          <select class="form-control" id="kota" name="kota" data-placeholder="" tabindex="1">
                              <option value="1" {{ ($optic->cabang_id == 1) ? 'selected' : '' }}>Kudus</option>
                              <option value="2" {{ ($optic->cabang_id == 2) ? 'selected' : '' }}>Semarang</option>
                          </select>
                        </div>
                    </div>
                    <?php

                       $item = array(
                                array("text", "nama", "Nama" ,"col-sm-3","col-sm-8",$optic->nama),
                                array("text", "alamat","Alamat", "col-sm-3","col-sm-8",$optic->alamat),
                                array("text", "telepon","telepon", "col-sm-3","col-sm-8",$optic->telepon),
                                array("text", "plafon","plafon", "col-sm-3","col-sm-8",$optic->plafon),
                              );
                    ?>

                    {!!NPMForm::FormGenerate($errors,$item)!!}

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>

                    {{ Form::close() }}
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection
