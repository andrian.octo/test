
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@component('components.datatablePlugin')
@endcomponent

@component('components.modalBasic',['title'=>'Create New Category','id'=>'modalCategory'])

  @slot('footer')
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="button" onClick="insertCategory()" class="btn green">Submit</button>
  @endslot

  <div class="row">
    <div class="form-group">
        <label class="col-md-3 control-label" style="margin-top:5px">Nama Category</label>
        <div class="col-md-9">
            <input type="text" id="textCategory" name="textCategory" class="form-control" placeholder="Input Nama Category">
        </div>
    </div>
  </div>

  <div class="row">
      <div class="form-group">
          <label class="col-md-3 control-label" style="margin-top:5px">Expiry Date</label>
          <div class="col-md-9">
              <div class="mt-checkbox-list">
                  <label class="mt-checkbox mt-checkbox-outline">
                      <input id="expiryCheck" name="expiryCheck[]" value="1" type="checkbox">
                      <span></span>
                  </label>
              </div>
          </div>
      </div>
  </div>
@endcomponent

@component('components.modalBasic',['title'=>'Edit Category','id'=>'editModalCategory'])

  @slot('footer')
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="button" onClick="updateCategory()" class="btn green">Submit</button>
  @endslot

  <input type="hidden" id="idEditCategory" name="idEditCategory" class="form-control" placeholder="Input Nama Category">

  <div class="row">
    <div class="form-group">
        <label class="col-md-3 control-label" style="margin-top:5px">Nama Category</label>
        <div class="col-md-9">
            <input type="text" id="textEditCategory" name="textEditCategory" class="form-control" placeholder="Input Nama Category">
        </div>
    </div>
  </div>

  <div class="row">
      <div class="form-group">
          <label class="col-md-3 control-label" style="margin-top:5px">Expiry Date</label>
          <div class="col-md-9">
              <div class="mt-checkbox-list">
                  <label class="mt-checkbox mt-checkbox-outline">
                      <input id="expiryEditCheck" name="expiryEditCheck[]" value="1" type="checkbox">
                      <span></span>
                  </label>
              </div>
          </div>
      </div>
  </div>
@endcomponent

@component('components.modalBasic',['title'=>'Create New Sub Category','id'=>'modalSubCategory'])

  @slot('footer')
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="button" onClick="insertSubCategory()" class="btn green">Submit</button>
  @endslot

  <div class="row">
    <div class="form-group">
        <label class="col-md-4 control-label" style="margin-top:5px">Nama Sub Category</label>
        <div class="col-md-8">
            <input type="text" id="textSubCategory" name="textSubCategory" class="form-control" placeholder="Input Nama Sub Category">
        </div>
    </div>
  </div>
@endcomponent

@component('components.modalBasic',['title'=>'Edit Sub Category','id'=>'editModalSubCategory'])

  @slot('footer')
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="button" onClick="updateSubCategory()" class="btn green">Submit</button>
  @endslot

  <input type="hidden" id="idEditSubCategory" name="idEditSubCategory" class="form-control" placeholder="Input Nama Category">

  <div class="row">
    <div class="form-group">
        <label class="col-md-3 control-label" style="margin-top:5px">Nama Sub Category</label>
        <div class="col-md-9">
            <input type="text" id="textEditSubCategory" name="textEditSubCategory" class="form-control" placeholder="Input Nama Sub Category">
        </div>
    </div>
  </div>
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> List Category
            <small>detail & manage category</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


              <div class="portlet light ">
                  <div class="portlet-title">
                      <a data-toggle="modal" href="#modalCategory" type="button" class="btn blue btn-outline">New Category</a>
                  </div>

                  <div class="portlet-body">
                    @component('components.npmDatatable',['idTable' => 'category'])
                      <thead>
                      <tr>
                        <th style="min-width:400px">
                          Nama
                        </th>
                        <th style="min-width:150px">
                          Expiry
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                      </thead>
                    @endcomponent
                  </div>

                </div>


                <div class="portlet light " id="subCategoryPortlet" hidden>
                    <div class="portlet-title">
                        <a data-toggle="modal" href="#modalSubCategory" type="button" class="btn blue btn-outline">New Sub Category</a>
                    </div>
                    <div class="portlet-body">
                      @component('components.npmDatatable',['idTable' => 'subCategory'])
                        <thead>
                        <tr>
                          <th>
                            Nama
                          </th>

                          <th>
                            Action
                          </th>
                        </tr>
                        </thead>
                      @endcomponent
                    </div>
                  </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>



@endsection

@push('scripts')
<script>
var table2      = null;
var table       = null;
var idCategory  = -99;
$(function() {
    $.fn.dataTable.ext.errMode = 'none';
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    table2 = null;
    table = $('#category').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("Category") }}/datatable',
        columns: [
            { data: 'nama', name: 'nama' },
            { data: 'expiryStat', name: 'expiryStat' },
            { data: 'action', name: 'action',Sortable: "false"}
        ],
        columnDefs: [
          { className: "dt-body-center", "targets": [ 1] },
          { width: "150px", targets: [1] }
        ],
        fnDrawCallback: function (oSettings) {
          $("[data-toggle='confirmation']").confirmation({
            singleton: true,
            onConfirm:function(){
              var myElement = $(this).closest('.popover').prev();
              var id        = myElement.context.getAttribute('data-id');

              $.ajax({
                  url: '{{url("Category")}}/'+id,
                  type: 'DELETE',
                  data: {_token: '{{csrf_token()}}'},
                  success: function(result) {
                    // table.ajax.reload();
                    // toastr.success("Category Berhasil dihapus", "Success");
                    if (result.status=="OK") {
                      table.ajax.reload();
                      toastr.success("Category Berhasil dihapus", "Success");
                    }else{
                      toastr.success(result.message, "Warning");
                    }
                    // console.log(result.status);
                  }
              });

            }
          });
        }
    });


    //event on click table category
    $('#category tbody').on( 'click', 'tr', function () {
        var id = $(this).attr('id');
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            idCategory = -99;
            $( "#subCategoryPortlet" ).toggle();
            table2.destroy();
            table2 = null;
        }
        else {
            //check if var table2 already initiate
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            //initiate datatable
            if(table2 ==null){
              try{
                $( "#subCategoryPortlet" ).toggle();
                initTable(id);
                idCategory = id;
              }catch(e){
                console.log("masuk");
              }

            }
            //reload datatable using new URL
            else {
              idCategory = id;
              table2.ajax.url( '{{ url("SubCategory") }}/datatable/'+ id).load();
            }
        }
    } );

    function initTable(id){
      table2 = $('#subCategory').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{{ url("SubCategory") }}/datatable/'+id,
          columns: [
              { data: 'nama', name: 'nama' },
              { data: 'action', name: 'action',Sortable: "false"}
          ],
          columnDefs: [
            { className: "dt-body-center", "targets": [ 1] },
            { width: "150px", targets: [1] }
          ],
          fnDrawCallback: function (oSettings) {
            $("[data-toggle='confirmation']").confirmation({
              singleton: true,
              onConfirm:function(){
                var myElement = $(this).closest('.popover').prev();
                var id        = myElement.context.getAttribute('data-id');

                $.ajax({
                    url: '{{url("SubCategory")}}/'+id,
                    type: 'DELETE',
                    data: {_token: '{{csrf_token()}}'},
                    success: function(result) {
                      if (result.status=="OK") {
                        table2.ajax.reload();
                        toastr.success("Sub Category Berhasil dihapus", "Success");
                      }else{
                        toastr.success(result.message, "Warning");
                      }
                      // table2.ajax.reload();
                      // toastr.success("Category Berhasil dihapus", "Success");
                    }
                });

              }
            });
          }
      });
    }

});


function insertCategory(){
  var nameCategory = $("#textCategory").val();
  var expiryCheck  = $('#expiryCheck')[0].checked;
  var expiryStat   = 0;
  if(expiryCheck)
    expiryStat  = 1;

  $.ajax({
      url: '{{url("Category")}}',
      type: 'POST',
      data: {_token: '{{csrf_token()}}',
              nama:nameCategory,
              expiry:expiryStat
            },
      success: function(result) {
        toastr.success("Category berhasil ditambah", "Success");
        $("#textCategory").val("");
        $('#expiryCheck').prop('checked', false);
        $("#modalCategory").modal('hide');
        table.ajax.reload();
      },
      statusCode: {
        422: function(result) {
          var error = result.responseJSON.errors.nama[0];
          toastr.error(error, "Failed")
        }
      }
  });
}

function editModalCategory(id,nama,check){
  $("#idEditCategory").val(id);
  if(check == 0)
    $('#expiryEditCheck').prop('checked', false);
  else {
    $('#expiryEditCheck').prop('checked', true);
  }
  $("#textEditCategory").val(nama);
}

function updateCategory(){
  var nameCategory = $("#textEditCategory").val();
  var expiryCheck  = $('#expiryEditCheck')[0].checked;
  var idCategory   = $("#idEditCategory").val();
  var expiryStat   = 0;
  if(expiryCheck)
    expiryStat  = 1;

  $.ajax({
      url: '{{url("Category")}}/'+idCategory,
      type: 'PATCH',
      data: {
              _token: '{{csrf_token()}}',
              id    :idCategory,
              nama  :nameCategory,
              expiry:expiryStat
            },
      success: function(result) {
        toastr.success("Category berhasil di update", "Success");
        $("#editModalCategory").modal('hide');
        table.ajax.reload();
      },
      statusCode: {
        422: function(result) {
          var error = result.responseJSON.errors.nama[0];
          toastr.error(error, "Failed")
        }
      }
  });
}

function insertSubCategory(){
  var nameSubCategory = $("#textSubCategory").val();

  $.ajax({
      url: '{{url("SubCategory")}}',
      type: 'POST',
      data: {_token: '{{csrf_token()}}',
              nama:nameSubCategory,
              idCat:idCategory
            },
      success: function(result) {
        toastr.success("Sub Category berhasil ditambah", "Success");
        $("#textSubCategory").val("");
        $("#modalSubCategory").modal('hide');
        table2.ajax.reload();
      },
      statusCode: {
        422: function(result) {
          var error = result.responseJSON.errors.nama[0];
          toastr.error(error, "Failed")
        }
      }
  });

}

function editModalSubCategory(id,nama){
  $("#idEditSubCategory").val(id);
  $("#textEditSubCategory").val(nama);
}

function updateSubCategory(){
  var nameCategory = $("#textEditSubCategory").val();
  var idCategory   = $("#idEditSubCategory").val();

  $.ajax({
      url: '{{url("SubCategory")}}/'+idCategory,
      type: 'PATCH',
      data: {_token: '{{csrf_token()}}',
              id:idCategory,
              nama:nameCategory
            },
      success: function(result) {
        toastr.success("Sub Category berhasil di update", "Success");
        $("#editModalSubCategory").modal('hide');
        table2.ajax.reload();
      },
      statusCode: {
        422: function(result) {
          var error = result.responseJSON.errors.nama[0];
          toastr.error(error, "Failed")
        }
      }
  });
}

</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
