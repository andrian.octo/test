
@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@section('content')

<div class="page-content-wrapper">

    <div class="page-content">
        <h2 class="page-title"> List Category
            <small>detail & manage category</small>
        </h2>

        <!-- BODY CONTENT -->
        <div class="row">
            <div class="col-md-12">
              <?php if(session('errMessage')) {?>
                  <div class="alert alert-danger">
                    {{session('errMessage')}}
                  </div>
              <?php } ?>


                <div class="portlet light ">
                  <div class="portlet-title">
                      <a href="{{url('/User/create')}}" type="button" class="btn blue btn-outline">New Category</a>
                  </div>
                  <div class="portlet-body">
                    @component('components.npmDatatable')
                      @slot('idTable')
                        users
                      @endslot
                      <thead>
                      <tr>
                        <th>
                          Username
                        </th>
                        <th>
                          Nama
                        </th>
                        <th>
                          Role
                        </th>
                        <th>
                          Action
                        </th>
                      </tr>
                      </thead>
                    @endcomponent
                  </div>
                </div>
            </div>

        </div>
        <!-- END BODY CONTENT -->

    </div>
    <!-- END CONTENT -->
</div>

@endsection

@push('scripts')
<script>

$(function() {
    @if(session('successMsg'))
      toastr.success("{{session('successMsg')}}", "Success")
    @endif

    var table = $('#category').DataTable();
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
