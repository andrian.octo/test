@extends('layouts.app')

@component('components.toastrNotif')
@endcomponent

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Item") }}">Manage Items</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>New Item</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Detail Stok</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    <form class="form-horizontal">                      

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->subcategory->category->nama}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Sub Kategori</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->subcategory->nama}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->nama}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Stok</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$itemStock->jumlah}}" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Harga Beli Acuan</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->harga_beli}}" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Harga Jual Acuan</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->harga_jual_default}}" readonly="">
                        </div>
                      </div>
                       </form>
                       <br />
                       <table class="table table-stripped table-bordered">
                         <tr>
                           <th>Tanggal</th>
                           <th>Harga Beli</th>
                           <th>Jumlah Masuk</th>
                           <th>Keterangan</th>
                           @if (Auth::user()->role === 2)
                           <th>#</th>
                           @endif
                         </tr>
                         @foreach($itemHistories as $history)
                          <tr>
                            <td>{{$history->created_at}}</td>
                            <td>{{$history->harga_beli}}</td>
                            <td>{{$history->jumlah}}</td>
                            <td>{{$history->keterangan}}</td>
                            @if (Auth::user()->role === 2)
                            <td>
                              <?php if ($history->keterangan=="habis"){ ?>
                                
                              <?php }else{ 
                                $produk = $item->subcategory->category->nama." ".$item->subcategory->nama." ".$item->nama;
                                if ($history->keterangan=="Stok Datang") {
                                  $sisa = $history->jumlah;
                                }else{
                                  $arrSisa = explode(" ", $history->keterangan);
                                  $sisa = $arrSisa[1];
                                }
                                ?>
                                <a onClick="editModalStock({{$history->id}},{{$history->jumlah}},{{$history->harga_beli}},'{{$produk}}','{{$sisa}}')" data-toggle="modal" href="#editModalStock" type="button" class="btn yellow-crusta btn-outline">Edit Stok</a>
                              <?php } ?>
                            </td>
                          </tr>
                          @endif
                         @endforeach
                       </table>

                    </div>
                    
                   
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection

@component('components.modalBasic',['title'=>'Edit Stok','id'=>'editModalStock'])

  @slot('footer')
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="button" onClick="updateSubStokDatang()" class="btn green">Submit</button>
  @endslot

  <input type="hidden" id="idEditStock" name="idEditStock" class="form-control">

  <div class="row">
    <div class="form-group">
        <label class="col-md-3 control-label" >Produk</label>
        <div class="col-md-9">
            <input type="text" id="textEditProduk" name="textEditProduk" class="form-control" readonly="">
        </div>
    </div>
    <br/>
    <div class="form-group">
        <label class="col-md-3 control-label" >Harga Beli</label>
        <div class="col-md-9">
            <input type="text" id="textEditHargaBeli" name="textEditHargaBeli" class="form-control" readonly="">
        </div>
    </div>
    <br/>
    <div class="form-group">
        <label class="col-md-3 control-label" >Stok Datang</label>
        <div class="col-md-9">
            <input type="text" id="textEditStock" name="textEditStock" class="form-control" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label" >Sisa Stok Datang</label>
        <div class="col-md-9">
            <input type="text" id="textEditSisa" name="textEditSisa" class="form-control" readonly="">
        </div>
    </div>
    <br/>
    <div class="form-group">
        <label class="col-md-3 control-label" >Penyesuaian Stok</label>
        <div class="col-md-9">
            <input type="number" id="textEditPenyesuaian" name="textEditPenyesuaian" class="form-control">
            <p class="help-block">Sesuaikan Jumlah dengan Data Asli</p>
        </div>
    </div>
  </div>
@endcomponent

@push('scripts')
<script>

function editModalStock(id,stock,harga_beli,produk,sisa){
  $("#idEditStock").val(id);
  $("#textEditStock").val(stock);
  $("#textEditProduk").val(produk);
  $("#textEditHargaBeli").val(harga_beli);
  $("#textEditSisa").val(sisa);
  $("#textEditPenyesuaian").val(sisa);

  $("#textEditPenyesuaian").attr({
       "max" : sisa,
    });
}

function updateSubStokDatang(){
  var jumlahStokUpdate = $("#textEditPenyesuaian").val();
  var idStokHistory    = $("#idEditStock").val();
  $.ajax({
      url: '{{url("Stock")}}/'+idStokHistory,
      type: 'PATCH',
      data: {_token: '{{csrf_token()}}',
              id:idStokHistory,
              jumlahStokUpdate:jumlahStokUpdate
            },
      success: function(result) {
        toastr.success("Stok Datang Berhasil di Update", "Success");
        $("#editModalStock").modal('hide');
        setTimeout(function() {location.reload();}, 3000);
      },
      statusCode: {
        422: function(result) {
          var error = result.responseJSON.errors.nama[0];
          toastr.error(error, "Failed")
        }
      }
  });
}

$('#category').on('change', function(e){
  var cat_id = e.target.value;
  $.get('/Category/SubCategory/'+cat_id, function(data){
    $('#subcategory').empty();
    $('#subcategory').append('<option>-Pilih Sub Kategori Produk-</option>');    
    $.each(JSON.parse(data), function(index, subcatObj){
          $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.nama+'</option>');
    });
  });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
