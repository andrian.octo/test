
@extends('layouts.app')

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Stock") }}">Manage Stock</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>Update Item Stock</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Update Manual Stock Produk - {{$item->nama}}</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::model($item, ['method' => 'PATCH','route' => ['Stock.update', $item->id],'class'=>"form-horizontal"]) !!}

                    <?php

                       $item = array(
                                array("text", "category", "Kategori" ,"col-sm-3","col-sm-4",$item->subcategory->category->nama),
                                array("text", "subcategory", "Sub Kategori" ,"col-sm-3","col-sm-4",$item->subcategory->nama),
                                array("text", "nama", "Nama" ,"col-sm-3","col-sm-4",$item->nama),
                                array("text", "harga_jual_default","Harga Jual", "col-sm-3","col-sm-4",$item->harga_jual_default),
                                array("text", "current_stock", "Jumlah Saat ini" ,"col-sm-3","col-sm-2",$itemStock->jumlah)
                              );

                    ?>

                    {!!NPMForm::FormGenerate($errors,$item)!!}

                    <div class="form-group" >
                      <label class="col-sm-3 control-label">Penyesuaian</label>
                       <label for="inputtext3" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8 radio">
                            <label><input onclick="selectTambah()" class="satuan_item" type="radio" name="operasi" value="plus">Tambah</label>
                            <label><input onclick="selectKurang()" class="satuan_item" type="radio" name="operasi" value="minus">Kurang</label>
                        </div>
                     </div>

                     <div class="form-group" id="tambahStock" hidden>
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-2">
                          <input class="form-control" type="text" name="tambah" placeholder="Tambah Stock">
                        </div>
                      </div>

                      <div class="form-group" id="kurangiStock" hidden>
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-2">
                          <input class="form-control" type="text" name="kurang" placeholder="Kurangi Stock">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-4">
                          <textarea class="form-control" name="keterangan"></textarea>
                        </div>
                      </div>
                   
                     
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Update Stock</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>

                    {{ Form::close() }}
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection

@push('scripts')
<script>

$('#category').attr('disabled', true);
$('#subcategory').attr('disabled', true);
$('#nama').attr('disabled', true);
$('#harga_jual_default').attr('disabled', true);
$('#current_stock').attr('disabled', true);

function selectTambah(){
  $("#tambahStock").show();
  $("#kurangiStock").hide();
}

function selectKurang(){
  $("#kurangiStock").show();
  $("#tambahStock").hide();
}


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
