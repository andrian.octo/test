
@extends('layouts.app')

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Item") }}">Manage Items</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>New Item</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Detail Stok</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    <form class="form-horizontal">                      

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->subcategory->category->nama}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Sub Kategori</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->subcategory->nama}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->nama}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Stok</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$itemStock->jumlah}}" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Harga Beli Acuan</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->harga_beli}}" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Harga Jual Acuan</label>
                        <div class="col-sm-4">
                          <input class="form-control" value="{{$item->harga_jual_default}}" readonly="">
                        </div>
                      </div>
                       </form>
                       <br />
                       <table class="table table-stripped table-bordered">
                         <tr>
                           <th>Tanggal</th>
                           <th>Harga Beli</th>
                           <th>Jumlah Masuk</th>
                           <th>Keterangan</th>
                         </tr>
                         @foreach($itemHistories as $history)
                          <tr>
                            <td>{{$history->created_at}}</td>
                            <td>{{$history->harga_beli}}</td>
                            <td>{{$history->jumlah}}</td>
                            <td>{{$history->keterangan}}</td>
                          </tr>
                         @endforeach
                       </table>

                    </div>
                    
                   
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection

@push('scripts')
<script>

$('#category').on('change', function(e){
  var cat_id = e.target.value;
  $.get('/Category/SubCategory/'+cat_id, function(data){
    $('#subcategory').empty();
    $('#subcategory').append('<option>-Pilih Sub Kategori Produk-</option>');    
    $.each(JSON.parse(data), function(index, subcatObj){
          $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.nama+'</option>');
    });
  });
});


</script>

<script src="{{asset("assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-confirmations.min.js")}}" type="text/javascript"></script>

@endpush
