
@extends('layouts.app')

@component('components.select2Plugin')
@endcomponent

@component('components.datePicker')
@endcomponent

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("Order") }}">Stok</a>
                      <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                      <span>Update</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption">
                      <i class="icon-equalizer font-red-sunglo"></i>
                      <span class="caption-subject font-red-sunglo bold uppercase">Stok Datang</span>

                  </div>
                  <div class="tools">
                      <a href="" class="collapse"> </a>
                      <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                      <a href="" class="reload"> </a>
                      <a href="" class="remove"> </a>
                  </div>
              </div>
              <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                    <form id="order">
                          <div class="row table-scrollable">
                            <table class="table table-hover" id="itemSales">
                                <thead>
                                    <tr>
                                        <th> Category </th>
                                        <th> Sub Category </th>
                                        <th> Produk </th>
                                        <th> In Stock </th>
                                        <th> Pcs </th>
                                        <th> Unit Price </th>
                                        <th hidden> Discount </th>
                                        <th> Expiry </th>
                                        <th> Amount </th>
                                        <th colspan="3"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-md-8">
                                &nbsp;
                            </div>
                            <div class="col-md-4">
                                <h3 class="form-section">Total</h3>
                                <h4 id="totalAmount">Rp 0</h4>
                            </div>
                          </div>

                          <!--/row-->
                      </div>
                    </form><br /><br />

                      <div class="form-actions pull-right">
                          <button type="button" class="btn default">Cancel</button>
                          <button type="submit" onClick="submit()" class="btn blue">
                              <i class="fa fa-check"></i> Update Stok</button>
                      </div>

                      <br /><br /><br />
                  <!-- END FORM-->
              </div>
          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@push('scripts')
  <script>

    var countItem = 0;

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });



    function submit(){
      var dataPass  = $('#order').serializeArray();

      var arrCat    = $("td[id*='cat']");
      var arrSubCat = $("td[id*='subCat']");
      var arrItem   = $("td[id*='desc']");
      var arrCount  = $("td[id*='count']");
      var arrPrice  = $("td[id*='price']");
      var arrExpiry = $("td[id*='expiry']");
      var arrDisc   = $("td[id*='disc']");
      var arrAmount = $("td[id*='amount']");
      var itemCounts = 0;
      var totalAmount = $("#totalAmount").text();


      for(var i = 0; i<arrCat.length;i++){
        dataPass.push({name: arrCat[i].attributes.name.value+''+i, value: arrCat[i].textContent});
        itemCounts++;
      }
      for(var i = 0; i<arrSubCat.length;i++){
        dataPass.push({name: arrSubCat[i].attributes.name.value+''+i, value: arrSubCat[i].textContent});
      }
      for(var i = 0; i<arrItem.length;i++){
        dataPass.push({name: arrItem[i].attributes.name.value+''+i, value: arrItem[i].textContent});
      }
      for(var i = 0; i<arrCount.length;i++){
        dataPass.push({name: arrCount[i].attributes.name.value+''+i, value: numeral(arrCount[i].textContent).value()});
      }
      for(var i = 0; i<arrPrice.length;i++){
        dataPass.push({name: arrPrice[i].attributes.name.value+''+i, value: numeral(arrPrice[i].textContent).value()});
      }
      for(var i = 0; i<arrExpiry.length;i++){
        dataPass.push({name: arrExpiry[i].attributes.name.value+''+i, value: arrExpiry[i].textContent});
      }
      for(var i = 0; i<arrDisc.length;i++){
        dataPass.push({name: arrDisc[i].attributes.name.value+''+i, value: arrDisc[i].textContent});
      }
      for(var i = 0; i<arrAmount.length;i++){
        dataPass.push({name: arrAmount[i].attributes.name.value+''+i, value: numeral(arrAmount[i].textContent).value()});
      }
      dataPass.push({name: 'itemCounts', value: itemCounts});

      totalAmount = totalAmount.replace("Rp","");
      totalAmount = numeral(totalAmount.trim()).value();

      dataPass.push({name: 'totalAmount', value: totalAmount});

      $.ajax({
          url: '{{url("Stock")}}',
          type: 'POST',
          data: dataPass,
          success: function(result) {
            window.location.href = '{{url("Stock")}}';

          }
      });
    }


  </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="{{asset("js/itemStockTable.js")}}" type="text/javascript"></script>
@endpush

@endsection
