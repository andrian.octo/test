<div class="modal fade" id="{{$id}}" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{{$title}}</h4>
            </div>
            <div class="modal-body"> {{$slot}} </div>
            <div class="modal-footer">
                {{$footer}}
                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
