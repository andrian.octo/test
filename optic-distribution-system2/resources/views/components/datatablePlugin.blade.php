
@push('styles')
<link href="{{asset("assets/global/plugins/datatables/datatables.min.css")}}" rel="stylesheet" type="text/css" />
<link href="{{asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css")}}" rel="stylesheet" type="text/css" /> -->
@endpush

@push('scripts')
<script src="{{asset("assets/global/scripts/datatable.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/datatables/datatables.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/table-datatables-managed.min.js")}}" type="text/javascript"></script>
@endpush
