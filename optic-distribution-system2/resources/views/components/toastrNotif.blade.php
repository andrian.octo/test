@push('styles')
<link href="{{asset("assets/global/plugins/bootstrap-toastr/toastr.min.css")}}" rel="stylesheet" type="text/css" />
@endpush

@push('scripts')
<script src="{{asset("assets/global/plugins/bootstrap-toastr/toastr.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/ui-toastr.min.js")}}" type="text/javascript"></script>
@endpush
