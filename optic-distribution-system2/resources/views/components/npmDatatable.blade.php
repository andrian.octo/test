<!-- Requirement CSS
<link href="{{asset("/global/plugins/datatables/datatables.min.css")}}" rel="stylesheet" type="text/css" />
<link href="{{asset("/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css")}}" rel="stylesheet" type="text/css" /> -->


<table class="table table-striped table-bordered table-hover table-checkable order-column" id="{{$idTable}}">
    {{$slot}}
</table>



<!--
Requirement JS
<script src="{{asset("/global/scripts/datatable.js")}}" type="text/javascript"></script>
<script src="{{asset("/global/plugins/datatables/datatables.min.js")}}" type="text/javascript"></script>
<script src="{{asset("/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js")}}" type="text/javascript"></script>
<script src="{{asset("/pages/scripts/table-datatables-managed.min.js")}}" type="text/javascript"></script> -->
