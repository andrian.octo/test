<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="{{ $icon }}"></i>
        <span class="title">Manage {{ $name }}</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href="{{url($name)}}/create" class="nav-link ">
                <i class=" fa fa-plus-square-o"></i>
                <span class="title">Tambah {{ $name }}</span>
            </a>
        </li>

        <li class="nav-item  ">
            <a href="{{url($name.'')}}" class="nav-link ">
              <i class=" fa fa-pencil-square-o"></i>
                <span class="title">Manage {{ $name }}</span>
            </a>
        </li>

        {{$slot}}

    </ul>

</li>
