
@extends('layouts.app')

@section('content')

<div class="page-content-wrapper">
  <div class="page-content-wrapper">

      <div class="page-content">
          <div class="page-bar">
              <ul class="page-breadcrumb">
                  <li>
                      <i class="icon-home"></i>
                      <a href="{{ url("User") }}">Manage User</a>
                      <i class="fa fa-angle-right"></i>
                  </li>

                  <li>
                      <span>Edit User</span>
                  </li>
              </ul>
          </div>
          <!-- BODY CONTENT -->
          <div class="row">
              <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus"></i>
                        <span class="caption-subject bold uppercase"> Create New User</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                  <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::model($user, ['method' => 'PATCH','route' => ['User.update', $user->id],'class'=>"form-horizontal"]) !!}

                    <?php
 //                      [$type,$id,$Tag,sizeLabel,sizeForm]
                       $item = array(
                                array("text", "name", "Nama" ,"col-sm-3","col-sm-8",$user->name),
                                array("text", "username","Username", "col-sm-3","col-sm-8",$user->username),
                                array("password", "password","Password", "col-sm-3","col-sm-8",$user->password),
                              );
                    ?>

                    {!!NPMForm::FormGenerate($errors,$item)!!}

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Role</label>
                        <div class="col-sm-8">
                          <div class="btn-group" data-toggle="buttons">
                                <label class="btn dark btn-outline {{ ($user->role == 0) ? 'active' : '' }}" style="margin-right:5px">
                                    <input name="role" value="0" type="radio" class="toggle"> Owner </label>
                                <!-- <label class="btn dark btn-outline {{ ($user->role == 1) ? 'active' : '' }}" style="margin-right:5px">
                                    <input name="role" value="1" type="radio" class="toggle" checked> Manager </label> -->
                                <label class="btn dark btn-outline {{ ($user->role == 1) ? 'active' : '' }}" style="margin-right:5px">
                                    <input name="role" value="2" type="radio" class="toggle"> Customer Service </label>
                                <label class="btn dark btn-outline {{ ($user->role == 2) ? 'active' : '' }}" style="margin-right:5px">
                                    <input name="role" value="2" type="radio" class="toggle"> Kepala Gudang </label>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label">Cabang</label>
                        <div class="col-sm-4">
                          <select id="cabang" name="cabang" class="form-control">
                              <option value="1" {{ ($user->cabang_id == 1) ? 'selected' : '' }}>Semarang</option>
                              <option value="2" {{ ($user->cabang_id == 2) ? 'selected' : '' }}>Kudus</option>
                          </select>
                        </div>
                      </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>

                    {{ Form::close() }}
                  </div>
                  </div>
              </div>

          </div>
          <!-- END BODY CONTENT -->

      </div>
      <!-- END CONTENT -->
  </div>
</div>

@endsection
