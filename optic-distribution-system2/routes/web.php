<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();
//Route User
Route::get('/User/datatable', 'UserController@datatable')->middleware('auth');

//Route Category
Route::get('/Category/datatable', 'CategoryController@datatable')->middleware('auth');
Route::patch('/Category/json', 'CategoryController@getItemJson')->middleware('auth');
Route::get('/Category/jsonSelect', 'CategoryController@getItemJsonSelect2')->middleware('auth');

//Route SubCategory
Route::get('/SubCategory/datatable/{id}', 'SubCategoryController@datatable')->middleware('auth');
Route::patch('/SubCategory/json', 'SubCategoryController@getItemJson')->middleware('auth');
Route::get('/SubCategory/jsonSelect', 'SubCategoryController@getItemJsonSelect2')->middleware('auth');

//Route Optic
Route::get('/Optic/datatable', 'OpticController@datatable')->middleware('auth');
Route::patch('/Optic/json', 'OpticController@getOpticJson')->middleware('auth');

//Route Item
Route::get('/Item/datatable', 'ItemController@datatable')->middleware('auth');
Route::patch('/Item/json', 'ItemController@getItemJson')->middleware('auth');
Route::patch('/Item/jsonitemWithStock', 'ItemController@getItemWithStockJson')->middleware('auth');
Route::get('/Item/jsonSelect', 'ItemController@getItemJsonSelect2')->middleware('auth');

//Route Order
Route::get('/Order/datatable', 'SalesController@datatable')->middleware('auth');
Route::get('/Order/view/{id}', 'SalesController@view')->middleware('auth');
Route::get('/Order/edit/{id}', 'SalesController@edit')->middleware('auth');
Route::get('/Order/retur/{id}', 'SalesController@retur')->middleware('auth');
Route::post('/Order/retur/{id}', 'SalesController@prosesRetur')->middleware('auth');

Route::post('/Order/editSales/{id}', 'SalesController@updateSales')->middleware('auth');


Route::get('/Category/SubCategory/{id}', 'CategoryController@ajax_subcategory')->middleware('auth');
Route::get('/ItemStock/datatable', 'ItemStockController@datatable')->middleware('auth');
Route::get('/Print/{id}', 'PrintController@index')->middleware('auth');
Route::get('/Print/{id}/{id_payment}', 'PrintController@payment')->middleware('auth');
Route::get('/Print/ReturDistributor/{id}', 'PrintController@ReturDistributor')->middleware('auth');


Route::resource('User', 'UserController')->middleware('auth');
Route::resource('Category', 'CategoryController')->middleware('auth');
Route::resource('SubCategory', 'SubCategoryController')->middleware('auth');
Route::resource('Optic', 'OpticController')->middleware('auth');
Route::resource('Item', 'ItemController')->middleware('auth');
//Route::resource('Payment', 'PaymentController')->middleware('auth');
Route::get('Payment/{id}', ["as" => "Payment.index", "uses" => "PaymentController@index"])->middleware('auth');
Route::get('Payment/{id}/datatable', 'PaymentController@datatable')->middleware('auth');
Route::get('Payment/{id}/create', 'PaymentController@create')->middleware('auth');
Route::post('Payment', 'PaymentController@store')->middleware('auth');

Route::resource('Stock', 'ItemStockController')->middleware('auth');
Route::resource('Order', 'SalesController')->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


Route::get('/Item/test', 'ItemController@test')->middleware('auth');

Route::get('/Stock/updateItemStock/{id}', 'ItemStockController@newStock')->middleware('auth');
Route::get('/Stock/detail/{id}', 'ItemStockController@detailStock')->middleware('auth');
Route::get('/receiveItemStock', 'ItemStockController@receiveStock')->middleware('auth');
Route::get('/Stock/create', 'ItemStockController@receiveStock')->middleware('auth');


Route::get('/Stock/newItemStock/{id}', 'ItemStockController@newStock')->middleware('auth');
Route::get('/receiveItemStock', 'ItemStockController@receiveStock')->middleware('auth');

// report controller
Route::get('/Report/inventoryDatatable', 'ReportController@inventoryDatatable')->middleware('auth');
Route::get('/Report/inventoryLowDatatable', 'ReportController@inventoryLowDatatable')->middleware('auth');
Route::get('/Report/expiryDatatable', 'ReportController@expiryDatatable')->middleware('auth');
Route::get('/Report/salesDatatable', 'ReportController@salesDatatable');
Route::post('/Report/salesDatatable', 'ReportController@salesDatatable');
Route::get('/Report/stokChartJson', 'ReportController@inventoryChartJSON');
Route::get('/Report/salesChartByItemJSON', 'ReportController@salesChartByItemJSON');
Route::get('/Report/salesChartByRevenueJSON', 'ReportController@salesChartByRevenueJSON');
Route::get('/Report/summaryJson', 'ReportController@summaryJson');


Route::get('/Report/inventory', 'ReportController@inventory')->middleware('auth');
Route::get('/Report/inventoryLow', 'ReportController@inventoryLow')->middleware('auth');
Route::get('/Report/sales', 'ReportController@sales')->middleware('auth');
Route::get('/Report/expiry', 'ReportController@expiry')->middleware('auth');

Route::get('/Retur', 'ReturController@index')->middleware('auth');
Route::get('/Retur/Client', 'ReturController@indexClient')->middleware('auth');
Route::get('/Retur/Client/datatable', 'ReturController@datatableClient')->middleware('auth');
Route::get('/Retur/Client/{id}', 'ReturController@viewClient')->middleware('auth');
Route::delete('/Retur/Client/delete/{id}', 'ReturController@destroyClient')->middleware('auth');

Route::get('/Retur/datatable', 'ReturController@datatable')->middleware('auth');
Route::get('/ReturDistributor/create', 'ReturController@createDistributor')->middleware('auth');
Route::post('/ReturDistributor/create', 'ReturController@storeDistributor')->middleware('auth');
Route::get('/ReturDistributor/view/{id}', 'ReturController@viewDistributor')->middleware('auth');
Route::get('/ReturDistributor/edit/{id}', 'ReturController@editDistributor')->middleware('auth');
Route::post('/ReturDistributor/edit/{id}', 'ReturController@updateDistributor')->middleware('auth');
Route::delete('/ReturDistributor/delete/{id}', 'ReturController@destroyDistributor')->middleware('auth');

Route::get('/Report/receivings', 'ReportController@receivings')->middleware('auth');



Route::get('/OpticList/datatable', 'BillingController@datatable')->middleware('auth');
Route::resource('Billing', 'BillingController')->middleware('auth');
Route::patch('/billing/json', 'BillingController@getOpticJson')->middleware('auth');