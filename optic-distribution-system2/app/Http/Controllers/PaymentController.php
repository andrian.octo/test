<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Sale;
use App\Optic;
use Auth;
use App\Payment;
use Redirect;
use Validator;
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function datatable($id){
         $model = Payment::query();

         return DataTables::eloquent($model)
                 ->addColumn('action', function(Payment $payment) {
                     $retVal = '
                               <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$payment->id.'" data-original-title="Hapus Data User?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                     return $retVal;
                 })

                 ->rawColumns(['action'])
                 ->toJson();
     }

    public function index(Request $request,$id)
    {

      $temp = "".$id;
      $retVal = "";
      for($i=0;$i<(12 - strlen($temp));$i++){
        $retVal = $retVal."0";
      }
      $retVal = $retVal."". $temp;

      $sale = Sale::find($id)->first();
      $maxPayment = Payment::where('sale_id',$id)->max('id');

      return view('payment.index')->with('id_string',$retVal)
                                    ->with('id',$id)
                                    ->with('sale',$sale)
                                    ->with('maxPayment',$maxPayment)
                                    ->with('successMsg',$request->input('successMsg'))
                                    ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
      $payment = new Payment;
      $sale = Sale::find($id)->first();
      return view('payment.create', ['model'=>$payment,'sale_id'=>$id,'sale'=>$sale]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'jumlah'      => 'required|max:15'
      ]);

      $sale = Sale::find($request->sale_id);
      if ($validator->fails() || ($request->jenis == 1 && $request->jumlah != $sale->total))
      {
          $validator->errors()->add('jumlah', 'Jumlah tidak sesuai dengan tagihan');
          return Redirect::back()->withErrors($validator);
      }


      $payment = new Payment;

      $payment->metode   = $request->metode;
      $payment->jumlah   = $request->jumlah;
      $payment->sale_id  = $request->sale_id;
      $payment->user_id  = Auth::id();

      $payment->save();

      $optic = Optic::find($sale->optic->id);
      $optic->sisa_plafon += $request->jumlah;
      $optic->save();

      $maxPayment = Payment::where('sale_id',$request->sale_id)->max('id');
      $request->session()->put('successMsg', 'Payment created successfully');
      return redirect()->route('Payment.index',['id' => $request->sale_id])
                        ->with('successMsg','Payment created successfully');
      //return redirect()->action('PaymentController@index', ['id' => $request->sale_id,'maxPayment'=>$maxPayment]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
