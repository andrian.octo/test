<?php

namespace App\Http\Controllers;


use DataTables;
use App\User;
use App\Item;
use App\ItemsStock;
use App\ItemsHistory;
use App\SalesItem;

use App\SubCategory;
use App\Category;
use Auth;
use DB;

use Illuminate\Http\Request;
use Route;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item.index');
    }

    public function getItemJson(Request $request){
      if(isset($request->id))
        $item = Item::find($request->id);
      else
        $item = Item::All();

      return response()->json($item->toArray());
    }

    public function getItemWithStockJson(Request $request){
      if(isset($request->id))
        $item = Item::with('itemsStock','subCategory','subCategory.category')->where('id',$request->id)
        ->whereHas('itemsStock', function ($query) {
            $query->where('cabang_id', '=', Auth::user()->cabang_id);
        })
        ->firstOrFail();
      else
        $item = Item::All();

      return response()->json($item->toArray());
    }

    public function getItemJsonSelect2(Request $request){
      if(isset($request->q)){
        $item = Item::where('nama','like','%'.$request->q.'%')
                ->where('sub_category_id','=',$request->idCat)
                ->get();
      }

      else
        $item = Item::where('sub_category_id','=',$request->idCat)->get();

      return response()->json($item->toArray());
    }

    public function datatable()
    {
        $model = Item::with('subcategory.category');

        return DataTables::eloquent($model)
                    ->addColumn('category',function(Item $item){
                        return $item->subcategory->category->nama;
                    })
                    ->addColumn('subcategory',function(Item $item){
                        return $item->subcategory->nama;
                    })
                    ->addColumn('action', function(Item $item) {
                        $retVal = ' <a href="'.url("/Item").'/'.$item->id.'/edit" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                                  <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$item->id.'" data-original-title="Hapus Data Item?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                        return $retVal;
                    })
                    ->rawColumns(['action'])
                    ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new Item;
        $category = Category::all();
        return view('item.create',['model'=>$item, 'category'=>$category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'subcategory' => 'required',
            'nama'  => 'required',
            'harga_jual_default'  => 'required|integer',
            'harga_beli'  => 'required|integer',
        ]);

        // item data
        $item = new Item;
        $item->sub_category_id      = $request->subcategory;
        $item->nama                 = $request->nama;
        $item->harga_beli           = $request->harga_beli;
        $item->harga_jual_default   = $request->harga_jual_default;
        $lastInsertedId             = $item->id;
        $item->save();

        // $itemId = $item->id;

        // // items history data
        // $itemHistory = new ItemsHistory;
        // $itemHistory->item_id       = $itemId;
        // $itemHistory->cabang_id     = Auth::user()->cabang_id;
        // $itemHistory->jumlah        = $request->jumlah;
        // $itemHistory->keterangan    = "Stock Awal";
        // $itemHistory->save();

        // //item stock data
        // $itemStock = new ItemsStock;
        // $itemStock->item_id     = $itemId;
        // $itemStock->cabang_id   = Auth::user()->cabang_id;
        // $itemStock->jumlah      = $request->jumlah;
        // $itemStock->save();

        return redirect()->route('Item.index')
                        ->with('successMsg','Product created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $category = Category::all();

        $category_id = SubCategory::find($item->sub_category_id)->category_id;

        return view('item.edit',['item'=>$item, 'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama'  => 'required',
        ]);

        $item = Item::find($id);
        $item->sub_category_id  = $request->subcategory;
        $item->nama             = $request->nama;
        $item->harga_beli         = $request->harga_beli;
        $item->harga_jual_default = $request->harga_jual_default;

        $item->save();

        return redirect()->route('Item.index')
                        ->with('successMsg','Product created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $countItem = ItemsStock::where('item_id',$id)->count();
      $countSalesItem = SalesItem::where('item_id',$id)->count();

      if ($countItem>0 || $countSalesItem>0) {
          $status = "Failed";
          $message = "Item Tidak Dapat Dihapus";
      }else{
        Item::find($id)->delete();
        $status = "OK";
        $message = "Item Berhasil Dihapus";
      }

      return response()->json([
          'status' => $status,
          'message' => $message
      ]);

    }
}
