<?php

namespace App\Http\Controllers;
use DataTables;
use App\Category;
use App\SubCategory;
use App\Item;
use Route;

use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function datatable($id){
      $model = Category::find($id)->subCategory;
      return DataTables::collection($model)
                ->addColumn('action', function(SubCategory $subCategory) {
                    $retVal = ' <a onClick="editModalSubCategory('.$subCategory->id.',\''.$subCategory->nama.'\')" data-toggle="modal" href="#editModalSubCategory" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                            <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$subCategory->id.'" data-original-title="Hapus Data Sub Category?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';

                    return $retVal;
                })
                ->rawColumns(['action','roleString'])
                ->toJson();
    }

    public function getItemJson(Request $request){
      if(isset($request->id))
        $item = SubCategory::find($request->id);
      else
        $item = SubCategory::All();

      return response()->json($item->toArray());
    }

    public function getItemJsonSelect2(Request $request){
      if(isset($request->q)){
        $item = SubCategory::where('nama','like','%'.$request->q.'%')
                ->where('category_id','=',$request->idCat)
                ->get();
      }

      else{
        $item = SubCategory::where('category_id','=',$request->idCat)
                ->get();
      }

      return response()->json($item->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
        'nama'      => 'required|max:50'
      ]);

      $catCount = SubCategory::where('nama', $request->nama)
                        ->where('category_id',$request->idCat)
                        ->count();
      if($catCount > 0)
        return response()->json([
            'errors' => array('nama' => array('Nama Sub Category has already been taken')),
            'message' => 'Nama Sub Category has already been taken'
        ],422
      );


      $subCategory        = new SubCategory;

      $subCategory->nama        = $request->nama;
      $subCategory->category_id = $request->idCat;

      $subCategory->save();

      return response()->json([
          'status' => 'OK',
          'message' => 'Success add sub category'
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validatedData = $request->validate([
        'nama'      => 'required|max:50'
      ]);

      $oldSubCategory = SubCategory::find($id);
      $catCount = SubCategory::where('nama', $request->nama)
                        ->where('category_id',$oldSubCategory->category_id)
                        ->count();

      if($catCount > 0)
        return response()->json([
            'errors' => array('nama' => array('Nama Sub Category has already been taken')),
            'message' => 'Nama Sub Category has already been taken'
        ],422
      );

      $oldSubCategory->nama        = $request->nama;

      $oldSubCategory->save();

      return response()->json([
          'status' => 'OK',
          'message' => 'Success add sub category'
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $countItem = Item::where('sub_category_id',$id)->count();
      if ($countItem>0) {
          $status = "Failed";
          $message = "Sub Kategori Tidak Dapat Dihapus";
      }else{
        SubCategory::find($id)->delete();
        $status = "OK";
        $message = "Sub Kategori Berhasil Dihapus";
      }

      return response()->json([
          'status' => $status,
          'message' => $message
      ]);
    }


}
