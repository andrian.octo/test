<?php

namespace App\Http\Controllers;

use DataTables;
use App\User;
use App\Optic;
use App\Sale;
use App\SalesItem;
use App\ItemsStock;
use App\ItemsHistory;
use App\Retur;
use App\ReturItem;

use DateTime;
use DB;
use Auth;

use Illuminate\Http\Request;
use Route;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('invoice.index');
    }

    public function datatable(){
         $model = Sale::with('optic')->groupBy('optic_id')->get();

         return DataTables::of($model)
                  ->addColumn('action', function(Sale $sale) {
                             $retVal = '
                                       <a href="'.url("/CreateInvoice").'/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">Invoice</a>';
                             return $retVal;
                         })
                 ->addColumn('status_pembayaran', function(Sale $sale) {
                           if($sale->status_pembayaran == "Pending"){
                             return $sale->status_pembayaran;
                           }
                           else{
                             return $sale->status_pembayaran;
                           }
                         })
                 ->addColumn('optic_name', function(Sale $sale) {
                    $retVal = $sale->optic->nama;
                    return $retVal;
                 })
                 ->addColumn('status_pembayaran', function(Sale $sale) {
                           if($sale->status_pembayaran == "Pending"){
                             return $sale->status_pembayaran;
                           }
                           else{
                             return $sale->status_pembayaran;
                           }
                         })
                 ->toJson();
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
