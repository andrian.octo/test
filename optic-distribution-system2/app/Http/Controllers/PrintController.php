<?php

namespace App\Http\Controllers;


use DataTables;
use App\User;
use App\Optic;
use App\Sale;
use App\Item;
use App\Payment;
use App\SalesItems;

use Auth;

use Illuminate\Http\Request;
use Route;
use PDF;

class PrintController extends Controller
{
  public function index(Request $request,$id)
  {
    $sale = Sale::with('optic')
            ->where('id',$id)
            ->first();
    // $salesItem = Sale::find(1)->get();
    // foreach ($salesItem as $key => $value) {
    //   foreach ($value->items as $key => $value) {
    //     var_dump($value->pivot->jumlah);
    //   }
    // }

    $itemList = $sale->items;
    $subCategoryList = [];

    foreach ($itemList as $key => $value) {
      // $tempItem = Item::with('subCategory')->where('id',$value->id)->get();
      //
      // foreach ($tempItem as $key => $value) {
      //   $subCategoryList[] = $value->subCategory;
      // }

      // var_dump($value->subCategory->category->nama);
    }


    $data = [
      'sale' => $sale,
      'items' => $itemList
    ];

    $pdf = PDF::loadView('printTheme.invoice',$data);
    return $pdf->setPaper('a4', 'portrait')->stream('invoice.pdf');
    // return $pdf->setPaper([0, 0, 609.4488, 396.85], 'portrait')->stream('invoice.pdf');
  }

  public function payment($id,$id_payment)
  {
    $sale     = Sale::with('optic')
              ->where('id',$id)
              ->first();

    $payment = Payment::find($id_payment)->first();


    $data = [
      'sale' => $sale,
      'payment' => $payment
    ];

    $pdf = PDF::loadView('printTheme.payment',$data);
    return $pdf->setPaper([0, 0, 569.764, 428.0315], 'portrait')->stream('invoice.pdf');
  }

  public function returDistributor(Request $request,$id)
  {
    $retur = Sale::with('optic')
            ->where('id',$id)
            ->first();
    // $salesItem = Sale::find(1)->get();
    // foreach ($salesItem as $key => $value) {
    //   foreach ($value->items as $key => $value) {
    //     var_dump($value->pivot->jumlah);
    //   }
    // }

    $itemList = $sale->items;
    $subCategoryList = [];

    foreach ($itemList as $key => $value) {
      // $tempItem = Item::with('subCategory')->where('id',$value->id)->get();
      //
      // foreach ($tempItem as $key => $value) {
      //   $subCategoryList[] = $value->subCategory;
      // }

      // var_dump($value->subCategory->category->nama);
    }


    $data = [
      'sale' => $sale,
      'items' => $itemList
    ];

    $pdf = PDF::loadView('printTheme.invoice',$data);
    return $pdf->setPaper([0, 0, 609.4488, 396.85], 'portrait')->stream('invoice.pdf');
  }

}
