<?php

namespace App\Http\Controllers;


use DataTables;
use App\User;
use App\Optic;
use App\Sale;
use App\SalesItem;
use App\ItemsStock;
use App\ItemsHistory;
use App\Retur;
use App\ReturItem;

use DateTime;
use DB;
use Auth;

use Illuminate\Http\Request;
use Route;
class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
       $this->middleware('auth');
     }

     public function datatable(){
         $model = Sale::with('optic')->limit(2000)->orderBy('id', 'desc')->get();

         return DataTables::of($model)
                 ->addColumn('action', function(Sale $sale) {
                     $retVal = '
                               <a href="'.url("/Payment").'/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">Payment</a>
                               <a href="'.url("/Order").'/edit/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                               <a href="'.url("/Order").'/view/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">View</a>
                               <a href="'.url("/Order").'/retur/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">Retur</a>
                               <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$sale->id.'" data-original-title="Hapus Data Sale?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                     return $retVal;
                 })
                 ->addColumn('optic_name', function(Sale $sale) {
                    $retVal = $sale->optic->nama;
                    return $retVal;
                 })
                 ->addColumn('status_pembayaran', function(Sale $sale) {
                   if($sale->status_pembayaran == "Pending"){
                     return '<span class="label label-danger"> '.$sale->status_pembayaran.' </span>';
                   }
                   else{
                     return '<span class="label label-success"> '.$sale->status_pembayaran.' </span>';
                   }
                 })
                 ->editColumn('jenis_pembayaran', function(Sale $sale) {
                    if($sale->jenis_pembayaran == "1")
                      return "Sekali Bayar";
                    else
                      return "Bertahap";
                  })
                  ->editColumn('id', function(Sale $sale) {
                     return $sale->id_string;
                   })
                  ->editColumn('created_at', function(Sale $sale) {
                     return $sale->created_at->format('d M Y');
                   })
                 ->rawColumns(['action','status_pembayaran'])
                 ->toJson();
     }

    public function index()
    {
        return view('sales.index');
    }

    public function view($id)
    {
      $sale = Sale::with('optic')
              ->where('id',$id)
              ->first();

      $itemList = $sale->items;

      $data = [
        'sale' => $sale,
        'items' => $itemList
      ];
        return view('sales.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user     = Auth::user();
      $date     = date("d-m-Y");
      $optic    = new Optic;
      $listOptic = Optic::all();
      return view('sales.create', ['model'=>$optic , 'listOptic'=>$listOptic, 'date' => $date,'user'=>$user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->opticSelect == -99){
        return response()->json([
            'status' => 'Failed',
            'message' => 'Pilih optic terlebih dahulu'
        ]);
      }

      $message = "Success create request";
      $status = "OK";

      DB::beginTransaction();
      //get tanggal
      $curDate = date('YmdHis');
      $dueDate = date('Y-m-d H:i:s', strtotime("+".$request->tempo." days"));

      $optic              = Optic::find($request->opticSelect);
      $optic->sisa_plafon -= $request->totalAmount;
      if(!isset($optic->last_order))
        $optic->last_order  = $dueDate;
      $optic->save();

      $sale                   = new Sale;
      $sale->user_id          = Auth::id();
      $sale->cabang_id        = Auth::user()->cabang_id;
      $sale->optic_id         = $request->opticSelect;
      $sale->jenis_pembayaran = $request->jenis_pembayaran;
      $sale->jatuh_tempo      = $dueDate;
      $sale->total            = $request->totalAmount;
      $sale->keterangan       = isset($request->keterangan) ? $request->keterangan : '' ;

      $sale->save();

      for($i=0;$i<$request->itemCounts;$i++){
        $item                   = new SalesItem;
        $item->sale_id          = $sale->id;
        $item->item_id          = $request->input('desc'.$i);
        $item->jumlah           = $request->input('count'.$i);
        $item->diskon           = $request->input('disc'.$i);
        $item->harga_jual       = $request->input('price'.$i);


      //  $stock    = ItemsStock::where('item_id',$request->input('desc'.$i))->first();

        // $stock    = ItemsStock::where('item_id',$request->input('desc'.$i))->first();
        // $stock    = ItemsStock::find($request->input('desc'.$i));

        // $stock    = ItemsStock::where('item_id','=',$request->input('desc'.$i))->
        // where('cabang_id','=',Auth::user()->cabang_id)
        // ->first();




        // if($stock == null || $stock->jumlah < $request->input('count'.$i)){
        //   $message = "Stok tidak cukup, cek kembali request order";
        //   $status = "Failed";
        //   DB::rollBack();
        //   break;
        // }
        // else {
        //   $stock->jumlah =  $stock->jumlah - $request->input('count'.$i);
        //   $stock->save();

        //   $count      = (int)$request->input('count'.$i);

        //   while($count > 0){
        //     $history    = ItemsHistory::where('keterangan','like','%sisa%')
        //                                 ->where('item_id','=',$request->input('desc'.$i))
        //                                 ->where('cabang_id','=',Auth::user()->cabang_id)
        //                                 ->orderBy('created_at')
        //                                 ->first();
        //     if($history !== null){
        //       $sisaItemArr = explode(' ',$history->keterangan);
        //       $sisaItem = (int)$sisaItemArr[1];
        //       if(($sisaItem - $count) < 0){
        //         $history->keterangan = 'habis';
        //         $history->save();
        //         $count -= $sisaItem;
        //       }
        //       else if(($sisaItem - (int)$count) > 0){
        //         $history->keterangan = 'sisa '.($sisaItem - $count);
        //         $history->save();
        //         $count = 0;
        //       }
        //       else{
        //         $history->keterangan = 'habis';
        //         $history->save();
        //         $count = 0;
        //       }
        //     }else{
        //       $history    = ItemsHistory::where('keterangan','=','Stok Datang')
        //                                   ->where('item_id','=',$request->input('desc'.$i))
        //                                   ->where('cabang_id','=',Auth::user()->cabang_id)
        //                                   ->orderBy('created_at')
        //                                   ->first();
        //       if($history !== null){
        //         $sisaItem = $history->jumlah;
        //         if(($sisaItem - $count) < 0){
        //           $history->keterangan = 'habis';
        //           $history->save();
        //           $count -= $sisaItem;
        //         }
        //         else if(($sisaItem - (int)$count) > 0){
        //           $history->keterangan = 'sisa '.($sisaItem - $count);
        //           $history->save();
        //           $count = 0;
        //         }
        //         else{
        //           $history->keterangan = 'habis';
        //           $history->save();
        //           $count = 0;
        //         }
        //       }
        //       else{
        //         $message = "Stok tidak cukup, cek kembali request order";
        //         $status = "Failed";
        //         DB::rollBack();

        //         $count = 0;
        //       }
        //   }}
        // }

        $item->save();


      }

      if($status == "OK")
        DB::commit();

      return response()->json([
          'status' => $status,
          'message' => $message,
          'id'=>$sale->id
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sale = Sale::with('optic')
              ->where('id',$id)
              ->first();
      $user     = Auth::user();
      $date     = date("d-m-Y");
      $optic    = new Optic;
      $listOptic = Optic::all();

      $itemList = $sale->items;

      $fdate = $sale->jatuh_tempo;
      $tdate = $sale->created_at;
      $datetime1 = new DateTime($fdate);
      $datetime2 = new DateTime($tdate);
      $interval = $datetime1->diff($datetime2);
      $tempo = $interval->format('%a');
      //$test = date_diff($sale->jatuh_tempo,$sale->created_at)->format('%a');
      $data = [
        'sale' => $sale,
        'items' => $itemList,
        'model'=>$optic ,
        'listOptic'=>$listOptic,
        'date' => $date,
        'user'=>$user,
        'tempo'=>$tempo
      ];
        return view('sales.edit',$data);
    }

    public function retur($id)
    {
      $sale = Sale::with('optic')
              ->where('id',$id)
              ->first();
      $user     = Auth::user();
      $date     = date("d-m-Y");
      $optic    = new Optic;
      $listOptic = Optic::all();

      $itemList = $sale->items;

      $fdate = $sale->jatuh_tempo;
      $tdate = $sale->created_at;
      $datetime1 = new DateTime($fdate);
      $datetime2 = new DateTime($tdate);
      $interval = $datetime1->diff($datetime2);
      $tempo = $interval->format('%a');
      //$test = date_diff($sale->jatuh_tempo,$sale->created_at)->format('%a');
      $data = [
        'sale' => $sale,
        'items' => $itemList,
        'model'=>$optic ,
        'listOptic'=>$listOptic,
        'date' => $date,
        'user'=>$user,
        'tempo'=>$tempo
      ];
        return view('sales.retur',$data);
    }

    public function prosesRetur(Request $request, $id)
    {

        DB::beginTransaction();

        $message = "Success create request";
        $status = "OK";
        $recoverPlafon = 0;
        $sale = Sale::find($id);


        //Insert retur
        $retur                   = new Retur;
        $retur->user_id          = Auth::id();
        $retur->sale_id          = $id;
        $retur->cabang_id        = Auth::user()->cabang_id;
        $retur->optic_id         = $sale->optic_id;
        $retur->keterangan       = 'Retur sale id '.$id;

        $retur->save();

        // proses retur item
        for($i=0;$i<$request->itemCounts;$i++){
          $item                   = new ReturItem;
          $item->retur_id         = $retur->id;
          $item->item_id          = $request->input('desc'.$i);
          $item->diskon           = $request->input('disc'.$i);
          $item->jumlah           = $request->input('count'.$i);
          $item->harga_jual       = $request->input('price'.$i);

          $recoverPlafon += $item->jumlah * ($item->harga_jual - ($item->harga_jual * $item->diskon / 100));

          $stock                  = New ItemsHistory;

          $stock->jumlah      =  $request->input('count'.$i);
          $stock->item_id     =  $item->item_id;
          $stock->cabang_id   = Auth::user()->cabang_id;
          $stock->harga_beli  = $item->harga_jual;
          $stock->keterangan  = 'retur sale id '.$id.' retur id '.$retur->id;

          $stock->save();

          $saleItem = SalesItem::where('sale_id','=',$id)
                                ->where('item_id','=',$item->item_id)
                                ->first();

          $saleItem->jumlah -= $item->jumlah;

          $saleItem->save();

          $item->save();

        }

        $sale->total -= $recoverPlafon;
        $sale->save();

        $optic = Optic::find($sale->optic_id);
        $optic->sisa_plafon += $recoverPlafon;
        $optic->save();

        DB::commit();

        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateSales(Request $request, $id)
    {
      if($request->opticSelect == -99){
        return response()->json([
            'status' => 'Failed',
            'message' => 'Pilih optic terlebih dahulu'
        ]);
      }

      $message = "Success create request";
      $status = "OK";

      DB::beginTransaction();

      //get tanggal
      $curDate = date('YmdHis');
      $dueDate = date('Y-m-d H:i:s', strtotime("+".$request->tempo." days"));

      //restore item histories

      $saleItem = SalesItem::where('sale_id','=',$id)->get();

      foreach ($saleItem as $key => $item) {
        $count      = (int)$item->jumlah;

        $stock    = ItemsStock::where('item_id','=',$item->item_id)->
        where('cabang_id','=',Auth::user()->cabang_id)
        ->first();

        $stock->jumlah = $stock->jumlah + $count;
        $stock->save();

        while($count > 0){
          $history    = ItemsHistory::where('keterangan','like','%sisa%')
                                      ->where('item_id','=',$item->item_id)
                                      ->where('cabang_id','=',Auth::user()->cabang_id)
                                      ->orderBy('created_at','desc')
                                      ->first();
          if($history !== null){
            $sisaItemArr = explode(' ',$history->keterangan);
            $sisaItem = (int)$sisaItemArr[1];

            if($sisaItem + $count = $history->jumlah){
              $history->keterangan = 'Stok Datang';
              $history->save();

              $count = 0;
            }
            else if($sisaItem + $count > $history->jumlah){
              $history->keterangan = 'Stok Datang';
              $history->save();

              $count -= ($history->jumlah - $sisaItem);
            }
            else if($sisaItem + $count < $history->jumlah){
              $history->keterangan = 'sisa '.($sisaItem + $count);
              $history->save();
              $count = 0;
            }
          }else{
            $history    = ItemsHistory::where('keterangan','=','habis')
                                        ->where('item_id','=',$item->item_id)
                                        ->where('cabang_id','=',Auth::user()->cabang_id)
                                        ->orderBy('created_at','desc')
                                        ->first();
            if($history !== null){
              $sisaItem = 0;

              if($sisaItem + $count = $history->jumlah){
                $history->keterangan = 'Stok Datang';
                $history->save();
                $count = 0;
              }

              else if($sisaItem + $count > $history->jumlah){
                $history->keterangan = 'Stok Datang';
                $history->save();
                $count -= ($history->jumlah - $sisaItem);
              }
              else if($sisaItem + $count < $history->jumlah){
                $history->keterangan = 'sisa '.($sisaItem + $count);
                $history->save();
                $count = 0;
              }
              // $sisaItem = $history->jumlah;
              // if(($sisaItem - $request->input('count'.$i)) < 0){
              //   $history->keterangan = 'habis';
              //   $history->save();
              //   $count -= $sisaItem;
              // }
              // else if(($sisaItem - (int)$request->input('count'.$i)) > 0){
              //   $history->keterangan = 'sisa '.($sisaItem - $request->input('count'.$i));
              //   $history->save();
              //   $count = 0;
              // }
              // else{
              //   $history->keterangan = 'habis';
              //   $history->save();
              //   $count = 0;
              // }
          }
        }}
      }

      $optic              = Optic::find($request->opticSelect);
      $optic->sisa_plafon -= $request->totalAmount;
      if(isset($optic->last_order))
        $optic->last_order  = $dueDate;
      $optic->save();

      $sale                   = new Sale;
      $sale->user_id          = Auth::id();
      $sale->cabang_id        = Auth::user()->cabang_id;
      $sale->optic_id         = $request->opticSelect;
      $sale->jenis_pembayaran = $request->jenis_pembayaran;
      $sale->jatuh_tempo      = $dueDate;
      $sale->total            = $request->totalAmount;
      $sale->keterangan       = isset($request->keterangan) ? $request->keterangan : '' ;

      $sale->save();

      for($i=0;$i<$request->itemCounts;$i++){
        $item                   = new SalesItem;
        $item->sale_id          = $sale->id;

        $stock    = ItemsStock::where('item_id','=',$request->input('desc'.$i))->
        where('cabang_id','=',Auth::user()->cabang_id)
        ->first();

        if($stock->jumlah < $request->input('count'.$i)){
          $message = "Stok tidak cukup, cek kembali request order";
          $status = "Failed";
          DB::rollBack();
          break;
        }
        else {
          $stock->jumlah =  $stock->jumlah - $request->input('count'.$i);
          $stock->save();

          $count      = (int)$request->input('count'.$i);

          while($count > 0){
            $history    = ItemsHistory::where('keterangan','like','%sisa%')
                                        ->where('item_id','=',$request->input('desc'.$i))
                                        ->where('cabang_id','=',Auth::user()->cabang_id)
                                        ->orderBy('created_at')
                                        ->first();
            if($history !== null){
              $sisaItemArr = explode(' ',$history->keterangan);
              $sisaItem = (int)$sisaItemArr[1];
              if(($sisaItem - $count) < 0){
                $history->keterangan = 'habis';
                $history->save();
                $count -= $sisaItem;
              }
              else if(($sisaItem - (int)$count) > 0){
                $history->keterangan = 'sisa '.($sisaItem - $count);
                $history->save();
                $count = 0;
              }
              else{
                $history->keterangan = 'habis';
                $history->save();
                $count = 0;
              }
            }else{
              $history    = ItemsHistory::where('keterangan','=','Stok Datang')
                                          ->where('item_id','=',$request->input('desc'.$i))
                                          ->where('cabang_id','=',Auth::user()->cabang_id)
                                          ->orderBy('created_at')
                                          ->first();
              if($history !== null){
                $sisaItem = $history->jumlah;
                if(($sisaItem - $count) < 0){
                  $history->keterangan = 'habis';
                  $history->save();
                  $count -= $sisaItem;
                }
                else if(($sisaItem - (int)$count) > 0){
                  $history->keterangan = 'sisa '.($sisaItem - $count);
                  $history->save();
                  $count = 0;
                }
                else{
                  $history->keterangan = 'habis';
                  $history->save();
                  $count = 0;
                }
            }
          }}
        }

        $item->item_id          = $request->input('desc'.$i);
        $item->jumlah           = $request->input('count'.$i);
        $item->diskon           = $request->input('disc'.$i);
        $item->harga_jual       = $request->input('price'.$i);

        $item->save();
      }

      $salePrev = Sale::find($id);

      $lastOptic = Optic::find($salePrev->optic_id);
      $lastOptic->sisa_plafon = $lastOptic->sisa_plafon + $salePrev->total;
      $lastOptic->save();

      $salePrev->delete();

      if($status == "OK")
        DB::commit();

      return response()->json([
          'status' => $status,
          'message' => $message,
          'id'=>$sale->id
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $salePrev = Sale::find($id);

      $lastOptic = Optic::find($salePrev->optic_id);
      $lastOptic->sisa_plafon = $lastOptic->sisa_plafon + $salePrev->total;
      $lastOptic->save();

            // $saleItem = SalesItem::where('sale_id','=',$id)->get();

            // foreach ($saleItem as $key => $item) {
            //   $count      = (int)$item->jumlah;

            //   $stock    = ItemsStock::where('item_id','=',$item->item_id)->
            //   where('cabang_id','=',Auth::user()->cabang_id)
            //   ->first();

            //   $stock->jumlah = $stock->jumlah + $count;
            //   $stock->save();

            //   while($count > 0){
            //     $history    = ItemsHistory::where('keterangan','like','%sisa%')
            //                                 ->where('item_id','=',$item->item_id)
            //                                 ->where('cabang_id','=',Auth::user()->cabang_id)
            //                                 ->orderBy('created_at','desc')
            //                                 ->first();
            //     if($history !== null){
            //       $sisaItemArr = explode(' ',$history->keterangan);
            //       $sisaItem = (int)$sisaItemArr[1];

            //       if($sisaItem + $count = $history->jumlah){
            //         $history->keterangan = 'Stok Datang';
            //         $history->save();

            //         $count = 0;
            //       }
            //       else if($sisaItem + $count > $history->jumlah){
            //         $history->keterangan = 'Stok Datang';
            //         $history->save();

            //         $count -= ($history->jumlah - $sisaItem);
            //       }
            //       else if($sisaItem + $count < $history->jumlah){
            //         $history->keterangan = 'sisa '.($sisaItem + $count);
            //         $history->save();
            //         $count = 0;
            //       }
            //     }else{
            //       $history    = ItemsHistory::where('keterangan','=','habis')
            //                                   ->where('item_id','=',$item->item_id)
            //                                   ->where('cabang_id','=',Auth::user()->cabang_id)
            //                                   ->orderBy('created_at','desc')
            //                                   ->first();
            //       if($history !== null){
            //         $sisaItem = 0;

            //         if($sisaItem + $count = $history->jumlah){
            //           $history->keterangan = 'Stok Datang';
            //           $history->save();
            //           $count = 0;
            //         }

            //         else if($sisaItem + $count > $history->jumlah){
            //           $history->keterangan = 'Stok Datang';
            //           $history->save();
            //           $count -= ($history->jumlah - $sisaItem);
            //         }
            //         else if($sisaItem + $count < $history->jumlah){
            //           $history->keterangan = 'sisa '.($sisaItem + $count);
            //           $history->save();
            //           $count = 0;
            //         }
            //         // $sisaItem = $history->jumlah;
            //         // if(($sisaItem - $request->input('count'.$i)) < 0){
            //         //   $history->keterangan = 'habis';
            //         //   $history->save();
            //         //   $count -= $sisaItem;
            //         // }
            //         // else if(($sisaItem - (int)$request->input('count'.$i)) > 0){
            //         //   $history->keterangan = 'sisa '.($sisaItem - $request->input('count'.$i));
            //         //   $history->save();
            //         //   $count = 0;
            //         // }
            //         // else{
            //         //   $history->keterangan = 'habis';
            //         //   $history->save();
            //         //   $count = 0;
            //         // }
            //     }
            //   }}
            // }

        Sale::find($id)->delete();
            return response()->json([
              'status' => 'OK',
              'message' => 'Success delete product'
        ]);
    }
}
