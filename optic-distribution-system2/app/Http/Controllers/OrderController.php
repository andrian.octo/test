<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function datatable(){
        $model = Order::query();

        return DataTables::eloquent($model)
                ->addColumn('action', function(Order $order) {
                    $retVal = ' <a href="'.url("/Order").'/'.$order->id.'/edit" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                              <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$order->id.'" data-original-title="Hapus Data User?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                    return $retVal;
                })
                ->rawColumns(['action'])
                ->toJson();
    }

    public function index()
    {
        $user = Auth::user();
        return view('order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sale = new Sale;
        return view('order.create', ['model'=>$order]);
    }

    public function storeItem(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
