<?php

namespace App\Http\Controllers;
use DataTables;
use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Route;

class CategoryController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function datatable(){

      $model = Category::query();

      return DataTables::eloquent($model)
                ->addColumn('action', function(Category $category) {
                    $retVal = ' <a onClick="editModalCategory('.$category->id.',\''.$category->nama.'\','.$category->expiryStat.')" data-toggle="modal" href="#editModalCategory" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                              <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$category->id.'" data-original-title="Hapus Data Category?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                    return $retVal;
                })
                ->rawColumns(['action','roleString'])
                ->setRowId(function ($category) {
                    return $category->id;
                })
                ->toJson();
    }

    public function getItemJson(Request $request){
      if(isset($request->id))
        $item = Category::find($request->id);
      else
        $item = Category::All();

      return response()->json($item->toArray());
    }


    public function getItemJsonSelect2(Request $request){
      if(isset($request->q)){
        $item = Category::where('nama','like','%'.$request->q.'%')
                ->get();
      }

      else
        $item = Category::All();

      return response()->json($item->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $category = new Category;
      return view('category.create',['model' => $category ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
        'nama'      => 'required|unique:category|max:50'
      ]);

      $category = new Category;

      $category->nama           = $request->nama;
      $category->expiryStat     = $request->expiry;

      $category->save();

      return response()->json([
          'status' => 'OK',
          'message' => 'Success delete user'
      ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validatedData = $request->validate([
        'nama'      => 'required|unique:category|max:50'
      ]);

      $category = Category::find($id);

      $category->nama           = $request->nama;
      $category->expiryStat     = $request->expiry;

      $category->save();

      return response()->json([
          'status' => 'OK',
          'message' => 'Success delete user'
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $countSubCategory = SubCategory::where('category_id',$id)->count();
      if ($countSubCategory>0) {
          $status = "Failed";
          $message = "Kategori Tidak Dapat Dihapus";
      }else{
        Category::find($id)->delete();
        $status = "OK";
        $message = "Kategori Berhasil Dihapus";
      }

      return response()->json([
          'status' => $status,
          'message' => $message
      ]);
    }

    public function ajax_subcategory(Request $request,$id)
    {
      return SubCategory::where('category_id',$id)->get()->toJson();
    }
}
