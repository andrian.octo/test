<?php

namespace App\Http\Controllers;
use DataTables;
use App\User;
use App\Item;
use App\ItemsStock;
use App\ItemsHistory;
use App\Optic;
use App\Sale;
use App\SalesItems;

use App\SubCategory;
use App\Category;
use DB;
use Auth;

use Illuminate\Http\Request;
use Route;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $omset =0;
        $payed = 0;
        $model = DB::table('sales')
            ->select(DB::raw('YEAR(sales.created_at) as nama,sum(total) as jumlah'))
            ->whereYear('created_at', Date('Y'))
            ->groupBy('nama')
            ->get();
        $model2 = DB::table('sales_payment')
            ->join('sales', 'sales_payment.sale_id', '=', 'sales.id')
            ->select(DB::raw('YEAR(sales_payment.created_at) as nama,sum(sales_payment.jumlah) as jumlah'))
            ->whereYear('sales.created_at', Date('Y'))
            ->groupBy('nama')
            ->get();

        foreach ($model as $item) {
        $omset = (int)$item->jumlah/1000000;
        }
        foreach ($model2 as $item) {
        $payed = (int)$item->jumlah/1000000;
        }

        return view('home')->with(['omset'=>$omset,'payed'=>$payed]);
    }
}
