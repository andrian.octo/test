<?php

namespace App\Http\Controllers;

use DataTables;
use App\User;
use App\Optic;
use App\Sale;
use Auth;

use Illuminate\Http\Request;
use Route;
use Validator;
use Redirect;


class OpticController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function getOpticJson(Request $request){
      $optic = Optic::find($request->id);

      $curDate = date('Y-m-d');
      $retVal =$optic->toArray();
      if(isset($optic->last_order)){
        if($curDate > $optic->last_order)
          $retVal += [ "checkDate" => 0 ];
        else
          $retVal += [ "checkDate" => 1 ];
      }
      else{
        $retVal += [ "checkDate" => 1 ];
      }
      //var_dump($retVal);
      return response()->json($retVal);
    }

    public function datatable(){
        $model = Optic::query();

        return DataTables::eloquent($model)
                ->addColumn('action', function(Optic $optic) {
                    $retVal = ' <a href="'.url("/Optic").'/'.$optic->id.'/edit" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                              <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$optic->id.'" data-original-title="Hapus Data Optik?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                    return $retVal;
                })
                ->addColumn('kota', function(Optic $optic) {
                    $retVal = '';
                    if($optic->cabang_id == 1){
                      $retVal = 'Kudus';
                    }
                    else if($optic->cabang_id == 2){
                      $retVal = 'Semarang';;
                    }

                    return $retVal;
                })
                ->rawColumns(['action'])
                ->toJson();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('optic.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $optic = new Optic;
        return view('optic.create', ['model'=>$optic]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'nama'          => 'required|max:50',
          'alamat'        => 'required|max:200',
          'telepon'       => 'required',
          'plafon'        => 'required|integer'
        ]);


        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }

        $optic          = new Optic;
        $optic->nama    = $request->nama;
        $optic->alamat  = $request->alamat;
        $optic->cabang_id = $request->kota;
        $optic->telepon = $request->telepon;
        $optic->plafon  = $request->plafon;
        $optic->sisa_plafon  = $request->plafon;

        $optic->save();

        return redirect()->route('Optic.index')
                                ->with('successMsg','Optic created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $optic = Optic::find($id);
        return view('optic.edit',compact('optic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
        'nama'          => 'required|max:50',
        'alamat'        => 'required|max:200',
        'telepon'       => 'required',
        'plafon'        => 'required|integer'
      ]);

      if ($validator->fails())
      {
          return Redirect::back()->withErrors($validator);
      }


        $optic = Optic::find($id);



        if (Auth::user()->role!=2) {
          if ($optic->sisa_plafon < $optic->plafon)
          {
              $validator->errors()->add('plafon', 'Sisa plafon belum sama dengan plafon awal');
              return Redirect::back()->withErrors($validator);
          }
          $optic->plafon         = $request->plafon;
          $optic->sisa_plafon    = $request->plafon;

        }else{
          if ($optic->sisa_plafon < $optic->plafon){
            $transaksi  = $optic->plafon-$optic->sisa_plafon;

            $optic->plafon      = $request->plafon;
            $optic->sisa_plafon = $request->plafon-$transaksi;
          }else{
            $optic->plafon         = $request->plafon;
            $optic->sisa_plafon    = $request->plafon;
          }
        }

        $optic->cabang_id      = $request->kota;
        $optic->alamat         = $request->alamat;
        $optic->telepon        = $request->telepon;
        $optic->nama           = $request->nama;
        $optic->save();

        return redirect()->route('Optic.index')
                                ->with('successMsg','Optic updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countSales = Sale::where('optic_id',$id)->count();
        if ($countSales>0) {
            $status = "Failed";
            $message = "Optik Tidak Dapat Dihapus";
        }else{
          Optic::find($id)->delete();
          $status = "OK";
          $message = "Item Berhasil Dihapus";
        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

     public function getBillingJson(Request $request){
      $optic = Optic::find($request->id);

      $curDate = date('Y-m-d');
      $retVal =$optic->toArray();
      if(isset($optic->last_order)){
        if($curDate > $optic->last_order)
          $retVal += [ "checkDate" => 0 ];
        else
          $retVal += [ "checkDate" => 1 ];
      }
      else{
        $retVal += [ "checkDate" => 1 ];
      }
      //var_dump($retVal);
      return response()->json($retVal);
    }
}
