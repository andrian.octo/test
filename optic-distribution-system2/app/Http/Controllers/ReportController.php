<?php

namespace App\Http\Controllers;
use DataTables;
use App\User;
use App\Item;
use App\ItemsStock;
use App\ItemsHistory;
use App\Optic;
use App\Sale;
use App\SalesItems;

use App\SubCategory;
use App\Category;
use DB;
use Auth;

use Illuminate\Http\Request;
use Route;

class ReportController extends Controller
{
    public function inventoryDatatable()
    {
    	$model = ItemsStock::with('item')->where('cabang_id', Auth::user()->cabang_id);

        return DataTables::eloquent($model)
                    ->addColumn('nama',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->nama;
                    })
                    ->addColumn('harga_jual_default',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->harga_jual_default;
                    })
                    ->addColumn('category',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->subcategory->category->nama;
                    })
                    ->addColumn('subcategory',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->subcategory->nama;
                    })
                    ->rawColumns(['action'])
                    ->toJson();
    }

    public function expiryDatatable()
    {
      $d2 = date('y-m-d', strtotime('-30 days'));
    	$model = ItemsHistory::with('item')->where('cabang_id', Auth::user()->cabang_id)
                                        ->where('expiry','>',$d2)
                                        ->where('keterangan','<>','habis');

        return DataTables::eloquent($model)
                    ->addColumn('nama',function(ItemsHistory $ItemsHistory){
                        return $ItemsHistory->item->nama;
                    })
                    ->addColumn('category',function(ItemsHistory $ItemsHistory){
                        return $ItemsHistory->item->subcategory->category->nama;
                    })
                    ->addColumn('subcategory',function(ItemsHistory $ItemsHistory){
                        return $ItemsHistory->item->subcategory->nama;
                    })
                    ->addColumn('sisa',function(ItemsHistory $ItemsHistory){

                        $sisa = 0;

                        if (strpos($ItemsHistory->keterangan, 'sisa') !== false) {
                            $sisaItemArr = explode(' ',$ItemsHistory->keterangan);
                            $sisa = (int)$sisaItemArr[1];
                        }
                        else{
                          $sisa = (int)$ItemsHistory->jumlah;
                        }

                        return $sisa;
                    })
                    ->rawColumns(['action'])
                    ->toJson();
    }

    public function inventoryChartJSON(Request $request)
    {
      if(!isset($request->state)){
        $model = DB::table('items_stocks')
              ->join('items', 'items_stocks.item_id', '=', 'items.id')
              ->join('sub_category', 'sub_category.id', '=', 'items.sub_category_id')
              ->join('category', 'category.id', '=', 'sub_category.category_id')
              ->select(DB::raw('category.nama,sum(items_stocks.jumlah) as jumlah'))
              ->groupBy('category.nama')
              ->get();
      }
      else if(isset($request->category)){
        $model = DB::table('items_stocks')
              ->join('items', 'items_stocks.item_id', '=', 'items.id')
              ->join('sub_category', 'sub_category.id', '=', 'items.sub_category_id')
              ->join('category', 'category.id', '=', 'sub_category.category_id')
              ->where('category.nama','=',$request->category)
              ->select(DB::raw('sub_category.nama,sum(items_stocks.jumlah) as jumlah'))
              ->groupBy('sub_category.nama')
              ->get();
      }
      else if(isset($request->subCategory)){
        $model = DB::table('items_stocks')
              ->join('items', 'items_stocks.item_id', '=', 'items.id')
              ->join('sub_category', 'sub_category.id', '=', 'items.sub_category_id')
              ->where('sub_category.nama','=',$request->subCategory)
              ->select(DB::raw('items.nama,sum(items_stocks.jumlah) as jumlah'))
              ->groupBy('items.nama')
              ->get();
      }

      $descArr = array();
      $jumlahArr = array();
      foreach ($model as $item) {
          array_push($descArr, $item->nama);
          array_push($jumlahArr, (int)$item->jumlah);
      }

      $retVal = array("description"=>$descArr,"data"=>$jumlahArr);
      return response()->json($retVal);
    }

    public function salesChartByItemJSON(Request $request)
    {
      if(!isset($request->state)){
        $model = DB::table('sales_items')
              ->join('items', 'sales_items.item_id', '=', 'items.id')
              ->join('sub_category', 'sub_category.id', '=', 'items.sub_category_id')
              ->join('category', 'category.id', '=', 'sub_category.category_id')
              ->select(DB::raw('MONTH(sales_items.created_at) as nama,sum(sales_items.jumlah) as jumlah'))
              ->whereYear('sales_items.created_at', Date('Y'))
              ->groupBy('nama')
              ->get();
      }

      $descArr = array();
      $jumlahArr = array();

      for($i = 1;$i<=12;$i++){
        foreach ($model as $item) {
          if($i == $item->nama){
            array_push($descArr, $item->nama);
            array_push($jumlahArr, (int)$item->jumlah);
          }
          else{
            array_push($descArr, $i);
            array_push($jumlahArr, 0);
          }
        }
      }

      $retVal = array("description"=>$descArr,"data"=>$jumlahArr);
      return response()->json($retVal);
    }

    public function salesChartByRevenueJSON(Request $request)
    {
      if(!isset($request->state)){
        $model = DB::table('sales_items')
              ->join('items', 'sales_items.item_id', '=', 'items.id')
              ->join('sub_category', 'sub_category.id', '=', 'items.sub_category_id')
              ->join('category', 'category.id', '=', 'sub_category.category_id')
              ->select(DB::raw('MONTH(sales_items.created_at) as nama,sum((sales_items.jumlah * sales_items.harga_jual) - ((sales_items.jumlah * sales_items.harga_jual)*sales_items.diskon/100) ) as jumlah'))
              ->whereYear('sales_items.created_at', Date('Y'))
              ->groupBy('nama')
              ->get();
      }

      $descArr = array();
      $jumlahArr = array();

      for($i = 1;$i<=12;$i++){
        foreach ($model as $item) {
          if($i == $item->nama){
            array_push($descArr, $item->nama);
            array_push($jumlahArr, (int)$item->jumlah);
          }
          else{
            array_push($descArr, $i);
            array_push($jumlahArr, 0);
          }
        }
      }

      $retVal = array("description"=>$descArr,"data"=>$jumlahArr);
      return response()->json($retVal);
    }

    public function summaryJson(Request $request)
    {
      $omset =0;
      $payed = 0;
      if(!isset($request->state)){
        $model = DB::table('sales')
              ->select(DB::raw('YEAR(sales.created_at) as nama,sum(total) as jumlah'))
              ->whereYear('created_at', '2018')
              ->groupBy('nama')
              ->get();
        $model2 = DB::table('sales_payment')
              ->join('sales', 'sales_payment.sale_id', '=', 'sales.id')
              ->select(DB::raw('YEAR(sales_payment.created_at) as nama,sum(sales_payment.jumlah) as jumlah'))
              ->whereYear('sales.created_at', '2018')
              ->groupBy('nama')
              ->get();
      }

      $descArr = array();
      $jumlahArr = array();

      foreach ($model as $item) {
        $omset = (int)$item->jumlah/1000000;
      }
      foreach ($model2 as $item) {
        $payed = (int)$item->jumlah/1000000;
      }

      $retVal = array("omset"=>$omset,"payedPayment"=>$payed);
      return response()->json($retVal);
    }

    public function inventoryLowDatatable()
    {
    	$model = ItemsStock::with('item')->where('cabang_id', Auth::user()->cabang_id)->where('jumlah', '<', 50);

        return DataTables::eloquent($model)
                    ->addColumn('nama',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->nama;
                    })
                    ->addColumn('harga_jual_default',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->harga_jual_default;
                    })
                    ->addColumn('category',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->subcategory->category->nama;
                    })
                    ->addColumn('subcategory',function(ItemsStock $ItemsStock){
                        return $ItemsStock->item->subcategory->nama;
                    })
                    ->rawColumns(['action'])
                    ->toJson();
    }

    public function salesDatatable(Request $request)
    {
    	$startFrom = $request->startFrom;
        $to        = $request->to;

        $model = Sale::with('optic');

        if ($startFrom!=null && $to!=null) {
            $startFrom = date("Y-m-d",strtotime($request->startFrom));
            $to        = date("Y-m-d",strtotime($request->to));
            $model = $model->whereBetween('created_at', [$startFrom, $to]);
        }

        $model= $model->get();

         return DataTables::of($model)
                 ->addColumn('action', function(Sale $sale) {
                     $retVal = '
                               <a href="'.url("/Payment").'/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">Payment</a>
                               <a href="'.url("/Order").'/view/'.$sale->id.'" type="button" class="btn yellow-crusta btn-outline">View</a>';
                     return $retVal;
                 })
                 ->addColumn('optic_name', function(Sale $sale) {
                    $retVal = $sale->optic->nama;
                    return $retVal;
                 })
                 ->addColumn('employee', function(Sale $sale) {
                    $retVal = $sale->getStaffNameAttribute()->username;
                    return $retVal;
                 })
                 ->addColumn('status_pembayaran', function(Sale $sale) {
                   if($sale->status_pembayaran == "Pending"){
                     return '<span class="label label-danger"> '.$sale->status_pembayaran.' </span>';
                   }
                   else{
                     return '<span class="label label-success"> '.$sale->status_pembayaran.' </span>';
                   }
                 })
                 ->editColumn('jenis_pembayaran', function(Sale $sale) {
                    if($sale->jenis_pembayaran == "1")
                      return "Sekali Bayar";
                    else
                      return "Bertahap";
                  })
                  ->editColumn('id', function(Sale $sale) {
                     return $sale->id_string;
                   })
                  ->editColumn('created_at', function(Sale $sale) {
                     return $sale->created_at->format('d-m-Y');
                   })
                 ->rawColumns(['action','status_pembayaran'])
                 ->toJson();
    }

    public function inventory()
    {
    	return view('report.inventory');
    }

    public function inventoryLow()
    {
    	return view('report.inventoryLow');
    }

    public function expiry()
    {
    	return view('report.expiredItem');
    }

    public function sales()
    {
      $omset =0;
      $payed = 0;
      $model = DB::table('sales')
            ->select(DB::raw('YEAR(sales.created_at) as nama,sum(total) as jumlah'))
            ->whereYear('created_at', Date('Y'))
            ->groupBy('nama')
            ->get();
      $model2 = DB::table('sales_payment')
            ->join('sales', 'sales_payment.sale_id', '=', 'sales.id')
            ->select(DB::raw('YEAR(sales_payment.created_at) as nama,sum(sales_payment.jumlah) as jumlah'))
            ->whereYear('sales.created_at', Date('Y'))
            ->groupBy('nama')
            ->get();

      foreach ($model as $item) {
        $omset = (int)$item->jumlah/1000000;
      }
      foreach ($model2 as $item) {
        $payed = (int)$item->jumlah/1000000;
      }

    	return view('report.sales')->with(['omset'=>$omset,'payed'=>$payed]);
    }

    public function receivings()
    {

    }
}
