<?php

namespace App\Http\Controllers;

use DataTables;
use App\User;
use App\Optic;
use App\Sale;
use Auth;

use Illuminate\Http\Request;
use Route;
use Validator;
use Redirect;
use PDF;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
       $this->middleware('auth');
    }


   public function index()
    {
        return view('billing.index');
    }

    public function getOpticJson(Request $request){
        $optic = Optic::find($request->id);
        
        $sale = Sale::where('optic_id',$request->id)->get();
        $filtered = $sale->filter(function($model){
            return $model->status_pembayaran == "Pending";
        });



        $retVal['optic'] = $optic->toArray();
        $retVal['sales'] = $filtered->toArray();

        return response()->json($retVal);
    }

    public function datatable(){
         $model = Sale::with('optic')->groupBy('optic_id')->get();

         return DataTables::of($model)
                  ->addColumn('action', function(Sale $sale) {
                            if($sale->status_pembayaran == "Pending"){
                                 $retVal = '
                                       <a href="'.url("/CreateInvoice").'/'.$sale->optic_id.'" type="button" class="btn yellow-crusta btn-outline">Billing</a>';
                               }
                               else{
                                $retVal = '';
                               }
                             
                             return $retVal;
                         })
                 ->addColumn('status_pembayaran', function(Sale $sale) {
                           if($sale->status_pembayaran == "Pending"){
                             return $sale->status_pembayaran;
                           }
                           else{
                             return $sale->status_pembayaran;
                           }
                         })
                 ->addColumn('optic_name', function(Sale $sale) {
                    $retVal = $sale->optic->nama;
                    return $retVal;
                 })
                 ->addColumn('status_pembayaran', function(Sale $sale) {
                           if($sale->status_pembayaran == "Pending"){
                             return $sale->status_pembayaran;
                           }
                           else{
                             return $sale->status_pembayaran;
                           }
                         })

                 ->toJson();

        
     }

     



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $user     = Auth::user();
        $date     = date("d-m-Y");
        $optic    = new Optic;
        $listOptic = Optic::all();
        return view('billing.create', ['model'=>$optic , 'listOptic'=>$listOptic, 'date' => $date,'user'=>$user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $invoices   = $request->invoiceNumber;

        
        $optic = Optic::find($request->opticSelect);

        $billingList = array();
        $i=1;
        $total_billing = 0;
        foreach ($invoices as $invoiceID) {
            $sales = Sale::find($invoiceID);
            $billingRow['no'] = $i;
            $billingRow['invoiceID'] = $invoiceID;
            $billingRow['tanggal'] = date('d-m-Y', strtotime($sales->created_at));
            $billingRow['total'] = $sales->total;
            $billingList[]=$billingRow;
            $total_billing = $total_billing + $sales->total;
            $i++;
        }

        $data = [
            'ct'          => $request->ct,
            'jatuh_tempo' => $request->jatuh_tempo,
            'facet'       => $request->facet,
            'tinting'     => $request->tinting,
            'kirim'       => $request->kirim,
            'lain'        => $request->lain,
            'optic'       => $optic,
            'billingList' => $billingList,
            'total_billing' => $total_billing
        ];

        $pdf = PDF::loadView('printTheme.billing',$data);
        return $pdf->setPaper('a4', 'portrait')
                ->download('billing.pdf');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::with('optic')->where('optic_id',$id)->get();
        $filtered = $sale->filter(function($model){
            return $model->status_pembayaran == "Pending";
        });

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
