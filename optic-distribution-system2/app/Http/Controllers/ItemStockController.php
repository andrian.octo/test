<?php

namespace App\Http\Controllers;


use DataTables;
use App\User;
use App\Item;
use App\ItemsStock;
use App\ItemsHistory;
use DB;
use App\SubCategory;
use App\Category;
use Auth;


use Illuminate\Http\Request;
use Route;

class ItemStockController extends Controller
{
    public function index()
    {
        return view('itemStock.index');
    }

    public function datatable()
    {
        // $model = ItemsStock::with('item.subcategory.category')->where('cabang_id', Auth::user()->cabang_id);

        // return DataTables::eloquent($model)
        //             ->addColumn('nama',function(ItemsStock $ItemsStock){
        //                 return $ItemsStock->item->nama;
        //             })
        //             ->addColumn('harga_jual_default',function(ItemsStock $ItemsStock){
        //                 return $ItemsStock->item->harga_jual_default;
        //             })
        //             ->addColumn('category',function(ItemsStock $ItemsStock){
        //                 return $ItemsStock->item->subcategory->category->nama;
        //             })
        //             ->addColumn('subcategory',function(ItemsStock $ItemsStock){
        //                 return $ItemsStock->item->subcategory->nama;
        //             })
        //             ->addColumn('action', function(ItemsStock $ItemsStock) {
        //                 $retVal = ' <a style="display: none;" href="'.url("/Stock/updateItemStock").'/'.$ItemsStock->id.'" type="button" class="btn yellow-crusta btn-outline">Update Stok</a>
        //                           <a href="'.url("/Stock/detail").'/'.$ItemsStock->id.'" type="button" class="btn yellow-crusta btn-outline">Detail</a>';
        //                 return $retVal;
        //             })
        //             ->rawColumns(['action'])
        //             ->toJson();

        $model = DB::table('v_item_stocks')->where('cabang_id', Auth::user()->cabang_id);


        return DataTables::of($model)
                    ->addColumn('action', function($model) {
                        $retVal = ' <a style="display: none;" href="'.url("/Stock/updateItemStock").'/'.$model->id.'" type="button" class="btn yellow-crusta btn-outline">Update Stok</a>
                                  <a href="'.url("/Stock/detail").'/'.$model->id.'" type="button" class="btn yellow-crusta btn-outline">Detail</a>';
                        return $retVal;
                    })
                    ->make(true);
    }

    public function newStock($id){

        $itemStock      = ItemsStock::where('id', '=', $id)
                                ->where('cabang_id', '=', Auth::user()->cabang_id)->first();
        $item           = Item::find($itemStock->item_id);

        return view('itemStock.newStock',['item'=>$item, 'itemStock'=>$itemStock]);
    }

    public function receiveStock(){
        $item = new Item;
        $listItem = Item::with('itemsStock')->get();
        $category = Category::all();
        return view('itemStock.receiveStock',['model'=>$item,'items'=>$listItem, 'category'=>$category]);
    }

    public function update(Request $request, $id){

        // data item stock history
        $itemHistories = ItemsHistory::find($id);
        $itemId = $itemHistories->item_id;
        

        // data item stock
        $itemStock   = ItemsStock::where('item_id', '=', $itemId)
                        ->where('cabang_id', '=', Auth::user()->cabang_id)->first();

        // data item 
        $item = Item::find($itemId);

        // data for the update
        //keterangan, jumlah stock history, stock total
        $itemHistoriesJumlah        = $itemHistories->jumlah;
        $itemHistoriesKeterangan    = $itemHistories->keterangan;
        $itemStockTotal             = $itemStock->jumlah;

        // request data
        $requestStockHistoryUpdate = $request->jumlahStokUpdate;



        // operation
        if ($itemHistoriesKeterangan=="Stok Datang") {
            $sisaItem = 0;
            // stok datang -> langsung update jumlah stok history dan stok total
            $totalStockUpdate = ($itemStockTotal-$itemHistoriesJumlah)+$requestStockHistoryUpdate;
            
            // update item History
            $itemHistories->jumlah = $requestStockHistoryUpdate;
            $itemHistories->save();

            // update item stock
            $itemStock->jumlah = $totalStockUpdate;
            $itemStock->save();
        }else{
            $sisaItemArr = explode(' ',$itemHistories->keterangan);
            $sisaItem = (int)$sisaItemArr[1];
            $totalStockUpdate = ($itemStockTotal-$sisaItem)+$requestStockHistoryUpdate;

            // update item History
            $itemHistories->jumlah = $itemHistoriesJumlah-($sisaItem-$requestStockHistoryUpdate);
            $itemHistories->keterangan = "sisa ".$requestStockHistoryUpdate;
            $itemHistories->save();

            // update item stock
            $itemStock->jumlah = $totalStockUpdate;
            $itemStock->save();

        }

        return response()->json([
          'status' => 'OK',
          'message' => 'Success Update Stok Datang'
        ]);
    }

    public function store(Request $request)
    {
        for($i=0;$i<$request->itemCounts;$i++){
             $itemId                     = $request->input('desc'.$i);
             $newStock                   = $request->input('count'.$i);
             $expiry                     = $request->input('expiry'.$i);
             if (!is_null($expiry)) {
                 $expiry                     = date('Y-m-d', strtotime(str_replace('-', '/', $expiry)));
             }else{
                $expiry = NULL;
             }

            // item history data
            $itemsHistory = new ItemsHistory;
            $itemsHistory->item_id       = $itemId;
            $itemsHistory->cabang_id     = Auth::user()->cabang_id;
            $itemsHistory->harga_beli    = $request->input('price'.$i);
            $itemsHistory->expiry        = $expiry;
            $itemsHistory->jumlah        = $newStock;
            $itemsHistory->keterangan    = "Stok Datang";
            $itemsHistory->save();

            //item stock update
            $itemStock           = ItemsStock::where('item_id', '=', $itemId)
                                            ->where('cabang_id', '=', Auth::user()->cabang_id)->first();
            if (is_null($itemStock)) {
                // new item stock
                $itemStock = new ItemsStock;
                $itemStock->item_id     = $itemId;
                $itemStock->cabang_id   = Auth::user()->cabang_id;
                $itemStock->jumlah      = $newStock;
                $itemStock->save();
            }else{
                // update item stock
                $currentStock        = $itemStock->jumlah;
                $updateStock         = $currentStock+$newStock;
                $itemStock->jumlah   = $updateStock;
                $itemStock->save();
            }

          }

        return redirect()->route('Stock.index')
                        ->with('successMsg','Stock Updated Successfully');
    }

    public function detailStock($id)
    {
        // item stock data
        $itemStock = ItemsStock::where('id', '=', $id)
                                ->where('cabang_id', '=', Auth::user()->cabang_id)->first();
        // item data
        $item = Item::find($itemStock->item_id);
        // item stock histories
        $itemHistories = ItemsHistory::where('item_id', '=', $itemStock->item_id)
                                ->where('cabang_id', '=', Auth::user()->cabang_id)->get();

        return view('itemStock.detailStock', [
                    'item'=>$item,
                    'itemStock'=>$itemStock,
                    'itemHistories' => $itemHistories
                ]);
    }


}
