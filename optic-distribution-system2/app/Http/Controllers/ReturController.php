<?php

namespace App\Http\Controllers;

use DataTables;
use App\User;
use App\Retur;
use App\ReturItem;
use App\Optic;
use App\Sale;
use App\SalesItems;
use App\ItemsStock;
use App\ItemsHistory;

use DateTime;
use DB;
use Auth;
use Illuminate\Http\Request;

class ReturController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function datatable(){
      $model = Retur::where('cabang_id','=',Auth::user()->cabang_id)
                      ->whereNull('optic_id');
      return DataTables::of($model)
              ->addColumn('action', function(Retur $retur) {
                  $retVal = '
                            <a href="'.url("/ReturDistributor").'/edit/'.$retur->id.'" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                            <a href="'.url("/ReturDistributor").'/view/'.$retur->id.'" type="button" class="btn yellow-crusta btn-outline">View</a>
                            <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$retur->id.'" data-original-title="Hapus Data Retur?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                  return $retVal;
              })
               ->editColumn('id', function(Retur $retur) {
                  return $retur->id_string;
                })
               ->editColumn('created_at', function(Retur $retur) {
                  return $retur->created_at->format('d M Y');
                })
              ->rawColumns(['action'])
              ->toJson();
  }

  public function datatableClient(){
      $model = Retur::where('cabang_id','=',Auth::user()->cabang_id)
                      ->whereNotNull('optic_id');

      return DataTables::of($model)
              ->addColumn('action', function(Retur $retur) {
                  $retVal = '
                            <a href="'.url("/ReturClient").'/view/'.$retur->id.'" type="button" class="btn yellow-crusta btn-outline">View</a>
                            <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$retur->id.'" data-original-title="Hapus Data Retur?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                  return $retVal;
              })
               ->editColumn('id', function(Retur $retur) {
                  return $retur->id_string;
                })
               ->editColumn('created_at', function(Retur $retur) {
                  return $retur->created_at->format('d M Y');
                })
              ->rawColumns(['action'])
              ->toJson();
  }

  public function index()
  {
      return view('retur.index');
  }

  public function indexClient()
  {
      return view('retur.indexReturClient');
  }

  public function viewDistributor($id)
  {
    $retur = Retur::where('id',$id)
                    ->first();

    $itemList = $retur->items;

    $data = [
      'sale' => $retur,
      'items' => $itemList
    ];


      return view('retur.viewReturDistributor',$data);
  }

  public function viewClient($id)
  {
    $retur = Retur::where('id',$id)
                    ->first();

    $optic = Optic::find($retur->optic_id);

    $itemList = $retur->items;

    $data = [
      'sale' => $retur,
      'items' => $itemList,
      'optic' => $optic
    ];


      return view('retur.viewReturClient',$data);
  }

  public function createDistributor()
  {
    $user     = Auth::user();
    $date     = date("d-m-Y");
    $optic    = new Optic;
    $listOptic = Optic::all();
    return view('retur.createReturDistributor', ['model'=>$optic , 'listOptic'=>$listOptic, 'date' => $date,'user'=>$user]);
  }

  public function destroyClient($id){

    $retur    = Retur::find($id);

    $histories = ItemsHistory::where('keterangan','like','%retur id '.$id)
                                ->get();

    foreach ($histories as $key => $item) {
      $item->delete();
    }

    $retur->delete();
    return response()->json([
        'status' => 'OK',
        'message' => 'Success delete retur'
    ]);
  }

  public function destroyDistributor($id)
  {

    $listItem = ReturItem::where('retur_id','=',$id)->get();
    foreach ($listItem as $key => $item) {
      $count = $item->jumlah;
      while($count > 0){
        $history    = ItemsHistory::where('keterangan','like','%sisa%')
                                    ->where('item_id','=',$item->item_id)
                                    ->where('cabang_id','=',Auth::user()->cabang_id)
                                    ->orderBy('created_at','desc')
                                    ->first();
        if($history !== null){
          $sisaItemArr = explode(' ',$history->keterangan);
          $sisaItem = (int)$sisaItemArr[1];

          if($sisaItem + $count >= $history->jumlah){
            $history->keterangan = 'Stok Datang';
            $history->save();

            $stok = ItemsStock::where('item_id','=',$item->item_id)
                               ->where('cabang_id','=',Auth::user()->cabang_id)
                               ->orderBy('created_at','desc')->first();
            $stok->jumlah += ($history->jumlah - $sisaItem);
            $stok->save();
            $count -= $sisaItem;
          }
          else if($sisaItem + $count < $history->jumlah){
            $history->keterangan = 'sisa '.($sisaItem + $count);
            $history->save();

            $stok = ItemsStock::where('item_id','=',$item->item_id)
                               ->where('cabang_id','=',Auth::user()->cabang_id)
                               ->orderBy('created_at','desc')->first();
            $stok->jumlah += ($count);
            $stok->save();

            $count -= $sisaItem;
          }
        }else{
          $history    = ItemsHistory::where('keterangan','=','habis')
                                      ->where('item_id','=',$item->id)
                                      ->where('cabang_id','=',Auth::user()->cabang_id)
                                      ->orderBy('created_at','desc')
                                      ->first();
          if($history !== null){
            $sisaItem = 0;

            if($sisaItem + $count >= $history->jumlah){
              $history->keterangan = 'Stok Datang';
              $history->save();

              $stok = ItemsStock::where('item_id','=',$item->item_id)
                                 ->where('cabang_id','=',Auth::user()->cabang_id)
                                 ->orderBy('created_at','desc')->first();
              $stok->jumlah += ($history->jumlah - $sisaItem);
              $stok->save();

              $count -= $history->jumlah;
            }
            else if($sisaItem + $count < $history->jumlah){
              $history->keterangan = 'sisa '.($sisaItem + $count);
              $history->save();

              $stok = ItemsStock::where('item_id','=',$item->item_id)
                                 ->where('cabang_id','=',Auth::user()->cabang_id)
                                 ->orderBy('created_at','desc')->first();
              $stok->jumlah += $count;
              $stok->save();

              $count = 0;
            }

        }
      }}
    }

    $retur    = Retur::find($id);
    $retur->delete();
    return response()->json([
        'status' => 'OK',
        'message' => 'Success delete retur'
    ]);
  }

  public function editDistributor($id)
  {
    $sale = Retur::where('id',$id)
            ->first();
    $user     = Auth::user();
    $date     = date("d-m-Y");
    $optic    = new Optic;
    $listOptic = Optic::all();

    $itemList = $sale->items;

    //$test = date_diff($sale->jatuh_tempo,$sale->created_at)->format('%a');
    $data = [
      'sale' => $sale,
      'items' => $itemList,
      'model'=>$optic ,
      'listOptic'=>$listOptic,
      'date' => $date,
      'user'=>$user
    ];

    return view('retur.editReturDistributor', $data);
  }

  public function storeDistributor(Request $request)
  {
    DB::beginTransaction();

    $message = "Success create request";
    $status = "OK";

    $retur                   = new Retur;
    $retur->user_id          = Auth::id();
    $retur->cabang_id        = Auth::user()->cabang_id;

    $retur->keterangan       = isset($request->keterangan) ? $request->keterangan : '' ;

    $retur->save();

    for($i=0;$i<$request->itemCounts;$i++){
      $item                   = new ReturItem;
      $item->retur_id         = $retur->id;
      $item->item_id          = $request->input('desc'.$i);
      $item->diskon           = $request->input('disc'.$i);
      $item->jumlah           = $request->input('count'.$i);
      $item->harga_jual       = $request->input('price'.$i);

      $stock    = ItemsStock::find($request->input('desc'.$i));

      if($stock == null || $stock->jumlah < $request->input('count'.$i)){
        $message = "Stok tidak cukup, cek kembali request order";
        $status = "Failed";
        DB::rollBack();
        break;
      }
      else {
        $stock->jumlah =  $stock->jumlah - $request->input('count'.$i);
        $stock->save();

        $count      = (int)$request->input('count'.$i);

        while($count > 0){
          $history    = ItemsHistory::where('keterangan','like','%sisa%')
                                      ->where('item_id','=',$request->input('desc'.$i))
                                      ->where('cabang_id','=',Auth::user()->cabang_id)
                                      ->orderBy('created_at')
                                      ->first();
          if($history !== null){
            $sisaItemArr = explode(' ',$history->keterangan);
            $sisaItem = (int)$sisaItemArr[1];
            if(($sisaItem - $request->input('count'.$i)) < 0){
              $history->keterangan = 'habis';
              $history->save();
              $count -= $sisaItem;
            }
            else if(($sisaItem - (int)$request->input('count'.$i)) > 0){
              $history->keterangan = 'sisa '.($sisaItem - $request->input('count'.$i));
              $history->save();
              $count = 0;
            }
            else{
              $history->keterangan = 'habis';
              $history->save();
              $count = 0;
            }
          }else{
            $history    = ItemsHistory::where('keterangan','=','Stok Datang')
                                        ->where('item_id','=',$request->input('desc'.$i))
                                        ->where('cabang_id','=',Auth::user()->cabang_id)
                                        ->orderBy('created_at')
                                        ->first();
            if($history !== null){
              $sisaItem = $history->jumlah;
              if(($sisaItem - $request->input('count'.$i)) < 0){
                $history->keterangan = 'habis';
                $history->save();
                $count -= $sisaItem;
              }
              else if(($sisaItem - (int)$request->input('count'.$i)) > 0){
                $history->keterangan = 'sisa '.($sisaItem - $request->input('count'.$i));
                $history->save();
                $count = 0;
              }
              else{
                $history->keterangan = 'habis';
                $history->save();
                $count = 0;
              }
          }
        }}
      }

      $item->save();

    }

    DB::commit();

    return response()->json([
        'status' => $status,
        'message' => $message
    ]);
  }

  public function updateDistributor(Request $request,$id)
  {
    DB::beginTransaction();

    $message = "Success create request";
    $status = "OK";

    $retur                   = new Retur;
    $retur->user_id          = Auth::id();
    $retur->cabang_id        = Auth::user()->cabang_id;

    $retur->keterangan       = isset($request->keterangan) ? $request->keterangan : '' ;

    $retur->save();

    for($i=0;$i<$request->itemCounts;$i++){
      $item                   = new ReturItem;
      $item->retur_id         = $retur->id;
      $item->item_id          = $request->input('desc'.$i);
      $item->diskon           = $request->input('disc'.$i);
      $item->jumlah           = $request->input('count'.$i);
      $item->harga_jual       = $request->input('price'.$i);

      $stock    = ItemsStock::find($request->input('desc'.$i));

      if($stock == null || $stock->jumlah < $request->input('count'.$i)){
        $message = "Stok tidak cukup, cek kembali request order";
        $status = "Failed";
        DB::rollBack();
        break;
      }
      else {
        $stock->jumlah =  $stock->jumlah - $request->input('count'.$i);
        $stock->save();

        $count      = (int)$request->input('count'.$i);

        while($count > 0){
          $history    = ItemsHistory::where('keterangan','like','%sisa%')
                                      ->where('item_id','=',$request->input('desc'.$i))
                                      ->where('cabang_id','=',Auth::user()->cabang_id)
                                      ->orderBy('created_at')
                                      ->first();
          if($history !== null){
            $sisaItemArr = explode(' ',$history->keterangan);
            $sisaItem = (int)$sisaItemArr[1];
            if(($sisaItem - $request->input('count'.$i)) < 0){
              $history->keterangan = 'habis';
              $history->save();
              $count -= $sisaItem;
            }
            else if(($sisaItem - (int)$request->input('count'.$i)) > 0){
              $history->keterangan = 'sisa '.($sisaItem - $request->input('count'.$i));
              $history->save();
              $count = 0;
            }
            else{
              $history->keterangan = 'habis';
              $history->save();
              $count = 0;
            }
          }else{
            $history    = ItemsHistory::where('keterangan','=','Stok Datang')
                                        ->where('item_id','=',$request->input('desc'.$i))
                                        ->where('cabang_id','=',Auth::user()->cabang_id)
                                        ->orderBy('created_at')
                                        ->first();
            if($history !== null){
              $sisaItem = $history->jumlah;
              if(($sisaItem - $request->input('count'.$i)) < 0){
                $history->keterangan = 'habis';
                $history->save();
                $count -= $sisaItem;
              }
              else if(($sisaItem - (int)$request->input('count'.$i)) > 0){
                $history->keterangan = 'sisa '.($sisaItem - $request->input('count'.$i));
                $history->save();
                $count = 0;
              }
              else{
                $history->keterangan = 'habis';
                $history->save();
                $count = 0;
              }
          }
        }}
      }

      $item->save();

    }
    $salePrev = Retur::find($id);

    $salePrev->delete();

    DB::commit();

    return response()->json([
        'status' => $status,
        'message' => $message
    ]);
  }
}
