<?php

namespace App\Http\Controllers;

use DataTables;
use App\User;
use App\Cabang;
use Illuminate\Http\Request;
use Route;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return view('user.index');
    }

    public function datatable(){

      $model = User::query();

      return DataTables::eloquent($model)
                ->addColumn('action', function(User $user) {
                    $retVal = '';
                    if($user->role == "0"){
                      $retVal = ' <a href="'.url("/User").'/'.$user->id.'/edit" type="button" class="btn yellow-crusta btn-outline">Edit</a>';
                    }
                    else{
                      $retVal = ' <a href="'.url("/User").'/'.$user->id.'/edit" type="button" class="btn yellow-crusta btn-outline">Edit</a>
                              <a data-toggle="confirmation" data-btn-ok-class="btn btn-sm btn-success" data-btn-cancel-class="btn btn-danger btn-sm" data-id="'.$user->id.'" data-original-title="Hapus Data User?" data-singleton="true" type="button" class="btn red btn-outline" >delete</a>';
                    }

                    return $retVal;
                })
                ->addColumn('roleString', function(User $user) {
                    $retVal = '';
                    if($user->role == "0"){
                      $retVal = '<span class="label label-xs label-info"> Owner </span>';
                    }
                    else if($user->role == "1"){
                      $retVal = '<span class="label label-xs label-success"> Customer Service </span>';;
                    }
                    else{
                      $retVal = '<span class="label label-xs label-default"> Kepala Gudang </span>';;
                    }

                    return $retVal;
                })
                ->addColumn('cabang', function(User $user) {
                  $retVal = '';
                  if($user->cabang_id == 1){
                    $retVal = 'Semarang';
                  }
                  else if($user->cabang_id == 2){
                    $retVal = 'Kudus';;
                  }

                  return $retVal;
                })
                ->rawColumns(['action','roleString'])
                ->filterColumn('cabang', function($query, $keyword) {
                  $key = "";
                    if(strtoupper($keyword) == "SEMARANG")
                      $key = "1";
                    else if(strtoupper($keyword) == "KUDUS")
                      $key = "2";

                    $sql = "cabang_id = ?";
                    if($key != "")
                    $query->whereRaw($sql, ["{$key}"]);
                })
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        return view('user.create',['model' => $user ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //check if role not choosen yet
      $role   = $request->role;
      if(!isset($role)){
        $role = 1;
      }

      $validatedData = $request->validate([
        'name'      => 'required|max:255',
        'username'  => 'required|unique:users',
        'password'  => 'required'
      ]);

      $user = new User;

      $user->name     = $request->name;
      $user->username = $request->username;
      $user->password = bcrypt($request->password);
      $user->role     = $role;
      $user->cabang_id= $request->cabang;

      $user->save();

      return redirect()->route('User.index')
                        ->with('successMsg','User created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Int $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = User::find($id);
      return view('user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //check if role not choosen yet
      $role   = $request->role;
      if(!isset($role)){
        $role = 1;
      }

      $validatedData = $request->validate([
        'name'      => 'required|max:255',
        'username'  => 'required',
        'password'  => 'required'
      ]);

      $user = User::find($id);

      $user->name     = $request->name;
      $user->username = $request->username;
      $user->password = bcrypt($request->password);
      $user->role     = $role;
      $user->cabang_id= $request->cabang;

      $user->save();

      return redirect()->route('User.index')
                        ->with('successMsg','User updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      User::find($id)->delete();
      return response()->json([
          'status' => 'OK',
          'message' => 'Success delete user'
      ]);
    }
}
