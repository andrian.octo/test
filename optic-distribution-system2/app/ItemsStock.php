<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsStock extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'items_stocks';

    public function item(){
		return $this->belongsTo('App\Item', 'item_id', 'id');
	}
}
