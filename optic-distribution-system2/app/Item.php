<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'items';

    public function subCategory(){
		return $this->belongsTo('App\SubCategory','sub_category_id','id');
	}

	public function itemsStock(){
		return $this->hasMany('App\ItemsStock', 'item_id', 'id');
	}

	public function itemHistory(){
		return $this->hasOne('App\ItemsHistory', 'item_id', 'id');
	}

	public function sales()
	{
	  return $this->belongsToMany('App\SalesItems', 'sales_items', 'item_id', 'sale_id');
	}

}
