<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'sub_category';

  public function category(){
    return $this->belongsTo('App\Category','category_id','id');
  }

  public function item(){
  	return $this->hasMany('App\Item', 'sub_category_id', 'id');
  }  
}
