<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'cabang';

    public function user(){
	    return $this->belongsTo('App\User','id','id');
	}
}
