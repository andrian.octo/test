<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class NPMForm extends Facade{

    protected static function getFacadeAccessor() { return 'NPMForm'; }

}
