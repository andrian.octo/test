<?php

namespace App\Helpers;
use Form;
use File;

class NPMForm {

    public function Label($id,$textLabel)
    {
        $retVal = '<label for="'.$id.'">'.$textLabel.'</label>';

        return $retVal;
    }
    public function Input($id,$att)
    {
        $retVal = '<input type="text" ';
        $class  = 'class = "form-control';

        foreach ($att as $key => $value) {
          if($key == 'class')
            $class = $class . " ".$value;
          else {
            $retVal = $retVal .' '.$key.'="'.$value.'"';
          }
        }
        $class  = $class .'"';
        $retVal = $retVal .''.$class.' id="'.$id.'" name="'.$id.'"';

        return $retVal;
    }
    public function Email($id,$att)
    {
        $retVal = '<input type="email" ';
        $class  = 'class = "form-control';

        foreach ($att as $key => $value) {
          if($key == 'class')
            $class = $class . " ".$value;
          else {
            $retVal = $retVal .' '.$key.'="'.$value.'"';
          }
        }
        $class  = $class .'"';
        $retVal = $retVal .''.$class.' id="'.$id.'" name="'.$id.'"';

        return $retVal;
    }
    public function Password($id,$att)
    {
        $retVal = '<input type="email" ';
        $class  = 'class = "form-control';

        foreach ($att as $key => $value) {
          if($key == 'class')
            $class = $class . " ".$value;
          else {
            $retVal = $retVal .' '.$key.'="'.$value.'"';
          }
        }
        $class  = $class .'"';
        $retVal = $retVal .''.$class.' id="'.$id.'" name="'.$id.'"';

        return $retVal;
    }
    public function Alert($message)
    {
      $retVal = '<span class="help-block">'.$message.'</span>';

      return $retVal;
    }

    // public function InputGroup($id,$textLabel,$att)
    // {
    //   $retVal = '<div class="form-group">';
    //
    //   $retVal = $retVal . $this->Label($id,$textLabel) . $this->Input($id,$att) . $this->Alert('Default');
    //
    //   return $retVal;
    // }

    private function checkError($errors,$id){
      $retVal = '';

      if(sizeof($errors->getBag('default')->get($id)) > 0)
        $retVal = 'has-error';

      return $retVal;
    }

    public function FormGenerate($errors,$attr){
      $retVal = '';


      foreach ($attr as $key => $value) {
        //Header Form Group
        $retVal .= '<div class="form-group '.$this->checkError($errors,$value[1]).'">'.
                  Form::label($value[1], $value[2],['class' => $value[3].' control-label']).
                  '<div class="'.$value[4].'">';

        //Generate Form
        if($value[0] == 'text'){
          $retVal .= Form::text($value[1], $value[5], ['class' => 'form-control','placeholder' => 'Input '.$value[2]]);

          foreach ($errors->getBag('default')->get($value[1]) as $key => $errMessage) {
            $retVal .= '<span class="help-block">'.$errMessage.'</span>';
          }
        }

        else if($value[0] == 'password'){
          $retVal .= Form::password('password', ['class' => 'form-control','placeholder' => 'Input '.$value[2],'value' => $value[5]]);

          foreach ($errors->getBag('default')->get($value[1]) as $key => $errMessage) {
            $retVal .= '<span class="help-block">'.$errMessage.'</span>';
          }
        }
        $retVal .= '</div></div>';
      }



      return $retVal;
    }

    public function InputGroup($id,$att,$attForm){
      $classLabel   = 'control-label '. (isset($att['labelClass'])) ? $att['labelClass'] : '';
      if(isset($att['formClass'])){
        $classForm    = 'form-control '.$att['formClass'];
      }
      else {
        $classForm    = 'form-control ';
      }


      $formArr['class'] = $classForm;

      foreach ($attForm as $key => $value) {
        $formArr[$key] = $value;
      }

      $retVal = '<div class="form-group">'.Form::label($id, $att['labelName'],['class' => $classLabel]).
        '<div class="'.$att['sizeForm'].'">'.Form::text($id, '', $formArr).'</div>
        </div>';

        return $retVal;
    }

}
