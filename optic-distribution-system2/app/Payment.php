<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'sales_payment';

  public function getMetodePembayaranAttribute()
  {
      if($this->metode == 1){
        return "Cash";
      }else {
        return "Transfer";
      }
  }
}
