<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Retur extends Model
{
  use SoftDeletes;
  protected $primaryKey = 'id';
  protected $table = 'retur';

  public function getIdStringAttribute()
  {
      $temp = "".$this->id;
      $retVal = "";
      for($i=0;$i<(12 - strlen($temp));$i++){
        $retVal = $retVal."0";
      }
      $retVal = $retVal."". $temp;

      return $retVal;
  }

  public function items()
  {
      return $this->hasMany('App\ReturItem', 'retur_id', 'id');
  }

  public function getStaffNameAttribute(){
    $user = User::find($this->user_id)->first();

    return $user;
  }
}
