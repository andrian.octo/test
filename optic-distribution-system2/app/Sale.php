<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Sale extends Model
{
use SoftDeletes;
  public function optic(){
      return $this->belongsTo('App\Optic','optic_id','id');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item', 'sales_items',
          'sale_id', 'item_id')->withPivot('jumlah', 'harga_jual','diskon');
    }

    public function getStaffNameAttribute(){
      $user = User::find($this->user_id)->first();

      return $user;
    }

    public function getTotalTerbayarAttribute(){
      $retVal = Payment::where('sale_id','=',$this->id)->sum('jumlah');
      return $retVal;
    }

    public function getIdStringAttribute()
    {
        $temp = "".$this->id;
        $retVal = "";
        for($i=0;$i<(12 - strlen($temp));$i++){
          $retVal = $retVal."0";
        }
        $retVal = $retVal."". $temp;

        return $retVal;
    }

    public function getJenisStringAttribute()
    {
        if($this->jenis_pembayaran == "1"){
          return "Sekali Bayar";
        }else {
          return "Bertahap";
        }
    }

    public function getStatusPembayaranAttribute(){
      $terbayar = $this->getTotalTerbayarAttribute();

      if($terbayar!=$this->total)
        return "Pending";
      else
        return "Lunas";
    }

    protected $dates = ['deleted_at'];
}
