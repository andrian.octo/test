<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsHistory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'items_histories';

    public function item(){
      return $this->belongsTo('App\Item','item_id','id');
    }

}
