<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Optic extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'optics';

    public function sale(){
        return $this->hasMany('App\Sale');
      }
    public function getKotaAttribute(){
      return $this->cabang_id == 1 ? 'Kudus' : 'Semarang';
    }
}
