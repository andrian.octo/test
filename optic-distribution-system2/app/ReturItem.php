<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturItem extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'retur_item';

  public function item()
  {
      return $this->belongsTo('App\Item','item_id', 'id');
  }
}
