<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('category')->insert([
      'nama'      => "Lensa",
      ]);

      DB::table('category')->insert([
      'nama'      => "Pembersih Lensa",
      ]);

      DB::table('category')->insert([
      'nama'      => "Frame",
      ]);

    }
}
