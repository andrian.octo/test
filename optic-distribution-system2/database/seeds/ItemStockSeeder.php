<?php

use Illuminate\Database\Seeder;

class ItemStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items_stocks')->insert([
			'item_id' => 1,
			'cabang_id'  => 1,
			'jumlah'      => 100
		]);

		DB::table('items_stocks')->insert([
			'item_id' => 2,
			'cabang_id'  => 1,
			'jumlah'      => 100
		]);

		DB::table('items_stocks')->insert([
			'item_id' => 3,
			'cabang_id'  => 1,
			'jumlah'      => 100
		]);

		DB::table('items_stocks')->insert([
			'item_id' => 4,
			'cabang_id'  => 1,
			'jumlah'      => 100
		]);
    }
}
