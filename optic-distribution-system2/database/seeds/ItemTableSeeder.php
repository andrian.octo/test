<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('items')->insert([
			'sub_category_id' => 1,
			'nama'  => "Hoya",
			'harga_beli'      => 100000,
			'harga_jual_default'      => 120000
		]);

		DB::table('items')->insert([
			'sub_category_id' => 1,
			'nama'  => "Essilor",
			'harga_beli'      => 200000,
			'harga_jual_default'      => 300000
		]);

		DB::table('items')->insert([
			'sub_category_id' => 1,
			'nama'  => "Broco",
			'harga_beli'      => 300000,
			'harga_jual_default'      => 310000
		]);

		DB::table('items')->insert([
			'sub_category_id' => 1,
			'nama'  => "Adidas",
			'harga_beli'      => 100000,
			'harga_jual_default'      => 110000
		]);
    }
}
