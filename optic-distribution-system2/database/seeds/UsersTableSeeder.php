<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name'      => str_random(10),
        'username'  => "andrian",
        'role'      => 0,
        'password'  => bcrypt('1234'),
        'cabang_id' => 1,
      ]);

      DB::table('users')->insert([
        'name'      => str_random(10),
        'username'  => "manager",
        'role'      => 1,
        'password'  => bcrypt('1234'),
        'cabang_id' => 2,
      ]);

      DB::table('users')->insert([
        'name'      => str_random(10),
        'username'  => "pegawai",
        'role'      => 2,
        'password'  => bcrypt('1234'),
        'cabang_id' => 1,
      ]);
    }
}
