<?php

use Illuminate\Database\Seeder;

class OpticTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
		DB::table('optics')->insert([
			'nama'		=> 'Optic Muria',
      'plafon'  => 90000000,
      'alamat'	=> 'Jl. Ndawe Raya, Kudus',
      'cabang_id'=> 1,
      'sisa_plafon'=> 90000000,
      'telepon'	=> '9938388',
		]);

		DB::table('optics')->insert([
			'nama'		=> 'Optic Merbabu',
      'plafon'  => 90000000,
      'alamat'	=> 'Jl. Prambatan, Kudus',
      'cabang_id'=> 1,
      'sisa_plafon'=> 90000000,
      'telepon'	=> '8554685',
		]);

		DB::table('optics')->insert([
			'nama'		=> 'Optic Merbabu',
      'plafon'  => 90000000,
      'alamat'	=> 'Jl. Ahmad Yani, Semarang',
      'cabang_id'=> 2,
      'sisa_plafon'=> 90000000,
      'telepon'	=> '6468454',
		]);
    }
}
