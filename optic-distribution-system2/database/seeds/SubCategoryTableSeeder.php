<?php

use Illuminate\Database\Seeder;

class SubCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('sub_category')->insert([
      'nama'      => "minus 1",
      'category_id' => 1
      ]);

      DB::table('sub_category')->insert([
      'nama'      => "plus 1",
      'category_id' => 1
      ]);

      DB::table('sub_category')->insert([
      'nama'      => "netral",
      'category_id' => 1
      ]);

      DB::table('sub_category')->insert([
      'nama'      => "50ml",
      'category_id' => 2
      ]);

      DB::table('sub_category')->insert([
      'nama'      => "100ml",
      'category_id' => 2
      ]);

      DB::table('sub_category')->insert([
      'nama'      => "150ml",
      'category_id' => 2
      ]);

    }
}
