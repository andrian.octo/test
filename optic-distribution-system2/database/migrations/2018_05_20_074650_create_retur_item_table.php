<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_item', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('retur_id');
          $table->integer('item_id');
          $table->integer('jumlah');
          $table->float('diskon');
          $table->double('harga_jual',15,2);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retur_item');
    }
}
