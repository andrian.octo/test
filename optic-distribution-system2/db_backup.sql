-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: opticsystem
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cabang`
--

DROP TABLE IF EXISTS `cabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabang`
--

LOCK TABLES `cabang` WRITE;
/*!40000 ALTER TABLE `cabang` DISABLE KEYS */;
INSERT INTO `cabang` VALUES (1,'Semarang',NULL,NULL),(2,'Kudus',NULL,NULL);
/*!40000 ALTER TABLE `cabang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiryStat` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (2,'Pembersih Lensa',0,NULL,NULL),(3,'Frame',0,NULL,NULL),(4,'LENSA',0,'2018-10-08 04:47:31','2018-10-10 01:34:30'),(7,'SOFTLENS',1,'2018-10-10 01:41:20','2018-10-10 01:41:20'),(8,'Tempat Kacamata Plastik',0,'2018-10-10 01:41:52','2018-10-10 01:41:52'),(9,'Hard Case',0,'2018-10-10 01:42:02','2018-10-10 01:42:02');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_category_id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_beli` double(15,2) NOT NULL DEFAULT '0.00',
  `harga_jual_default` double(15,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (3,1,'Broco',300000.00,310000.00,NULL,NULL),(7,8,'PLANO',120000.00,280000.00,'2018-10-10 01:38:36','2018-10-10 01:38:36'),(8,8,'PLANO',76800.00,120000.00,'2018-10-10 01:43:40','2018-10-10 01:43:40'),(9,8,'-0.25',76800.00,120000.00,'2018-10-10 01:44:07','2018-10-10 01:44:07'),(10,8,'-0.50',76800.00,120000.00,'2018-10-10 02:00:56','2018-10-10 02:00:56'),(11,8,'-0.75',76800.00,120000.00,'2018-10-10 02:01:38','2018-10-10 02:01:38'),(12,8,'-1.00',76800.00,120000.00,'2018-10-10 02:02:29','2018-10-10 02:02:29'),(13,8,'-1.25',76800.00,120000.00,'2018-10-10 02:02:53','2018-10-10 02:02:53'),(14,8,'-1.50',76800.00,120000.00,'2018-10-10 02:03:17','2018-10-10 02:03:17'),(15,8,'-1.75',76800.00,120000.00,'2018-10-10 02:18:35','2018-10-10 02:18:35'),(16,8,'-1.75',76800.00,120000.00,'2018-10-10 04:08:05','2018-10-10 04:08:05'),(17,8,'-2.00',76800.00,120000.00,'2018-10-10 04:08:55','2018-10-10 04:08:55'),(18,8,'-2.25',76800.00,120000.00,'2018-10-10 04:41:19','2018-10-10 04:41:19'),(19,8,'-2.50',76800.00,120000.00,'2018-10-10 04:41:48','2018-10-10 04:41:48'),(20,8,'-2.75',76800.00,120000.00,'2018-10-10 04:42:15','2018-10-10 04:42:15'),(21,8,'-3.00',76800.00,120000.00,'2018-10-10 04:47:27','2018-10-10 04:47:27'),(22,8,'-3.25',76800.00,120000.00,'2018-10-10 04:47:47','2018-10-10 04:47:47'),(23,8,'-3.50',76800.00,120000.00,'2018-10-10 04:48:55','2018-10-10 04:48:55'),(24,8,'-3.75',76800.00,120000.00,'2018-10-10 04:49:42','2018-10-10 04:49:42'),(25,8,'-4.00',76800.00,120000.00,'2018-10-10 04:50:11','2018-10-10 04:50:11'),(26,8,'-4.25',76800.00,120000.00,'2018-10-10 04:50:37','2018-10-10 04:50:37'),(27,8,'-4.50',76800.00,120000.00,'2018-10-10 04:51:00','2018-10-10 04:51:00'),(28,8,'-4.75',76800.00,120000.00,'2018-10-10 04:52:04','2018-10-10 04:52:04'),(29,8,'-5.00',76800.00,120000.00,'2018-10-10 04:55:01','2018-10-10 04:55:01'),(30,8,'-5.25',76800.00,120000.00,'2018-10-10 04:55:22','2018-10-10 04:55:22'),(31,8,'-5.50',76800.00,120000.00,'2018-10-10 04:55:41','2018-10-10 04:55:41'),(32,8,'-5.57',76800.00,120000.00,'2018-10-10 04:56:02','2018-10-10 04:56:02'),(33,8,'-6.00',76800.00,120000.00,'2018-10-10 04:56:41','2018-10-10 04:56:41'),(34,8,'c-0.25',76800.00,120000.00,'2018-10-10 04:59:14','2018-10-10 04:59:14'),(35,8,'c-0.50',76800.00,120000.00,'2018-10-10 04:59:42','2018-10-10 04:59:42'),(36,8,'C-0.75',76800.00,120000.00,'2018-10-10 05:01:16','2018-10-10 05:01:16'),(37,8,'C-1.00',76800.00,120000.00,'2018-10-10 05:01:40','2018-10-10 05:01:40'),(38,8,'C-1.25',76800.00,120000.00,'2018-10-10 05:02:01','2018-10-10 05:02:01'),(39,8,'C-1.50',76800.00,120000.00,'2018-10-10 05:02:24','2018-10-10 05:02:24'),(40,8,'C-1.75',76800.00,120000.00,'2018-10-10 05:02:48','2018-10-10 05:02:48'),(41,8,'C-2.00',76800.00,120000.00,'2018-10-10 05:03:13','2018-10-10 05:03:13'),(42,8,'-0.25 C-0.25',76800.00,120000.00,'2018-10-10 05:04:05','2018-10-10 05:04:05'),(43,8,'C-0.25 C-0.25',76800.00,120000.00,'2018-10-10 05:29:34','2018-10-10 05:29:34');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_histories`
--

DROP TABLE IF EXISTS `items_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_beli` double(15,2) NOT NULL DEFAULT '0.00',
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_histories`
--

LOCK TABLES `items_histories` WRITE;
/*!40000 ALTER TABLE `items_histories` DISABLE KEYS */;
INSERT INTO `items_histories` VALUES (1,2,2,1,300000.00,'Stok Datang',NULL,'2018-09-22 01:57:29','2018-09-22 01:57:29'),(2,2,2,4,300000.00,'Stok Datang',NULL,'2018-09-22 01:59:36','2018-09-22 01:59:36'),(3,1,1,20,120000.00,'sisa 15',NULL,'2018-09-24 08:49:59','2018-09-24 08:51:50'),(4,3,2,100,310000.00,'Stok Datang',NULL,'2018-10-08 04:57:37','2018-10-08 04:57:37'),(5,4,2,300,110000.00,'Stok Datang',NULL,'2018-10-08 04:57:37','2018-10-08 04:57:37');
/*!40000 ALTER TABLE `items_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_stocks`
--

DROP TABLE IF EXISTS `items_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_stocks`
--

LOCK TABLES `items_stocks` WRITE;
/*!40000 ALTER TABLE `items_stocks` DISABLE KEYS */;
INSERT INTO `items_stocks` VALUES (1,2,2,0,'2018-09-22 01:57:29','2018-09-24 08:51:50'),(2,1,1,20,'2018-09-24 08:49:59','2018-09-24 08:49:59'),(3,3,2,100,'2018-10-08 04:57:37','2018-10-08 04:57:37'),(4,4,2,300,'2018-10-08 04:57:37','2018-10-08 04:57:37');
/*!40000 ALTER TABLE `items_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_01_18_135039_create_cabang_table',1),(4,'2018_01_20_043425_create_optics_table',1),(5,'2018_01_20_064126_create_category_table',1),(6,'2018_01_20_064947_create_sub_category_table',1),(7,'2018_01_20_070003_create_items_table',1),(8,'2018_01_20_070816_create_items_stocks_table',1),(9,'2018_01_20_070858_create_items_histories_table',1),(10,'2018_01_20_070916_create_sales_table',1),(11,'2018_01_20_070940_create_sales_items_table',1),(12,'2018_03_11_142945_create_payments_table',1),(13,'2018_05_20_074644_create_retur_table',1),(14,'2018_05_20_074650_create_retur_item_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optics`
--

DROP TABLE IF EXISTS `optics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `plafon` double(15,2) NOT NULL,
  `sisa_plafon` double(15,2) NOT NULL DEFAULT '0.00',
  `last_order` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optics`
--

LOCK TABLES `optics` WRITE;
/*!40000 ALTER TABLE `optics` DISABLE KEYS */;
INSERT INTO `optics` VALUES (1,'Optic Family','Jl. sunan kudus No.2B Kudus','085600267372',1,80000000.00,80000000.00,NULL,NULL,'2018-09-22 02:16:08'),(2,'OPTIK ORION','jl. mlati kidul, kudus','085641222349',1,10000000.00,10000000.00,NULL,NULL,'2018-09-22 01:55:19'),(3,'Optic hayla','kudus','087831933337',1,10000000.00,10000000.00,NULL,NULL,'2018-09-22 02:17:13'),(5,'optik Duta','jl. sunan kudus','085471187465',1,10000000.00,10000000.00,NULL,'2018-09-22 02:19:37','2018-09-22 02:19:37'),(6,'Optik Aufa','jl.Muneng, Gribig, Gebog','085640389844',1,3000000.00,3000000.00,NULL,'2018-09-22 02:22:09','2018-09-22 02:22:09'),(7,'optik fokus','jl.Mlati Norowito Gang 9 No.136','085293672299',1,3000000.00,3000000.00,NULL,'2018-09-22 02:23:52','2018-09-22 02:23:52'),(8,'optik bunda','jl. hos cokroaminoto no.52','085727567214',1,3000000.00,3000000.00,NULL,'2018-09-22 02:26:01','2018-09-22 02:26:01'),(9,'optik Gajah Mada Pati','jl.Supriyadi No.97B','08122520160',1,5000000000.00,5000000000.00,NULL,'2018-09-22 02:28:02','2018-09-22 02:55:54'),(10,'Optik Gajah mada lamongan','Jaksa Agung Suprapto No. 23 Komplek Ruko Orind, KALIOTIK','08122844898',1,500000000.00,500000000.00,NULL,'2018-09-22 02:29:58','2018-09-22 02:56:15'),(11,'optik gajah mada semarang','Gajahmada No.61B, Kembangsari','08126666468',1,100000000.00,100000000.00,NULL,'2018-09-22 02:32:03','2018-09-22 02:32:03'),(12,'optik gajah mada tuban','Jl. Basuki Rachmad No.8 tuban','085732220035',1,150000000.00,150000000.00,NULL,'2018-09-22 02:35:11','2018-09-22 02:35:11'),(13,'optik gajah mada cepu','jl.Ronggolawe No.108-A, Megal, Balun, Cepu','081392103798',1,5000000.00,5000000.00,NULL,'2018-09-22 02:39:31','2018-09-22 02:39:31'),(14,'optik gajah mada blora','jl.Pemuda No.5, Blora, Tempelan, Kec. Blora Kota','081228714740',1,20000000.00,20000000.00,NULL,'2018-09-22 02:41:23','2018-09-22 02:41:23'),(15,'optik gajah mada magelang','Jl. Tidar, Magersari, Magelang','08122965772',1,10000000.00,10000000.00,NULL,'2018-09-22 02:44:32','2018-09-22 02:44:32'),(16,'optik horizon','jl. Kenari, Purwogondo III, Purwogondo, Kalinyamatan','0811-2233-785',1,10000000.00,10000000.00,NULL,'2018-09-22 03:15:40','2018-09-22 03:15:40'),(17,'optik suryamata','jl.Soekarno Hatta No.km, 7, Tahunan','0877-1122-7088',1,10000000.00,10000000.00,NULL,'2018-09-22 03:18:02','2018-09-22 03:18:02'),(18,'optik muria','jl. Sunan Muria No.21, Barongan','08500000000',1,4000000.00,4000000.00,NULL,'2018-09-22 03:23:27','2018-09-22 03:23:27'),(19,'optik yukawi','jl.Kayen - Sukolilo, Lebakwetan, Sukolilo','00000',1,4000000.00,4000000.00,NULL,'2018-09-22 03:26:26','2018-09-22 03:26:26'),(20,'optik anugrah','jl.Mayjen Sutoyo, Garung Lor, Kaliwungu','0813-2624-2811',1,5000000.00,5000000.00,NULL,'2018-09-22 03:28:37','2018-09-22 03:28:37'),(21,'optik kharisma','jl.Dr. Susuanto 53, Randukuning, Pati Lor,','(0295) 356454',1,6000000.00,6000000.00,NULL,'2018-09-22 03:30:00','2018-09-22 03:30:00'),(22,'optik hafara','Dr. Susuanto No.29, Kaborongan, Pati Lor,','(0295) 384577',1,300000000.00,300000000.00,NULL,'2018-09-22 03:31:59','2018-09-22 03:31:59'),(23,'optik funny','jl.Kedung Mundu Raya No. 7, Sendangguwo, Tembalang,','0858-0066-6122',1,40000000.00,40000000.00,NULL,'2018-09-22 03:34:39','2018-09-22 03:34:39'),(24,'optik gajahmada bojonegoro','jl. diponegoro. n0. 30. bojonegoro','0000',1,500000000.00,500000000.00,NULL,'2018-09-22 03:38:58','2018-09-22 03:38:58'),(25,'optik mahir','jepara','085225519634',1,10000000.00,10000000.00,NULL,'2018-09-22 03:54:46','2018-09-22 03:54:46'),(26,'optik almira','Kaborongan, Pati Lor,','(0295) 4101932',1,5000000.00,5000000.00,NULL,'2018-09-22 03:57:00','2018-09-22 03:57:00'),(27,'optik pro semarang','Jl. Gajah Raya No.111 C, Sambirejo, Gayamsari','(024) 6703885',1,20000000.00,19470000.00,NULL,'2018-09-22 03:59:50','2018-09-24 08:52:55'),(28,'optik indojaya perkasa abadi','jl. semarang 142-152 TM7','0315467651',1,500000000.00,500000000.00,NULL,'2018-09-22 04:05:02','2018-09-22 04:05:02'),(29,'IBU IKE','Sab diago M9 no 45 Pakuwon City Surabaya','081331185650',1,1000000000.00,1000000000.00,NULL,'2018-09-22 04:08:06','2018-09-22 04:08:06'),(30,'Optik Indo Pekalongan','jl.Gajah Mada no 7 Pekalongan','08170599101',1,5000000000.00,5000000000.00,NULL,'2018-09-22 04:15:50','2018-09-22 04:15:50'),(31,'optik Top 5 Tegal','Jl. Arif Rahman Hakim, No. 143','087719993212',1,10000000.00,10000000.00,NULL,'2018-09-22 04:18:11','2018-09-22 04:18:11'),(32,'Optik Indonesia Tegal','Jl. Jend. A. Yani No.225, Mintaragen, Tegal Timur','0818107636',1,70000000.00,70000000.00,NULL,'2018-09-22 04:21:42','2018-09-22 04:21:42'),(33,'Ahmad Optik','MASJID AGUNG','08122572341',2,1000000.00,1000000.00,NULL,'2018-09-22 04:35:06','2018-09-22 06:02:54'),(34,'Ator Optik','Jl. Lamongan Raya No.40','083838111305',2,5000000.00,5000000.00,NULL,'2018-09-22 04:40:33','2018-09-22 04:40:33'),(35,'Andalusia Optik','Jl. MT. Haryono No. 6','3557147',2,5000000.00,5000000.00,NULL,'2018-09-22 04:49:35','2018-09-22 06:03:14'),(36,'Akram Optik','Jl. Dr. Karyadi No. 3','8455727',2,1000000.00,1000000.00,NULL,'2018-09-22 04:51:37','2018-09-22 06:03:06'),(37,'Asia Optik','Jl. Gang Pinggir No. 83','3554708',2,500000.00,500000.00,NULL,'2018-09-22 04:54:05','2018-09-22 06:03:25'),(38,'Atlantik Optik','Jl. Tambak Mas 16','3559135',2,1000000.00,1000000.00,NULL,'2018-09-22 04:55:45','2018-09-22 06:03:33'),(39,'Beta Optik','Jl. Majapahit No. 150','6714503',2,5000000.00,5000000.00,NULL,'2018-09-22 05:00:03','2018-09-22 06:04:00'),(40,'Bhinneka Optik','Jl. Pandanaran No. 49','8310634',2,1000000.00,1000000.00,NULL,'2018-09-22 05:01:34','2018-09-22 06:04:08'),(41,'Ben Rene Optik','Jl. Kanfer Raya Blok D No. 12 Banyumanik','76487687',2,1000000.00,1000000.00,NULL,'2018-09-22 05:08:49','2018-09-22 06:03:42'),(42,'Bestanden Optik','Jl. Trengguli III No. 10','8413617',2,5000000.00,5000000.00,NULL,'2018-09-22 05:10:09','2018-09-22 06:03:51'),(43,'Bringin Optik','Jl. Menoreh Raya','86451745',2,5000000.00,5000000.00,NULL,'2018-09-22 05:12:23','2018-09-22 06:47:12'),(44,'Bina Sehat Optik','Jl. KH. Dahlan No. 45','8311397',2,1000000.00,1000000.00,NULL,'2018-09-22 05:28:06','2018-09-22 06:47:47'),(45,'Boy Optik','Jl. MT. Haryono','8451002',2,1000000.00,1000000.00,NULL,'2018-09-22 05:29:26','2018-09-22 06:47:21'),(46,'Bipola Optik','Jl. Puri Anjasmoro','7600033',2,5000000.00,5000000.00,NULL,'2018-09-22 05:30:52','2018-09-22 06:47:34'),(47,'Devi Optik','Jl. Kanguru','081575483277',2,1000000.00,1000000.00,NULL,'2018-09-22 05:33:13','2018-09-22 06:46:58'),(48,'Aries Optik','Jl. Cempaka Sari Nomor 33, Sekaran,','085640242442',2,2500000.00,2500000.00,NULL,'2018-09-22 06:34:06','2018-09-22 06:34:06'),(49,'Baru Optik','Pertokoan Pandawa 55 - 56, Salatiga','0298 327607',2,15000000.00,15000000.00,NULL,'2018-09-22 06:36:44','2018-09-22 06:36:44'),(50,'Diana Optik','Jl. Depok','08813780252',2,1000000.00,1000000.00,NULL,'2018-09-22 06:46:25','2018-09-22 06:46:25'),(51,'Bringin Optik','Jl. S. Parman, Ungaran','081325687047',2,20000000.00,20000000.00,NULL,'2018-09-22 07:04:27','2018-09-22 07:04:27'),(52,'Cahaya Optik','Jl. Sultan Patah no.45, Demak','081225215099',2,2500000.00,2500000.00,NULL,'2018-09-22 07:06:06','2018-09-22 07:06:06'),(53,'Bening Optik','Jl. raya Kaliwungu 128, Kendal','085727876629',2,5000000.00,5000000.00,NULL,'2018-09-22 07:09:02','2018-09-22 07:09:02'),(56,'Bening Optik','Jl. Tegalsari V, Semarang','0811276018',2,500000.00,500000.00,NULL,'2018-09-24 05:01:51','2018-09-24 05:01:51'),(57,'Dicky Optik','Desa Pakuncen RT 03 RW 01 Selomerto, Wonosobo','085878196699',2,1000000.00,1000000.00,NULL,'2018-09-24 05:25:22','2018-09-24 05:25:22'),(58,'Damai Optik','Jl. Raya Barat Weleri, Sambungsari - Weleri, Kendal','08122513755',2,2500000.00,2500000.00,NULL,'2018-09-24 05:28:08','2018-09-24 05:28:08'),(59,'Family Optik','Jl. Parangkritik Raya No. 12 Tlogosari, Semarang','08156569560',2,500000.00,500000.00,NULL,'2018-09-24 05:33:07','2018-09-24 05:33:07'),(60,'Gagas Optik','Jl. Sukun Raya No. 48 D Banyumanik, Semarang','7478479',2,10000000.00,10000000.00,NULL,'2018-09-24 05:46:53','2018-09-24 05:46:53'),(61,'Hawaii Optik','Jl. Dr. Cipto No. 181 Semarang','8457281',2,500000.00,500000.00,NULL,'2018-09-24 05:48:19','2018-09-24 05:48:19'),(62,'Inna Swiss Optik','Jl. MT. Haryono 93A, Semarang','0816660577',2,5000000.00,5000000.00,NULL,'2018-09-24 06:13:02','2018-09-24 06:13:02'),(63,'Indah Optik','Jl. Raya Patemon No.99 Gunungpati','02470356303',2,5000000.00,5000000.00,NULL,'2018-09-24 06:14:42','2018-09-24 06:14:42'),(64,'Intan Optik','Jl. Gatot Subroto No. 686, Ungaran','(024) 6922617',2,5000000.00,5000000.00,NULL,'2018-09-24 06:17:35','2018-09-24 06:17:35'),(65,'Minly Optik','Jl. Depok No. 38 C, Semarang','024-6707171',2,500000.00,500000.00,NULL,'2018-09-24 06:19:14','2018-09-24 09:13:56'),(66,'Lia Optik','Jl. MT. Haryono No. 187, Semarang','085104960062',2,500000.00,500000.00,NULL,'2018-09-24 09:08:09','2018-09-24 09:08:09'),(67,'Metro Optik','Jl. Gajahmada No. 17, Semarang','0818298292',2,2500000.00,2500000.00,NULL,'2018-09-24 09:17:56','2018-09-24 09:17:56'),(68,'Nusantara Optik','Jl. Dr. Cipto No. 3 - 5 Semarang','024-3540458',2,2500000.00,2500000.00,NULL,'2018-09-24 09:30:29','2018-09-24 09:30:29'),(69,'Pro Optik','Jl. Gajah III C Gayamsari, Semarang','08122542054',2,500000.00,500000.00,NULL,'2018-09-24 09:32:37','2018-09-24 09:32:37'),(70,'Roemah Kacamata','Jl. Kelud Raya No.72 Semarang','087880673700',2,5000000.00,5000000.00,NULL,'2018-09-24 09:34:37','2018-09-24 09:34:37'),(71,'Sa. Ismail Optik','Jl. Pemuda (Flamboyan) No. 72 B Kendal','087700305145',2,5000000.00,5000000.00,NULL,'2018-09-24 09:37:27','2018-09-24 09:37:27'),(72,'Sa. Ismail Optik','Jl. MT. Haryono No. 112 Semarang','024-3565825',2,1000000.00,1000000.00,NULL,'2018-09-24 09:38:57','2018-09-24 09:38:57'),(73,'Wafi Optik','Jl. Fatmawati Tegalkangkung No. 171 RT 06 RW 02 Semarang','024-6706683',2,500000.00,500000.00,NULL,'2018-09-24 09:41:53','2018-09-24 09:41:53'),(74,'Wahyu Optik','Ruko Segitiga Emas Jl. Prof. Hamka Blok AB Ngaliyan, Semarang','081225706070',2,500000.00,500000.00,NULL,'2018-09-24 09:44:34','2018-09-24 09:44:34'),(75,'Yasmin Optik','Jl. Jend. Sudirman No. 330 Semarang','08156544839',2,500000.00,500000.00,NULL,'2018-09-24 09:46:13','2018-09-24 09:46:13'),(76,'Zona Optik','SCJ Matahari Lt.2 Pasar Johar Semarang','082137111166',2,500000.00,500000.00,NULL,'2018-09-28 04:36:29','2018-09-28 04:36:29'),(77,'Agus Classik Optik','Jl. Karangrejo Banyumanik, Semarang','081229612226',2,500000.00,500000.00,NULL,'2018-09-28 04:38:55','2018-09-28 04:38:55'),(78,'Kita Optik','Jl. Soekarno-Hatta, Semarang','082133005168',2,500000.00,500000.00,NULL,'2018-09-28 04:41:30','2018-09-28 04:41:30'),(79,'Alyn Optik','Mranggen, Semarang','081226704747',2,500000.00,500000.00,NULL,'2018-09-28 04:42:56','2018-09-28 04:42:56'),(80,'Armada Optik','Tembalang, Semarang','0818299956',2,500000.00,500000.00,NULL,'2018-09-28 04:44:39','2018-09-28 04:44:39'),(81,'Audifha Optik','Tlogosari, Semarang','085727646333',2,500000.00,500000.00,NULL,'2018-09-28 04:46:17','2018-09-28 04:46:17');
/*!40000 ALTER TABLE `optics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retur`
--

DROP TABLE IF EXISTS `retur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retur` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `cabang_id` int(11) NOT NULL,
  `optic_id` int(11) DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retur`
--

LOCK TABLES `retur` WRITE;
/*!40000 ALTER TABLE `retur` DISABLE KEYS */;
/*!40000 ALTER TABLE `retur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retur_item`
--

DROP TABLE IF EXISTS `retur_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retur_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `harga_jual` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retur_item`
--

LOCK TABLES `retur_item` WRITE;
/*!40000 ALTER TABLE `retur_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `retur_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `optic_id` int(11) NOT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `total` double(15,2) NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (14,6,1,27,2,'2018-09-26',540000.00,'','2018-09-24 08:51:50','2018-09-24 08:51:50',NULL);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_items`
--

DROP TABLE IF EXISTS `sales_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `harga_jual` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_items`
--

LOCK TABLES `sales_items` WRITE;
/*!40000 ALTER TABLE `sales_items` DISABLE KEYS */;
INSERT INTO `sales_items` VALUES (1,14,1,5,10.00,120000.00,'2018-09-24 08:51:50','2018-09-24 08:51:50');
/*!40000 ALTER TABLE `sales_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_payment`
--

DROP TABLE IF EXISTS `sales_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `metode` tinyint(4) NOT NULL DEFAULT '0',
  `jumlah` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_payment`
--

LOCK TABLES `sales_payment` WRITE;
/*!40000 ALTER TABLE `sales_payment` DISABLE KEYS */;
INSERT INTO `sales_payment` VALUES (1,14,6,2,10000.00,'2018-09-24 08:52:55','2018-09-24 08:52:55');
/*!40000 ALTER TABLE `sales_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_category`
--

LOCK TABLES `sub_category` WRITE;
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
INSERT INTO `sub_category` VALUES (1,1,'minus 1',NULL,NULL),(2,1,'plus 1',NULL,NULL),(3,1,'netral',NULL,NULL),(4,2,'50ml',NULL,NULL),(5,2,'100ml',NULL,NULL),(6,2,'150ml',NULL,NULL),(7,3,'fashion','2018-10-08 04:47:09','2018-10-08 04:47:56'),(8,4,'SL150-HVP','2018-10-10 01:35:23','2018-10-10 01:35:23'),(9,4,'SL155-HVP','2018-10-10 01:35:37','2018-10-10 01:35:37'),(10,4,'SL155BC-HVP','2018-10-10 01:35:49','2018-10-10 01:35:49'),(11,4,'SL160-HVP','2018-10-10 01:36:04','2018-10-10 01:36:04');
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cabang_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,2,'Fauzan','owner','$2y$10$pFHo91GHJVVhWhMCuD53eOK9qKfxRY0b.vh0cr..f928tGIu/l0kW',0,'fWXGHRynbBIolY2audNBLZv9hE27zrqGFpHSqWxuddq78K53XoK2etwNjMYA',NULL,'2018-09-22 01:49:47'),(2,1,'HENRI','Koordinator','$2y$10$Go1ukrrez8.fRkBoRDZ83OrHGaa5FziopGqIpqIfpCb3zBPd.6M5q',1,'WETa1RlfbQVWUEFBaKXVK3gFnho4r3OF9oTZ41BsMAUs0gM56eLmXC5CthVK',NULL,'2018-09-22 01:48:51'),(3,2,'Erna','pegawai','$2y$10$K3M7.sSfL.sAnwfeEvx8AebMmVc2xrcXiHWO.qnASYpofoZtPPPK.',2,NULL,NULL,'2018-09-22 01:46:59'),(4,2,'ELA','finance','$2y$10$3v75iMW1vmzagjLZm28Mf.F1xTuBpoNLr.0qGctX2N/ZID3.0VL52',1,'tbwP9SvcXvwx2IfspUF45e6oVdaHnqSjiZWd8uyQknJHIgpR2tJopIxWO4lC','2018-09-22 01:49:19','2018-09-22 02:12:23'),(5,2,'andrian','andrian','$2y$10$WLogQ9jOfswAZ4RM49KRiO1P9Zu9Je.yOHOOH82f.Lojj.y.fEsu6',1,'5OjFEPtQIX2bJUseE6yCoHqzx1R4nqUjYUgA8k6ZOPgmGO7nglct1zsIRQIu','2018-09-22 02:56:55','2018-09-22 02:56:55'),(6,1,'Yuni','yuni','$2y$10$MceO2Wu0U9kYGXnBsEbNeu7cuDoNTEuIsxY9BCNGi5XZeYM6Fn8Wu',1,'Lai4HmBQT4AsmhPD7HjOu61ZdFs99s3yvJ4Or7fKFae0all5QIZLrGmdEmSz','2018-09-24 08:45:51','2018-09-24 08:45:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-10  5:44:25
